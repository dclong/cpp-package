My C\+\+ library to make coding in C\+\+ easier. 
Facilities in this library includes

1. some generic algorithms not in STL

2. extended functionalities of the Armadillo package 

3. geometry classes

4. random number generating algorithms

5. string manipulations. 

6. timer class. 
