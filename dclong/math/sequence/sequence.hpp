/*
 * sequence.h
 *
 *  Created on: Aug 27, 2012
 *      Author: dclong
 */

#ifndef SEQUENCE_H_
#define SEQUENCE_H_

#include <cassert>

namespace dclong {
using namespace std;

template<class data_type> vector<data_type> ari_seq(data_type from, data_type by, int n){
	vector<data_type> x(n);
	x[0] = from;
	for(int i=1; i<n; ++i){
		x[i] = x[i-1] + by;
	}
	return x;
}

template<class data_type> vector<data_type> ari_seq(data_type from, data_type to, data_type by){
	assert(by != 0);
	if(by > 0){
		assert(from < to);
		int n = (to - from) / by + 1;
		std:: cout << to << " " << from << " " << by << " " << n << std::endl;
		return ari_seq(from, by, n);
	}
	assert(from > to);
	int n = (from - to) / (-by) + 1;
	return ari_seq(from, by, n);
}

template<class data_type> vector<data_type> ari_seq(data_type from, data_type to){
	return ari_seq(from, to, (data_type) 1);
}

template<class data_type> vector<data_type> ari_seq(data_type n){
	return ari_seq((data_type) 0, (data_type) (n-1));
}

template<class data_type> vector<data_type> geo_seq(data_type from, data_type to, data_type by){

}

}


#endif /* SEQUENCE_H_ */
