/*
 * line.hpp
 *
 *  Created on: Oct 3, 2012
 *      Author: dclong
 */

#ifndef LINE_HPP_
#define LINE_HPP_
#include "/home/dclong/Dropbox/code/cpp/cackage/math/sign/sign.hpp"
#include "/home/dclong/Dropbox/code/cpp/cackage/string/string.hpp"

#include <utility>
#include <tuple>
#include <cmath>
#include <limits>
#include <random>
#include <iostream>
namespace dclong {

using namespace std;

class line {
	protected:
		typedef pair<double, double> point_t;
		typedef pair<double, double> range_t;
		typedef pair<double, double> dpair_t;
		// (slope, intercept)
		double slope;
		double intercept;
		bool updated{true};
	public:
		void swap(line & other){
			std::swap(slope, other.slope);
			std::swap(intercept, other.intercept);
			std::swap(updated, other.updated);
		}
		line(const line & other) : slope{other.slope}, intercept{other.intercept} {}

		line(line && other) : slope{move(other.slope)}, intercept{move(other.intercept)} {}

		line & operator=(const line & other){
			line(other).swap(*this);
			return *this;
		}

		line & operator=(line && other){
			line(other).swap(*this);
			return *this;
		}
		line() : slope{0}, intercept{0} {}

		line(double a, double b) : slope(a), intercept(b){}

//		line(double x1, double y1, double x2, double y2) : a{(y2 - y1) / (x2 - x1)},
//		b{((std::abs(x1)<std::abs(x2))?intercept(x1,y1,a):intercept(x2,y2,a))} {}

		line(const point_t & p1, const point_t & p2) :
			slope{(p2.second - p1.second) / (p2.first - p1.first)},
			intercept{((std::abs(p1.first)<std::abs(p2.first))?b(p1,slope):b(p2,slope))} {}

//		line(double x, double y, double a) : a{a}, b{y - a * x} {}

		line(const point_t & p, double a) : slope{a}, intercept{p.second - a * p.first} {}

//		line(const pair<double, double> & point, double x, double y) :
//			line(point.first, point.second, x, y) {}

//		line(double x, double y, const pair<double, double> & point) :
//			line(x, y, point.first, point.second) {}

//		/**
//		 * Construct the line define by a point on the line and the slope of the line.
//		 * The data is represented in a tuple of the convention (x, y, a).
//		 * @param line a tuple of the convention (x, y, a).
//		 */
//		line(const tuple<double, double, double> & t) :
//			line(get<0>(t), get<1>(t), get<2>(t)) {}

		double a() const {
			return slope;
		}

		void a(double a){
			slope = a;
			updated = true;
		}
        
        void reset(double a, double b){
            slope = a;
            intercept = b;
			updated = true;
        }

        void reset(const line & l) {
        	reset(l.slope, l.intercept);
        	updated =true;
        }

		double b() const {
			return intercept;
		}

		void b(double b){
			intercept = b;
			updated = true;
		}

//		static inline double intercept(double x, double y, double a){
//			return y - a * x;
//		}

		static double b(const point_t & p, double a){
			return p.second - a * p.first;
		}
		/**
		 * Find the x coordinate of the intersection of this line and other line.
		 */
		double intersection(const line & other) const {
			return (other.intercept - intercept) / (slope - other.slope);
		}
		/**
		 * The difference of this line and other line at x.
		 * @param x
		 * @param line1
		 * @param line2
		 */
		inline double diff(double x, const line & other) const {
			return (slope - other.slope) * x + (intercept - other.intercept);
		}

		virtual double value(double x) const {
			return value(x, slope, intercept);
		}
		static double value(double x, double a, double b) {
			if(a){
				return a * x + b;
			}
			return b;
		}

		range_t value(range_t r) const {
			return make_pair(value(r.first), value(r.second));
		}
//		/**
//		 * The increment of the line value from x1 to x2.
//		 * @param a the slope of the line.
//		 * @param x1
//		 * @param x2
//		 */
//		inline double increment(double x1, double x2){
//			return a * (x2 - x1);
//		}
		inline double increment(const range_t & r) const {
			return slope * (r.second - r.first);
		}
		/**
		 * Check the relationship between line 1 and line 2 at x.
		 * @param line1
		 * @param line2
		 * @param x
		 * @return 1 if lower, -1 if higher and 0 if equal.
		 */
		inline int less(double x, const line & other) const {
			if(!std::isinf(x)){
				return -dclong::sign(diff(x, other));
			}
			int ads = dclong::sign(slope - other.slope);
            // x == inf or x == -inf
			if(x>0){
                // x == inf
				return -ads;
			}
            // x == -inf
			return ads;
		}
		pair<int, int> less(const range_t & r, const line & other) const{
			return make_pair(less(r.first, other), less(r.second, other));
		}
		virtual string to_string() const {
			return dclong::to_string(make_pair(slope, intercept), "Line: (", ")");
//			return "Line: (" + slope + ", " + intercept + ")";
		}
		virtual ~line(){};
};

}


#endif /* LINE_HPP_ */
