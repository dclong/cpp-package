
#ifndef SEGMENT_HPP_
#define SEGMENT_HPP_

#include "dclong/math/geometry/line/line.hpp"
#include "dclong/string.hpp"
#include <random>
#include <limits>
#include <utility>
#include <iostream>

namespace dclong{
using namespace std;
class segment : public dclong::line {
	private:
		virtual void update_state_extra(){}
	protected:
		typedef pair<int, int> ipair_t;
		// x domain, xrange.second > xrange.first
		pair<double, double> xrange;
		// value domain of the line
		// dx and dy
		pair<double, double> difference;
		pair<double, double> yminsum;
		~segment(){}
	public:
		void swap(segment & other){
            dclong::line::swap(other);
			std::swap(xrange, other.xrange);
//			std::swap(yrange, other.yrange);
			std::swap(difference, other.difference);
			std::swap(yminsum, other.yminsum);
		}
		bool update_state(){
			if(updated){
				pair<double, double>yrange = dclong::line::value(xrange);
				difference = make_pair(xrange.second - xrange.first, yrange.second - yrange.first);
				yminsum = make_pair(std::min(yrange.first, yrange.second), yrange.first + yrange.second);
				update_state_extra();
				updated = false;
				return true;
			}
			return false;
		}
		segment(const segment & other) :
			dclong::line{other},
			xrange{other.xrange},
//			yrange{other.yrange},
			difference{other.difference},
			yminsum{other.yminsum} {}

		segment(segment && other) :
			dclong::line{move(other)},
			xrange{move(other.xrange)},
//			yrange{move(other.yrange)},
			difference{move(other.difference)},
			yminsum{move(other.yminsum)} {}

		segment & operator=(const segment & other){
			segment(other).swap(*this);
			return *this;
		}

		segment & operator=(segment && other){
			segment(other).swap(*this);
			return *this;
		}

        segment() :
        	dclong::line(),
        	xrange{-numeric_limits<double>::infinity(), numeric_limits<double>::infinity()}
        	{}

		segment(const pair<double, double> & p1, const pair<double, double> & p2) :
			dclong::line(p1, p2), xrange{make_pair(p1.first, p2.first)} {}

		segment(double x1, double y1, double x2, double y2) :
			dclong::line(x1, y1, x2, y2), xrange{make_pair(x1, x2)} {}

		segment(double x, double y, double a, const pair<double, double> & xrange) :
			dclong::line(x, y, a),
			xrange{xrange}
		{}

		segment(double x, double y, double a, pair<double, double> && xrange) :
					dclong::line(x, y, a),
					xrange{move(xrange)}
				{}

//		segment(const dclong::line & line, const pair<double, double> & r) :
//			dclong::line(line),
//			xrange{r} {}
//
//		segment(const dclong::line & line) :
//			segment(line, make_pair(-numeric_limits<double>::infinity(), numeric_limits<double>::infinity())) {}
//
//		segment(dclong::line && line, const pair<double, double> & r) :
//			dclong::line{move(line)},
//			xrange{r} {}
//
//		segment(const dclong::line & line, pair<double, double> && r) :
//			dclong::line(line),
//			xrange{move(r)} {}
//
//		segment(dclong::line && line, pair<double, double> && r) :
//			dclong::line(move(line)),
//			xrange{move(r)} {}
//
//		segment(const dclong::line && line) :
//		segment(move(line), make_pair(-numeric_limits<double>::infinity(), numeric_limits<double>::infinity())) {}

        void x1(double x1){
            xrange.first = x1;
			updated = true;
        }

        double x1() const {
            return xrange.first;
        }
        double x2() const {
            return xrange.second;
        }
        void x2(double x2){
            xrange.second = x2;
            updated = true;
        }
        double dx() const {
        	return difference.first;
        }
        double y1() const {
			return value(xrange.first);
	    }
	    double y2() const {
			return value(xrange.second);
	    }
	    double ymin() const {
	    	return yminsum.first;
	    }
	    double ysum() const {
	    	return yminsum.second;
	    }
	    double dy() const {
	    	return difference.second;
	    }
	    pair<double, double> p1() const {
			return make_pair(xrange.first, y1());
	    }
	    pair<double, double> p2() const {
			return make_pair(xrange.second, y2());
	    }

	    void domain(double x1, double x2){
	    	xrange.first = x1;
	    	xrange.second = x2;
	    	updated = true;
	    }

		void domain(const pair<double, double> & r){
			xrange = r;
            updated = true;
		}
		void domain(pair<double, double> && r){
			xrange = move(r);
            updated = true;
		}
        pair<double, double> domain() const {
            return xrange;
        }
        /**
         * 2 right
         * 1 right end point
         * 0 in the open range
         * -1 left end point
         * -2 left
         */
        int in_domain(double x) const {
        	if(x > xrange.first && x< xrange.second){
        		return 0;
        	}
        	if(x>xrange.second){
        		return 2;
        	}
        	if(x<xrange.first){
        		return -2;
        	}
        	if(x==xrange.second){
        		return 1;
        	}
        	return -1;
        }


		/**
		 * Check whether this segment is lower than other segment at the two ends of other.
		 * @param other
		 */
		pair<signed char, signed char> less(const segment & other) const {
			return line::less(other.xrange, other);
		}
		/**
		 * Template, also support subclass segment_distribution<Exp>
		 */
		template<class Seg> vector<pair<signed char, signed char> > less(const vector<Seg> & segs) const{
			const int size = segs.size();
			vector<pair<signed char, signed char> > comparison(size);
			for(int i{0}; i<size; ++i){
				comparison[i] = less(segs[i]);
			}
			return comparison;
		}

//		using line::less;

		virtual string to_string() const override {
			return dclong::line::to_string()
			+ dclong::to_string(xrange, " Xrange: (", ")")
			+ dclong::to_string(difference, " Difference: (", ")");
		}
};

}
#endif
