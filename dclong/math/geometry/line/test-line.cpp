/*
 * test-line.cpp
 *
 *  Created on: Oct 3, 2012
 *      Author: dclong
 */


#include "line.hpp"
#include <iostream>
#include <iomanip>
using namespace std;
/**
 * l1 at 0: 2
 * inc l1 from 0 to 1: 1
 * l2 at 0: 2
 * inc l2 from 0 to 1: -1
 * l1<l2 at -1: 1
 * l1<l2 at 0: 0
 * l1<l2 at 1: -1
 * l1<l2 at -inf: 1
 * l1<l2 at inf:-1
 * 1
 * inc l3 from 0 to 1: 0
 *
 */
void test1(){
	const double inf{numeric_limits<double>::infinity()};
	dclong::line l1{-1, 2};
	dclong::line l2{1,2};
	cout << "l1 at 0: " << l1.value(0) << endl;
    cout << "inc l1 from 0 to 1: " << l1.increment(make_pair(0,1)) << endl;
	cout << "l2 at 0: " << l2.value(0) << endl;
    cout << "inc l2 from 0 to 1: " << l2.increment(make_pair(0,1)) << endl;
	cout << "l1<l2 at -1: " << l1.less(-1, l2) << endl;
	cout << "l1<l2 at 0: " << l1.less(0, l2) << endl;
	cout << "l1<l2 at 1: " << l1.less(1, l2) << endl;
	cout << "l1<l2 at -inf: " << l1.less(-inf, l2) << endl;
	cout << "l1<l2 at inf:" << l1.less(inf, l2) << endl;
    dclong::line l3{0,1};
    cout << l3.value(inf) << endl;
    cout << "increment of l3 from 0 to 1: " << l3.increment(make_pair(0,1)) << endl;
    cout << "l1 before swapping: " << l1.to_string() << endl;
    cout << "l3 before swapping: " << l3.to_string() << endl;
    std::swap(l1, l3);
    cout << "l1 after swapping: " << l1.to_string() << endl;
    cout << "l3 after swapping: " << l3.to_string() << endl;

}
void test2(){
	dclong::line l1{make_pair(0,2), make_pair(1, 1)};
	cout << l1.to_string() << endl;
}
int main(){
    test2();
}

