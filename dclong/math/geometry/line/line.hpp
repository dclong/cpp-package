
#ifndef LINE_HPP_
#define LINE_HPP_
#include "dclong/math/function/function.hpp"
#include "dclong/string.hpp"

#include <utility>
#include <tuple>
#include <cmath>
#include <limits>
#include <random>
#include <iostream>
#include <limits>
#include <cassert>
namespace dclong {

using namespace std;

class line {
private:
	protected:
		double slope;
		double intercept;
		bool updated{true};
		~line(){};
	public:
		void swap(line & other){
			std::swap(slope, other.slope);
			std::swap(intercept, other.intercept);
			std::swap(updated, other.updated);
		}
		line(const line & other) : slope{other.slope}, intercept{other.intercept} {}

		line(line && other) : slope{move(other.slope)}, intercept{move(other.intercept)} {}

		line & operator=(const line & other){
			line(other).swap(*this);
			return *this;
		}

		line & operator=(line && other){
			line(other).swap(*this);
			return *this;
		}
		line() : line(0, 0) {}

		line(double a, double b) : slope(a), intercept(b){
			assert(!std::isinf(slope));
			assert(!std::isnan(slope));
			assert(!std::isinf(intercept));
			assert(!std::isnan(intercept));
		}

//		line(double x1, double y1, double x2, double y2) : a{(y2 - y1) / (x2 - x1)},
//		b{((std::abs(x1)<std::abs(x2))?intercept(x1,y1,a):intercept(x2,y2,a))} {}

		line(const pair<double, double> & p1, const pair<double, double> & p2) :
			line(p1.first, p1.second, p2.first, p2.second) {}

		line(double x1, double y1, double x2, double y2) :
			slope{(y2-y1)/(x2-x1)},
			intercept{((std::abs(x1)<std::abs(x2))?(y1 - slope * x1):(y2 - slope * x2))} {
				assert(!std::isinf(slope));
							assert(!std::isnan(slope));
							assert(!std::isinf(intercept));
							assert(!std::isnan(intercept));
			}

		line(double x, double y, double a) : slope{a}, intercept{y - a * x} {
			assert(!std::isinf(slope));
						assert(!std::isnan(slope));
						assert(!std::isinf(intercept));
						assert(!std::isnan(intercept));
		}

		line(const pair<double, double> & p, double a) : line(p.first, p.second, a) {}

//		line(const pair<double, double> & point, double x, double y) :
//			line(point.first, point.second, x, y) {}

//		line(double x, double y, const pair<double, double> & point) :
//			line(x, y, point.first, point.second) {}

//		/**
//		 * Construct the line define by a point on the line and the slope of the line.
//		 * The data is represented in a tuple of the convention (x, y, a).
//		 * @param line a tuple of the convention (x, y, a).
//		 */
//		line(const tuple<double, double, double> & t) :
//			line(get<0>(t), get<1>(t), get<2>(t)) {}

		double a() const {
			return slope;
		}

		void a(double a){
			slope = a;
			assert(!std::isinf(slope));
						assert(!std::isnan(slope));
			updated = true;
		}

		pair<double, double> ab() const {
			return make_pair(slope, intercept);
		}

		void ab(double a, double b) {
			slope = a;
			intercept = b;
			assert(!std::isinf(slope));
						assert(!std::isnan(slope));
						assert(!std::isinf(intercept));
						assert(!std::isnan(intercept));
			updated = true;
		}
		void ab(double x, double y, double a){
			slope = a;
			intercept = y - a * x;
			assert(!std::isinf(slope));
						assert(!std::isnan(slope));
						assert(!std::isinf(intercept));
						assert(!std::isnan(intercept));
			updated = true;
		}
		void ab(const pair<double, double> p, double a){
			ab(p.first, p.second, a);
		}
		void ab(double x1, double y1, double x2, double y2){
			slope = (y2 - y1) / (x2 - x1);
			intercept = ((std::abs(x1)<std::abs(x2))?(y1 - slope * x1):(y2 - slope * x2));
			assert(!std::isinf(slope));
						assert(!std::isnan(slope));
						assert(!std::isinf(intercept));
						assert(!std::isnan(intercept));
			updated = true;
		}
		void ab(const pair<double, double> & p1, const pair<double, double> & p2){
			slope = (p2.second - p1.second) / (p2.first - p1.first);
			intercept = ((std::abs(p1.first)<std::abs(p2.first))?b(p1,slope):b(p2,slope));
			assert(!std::isinf(slope));
						assert(!std::isnan(slope));
						assert(!std::isinf(intercept));
						assert(!std::isnan(intercept));
			updated = true;
		}
//		void ab(const dclong::line & l){
//			slope = l.slope;
//			intercept = l.intercept;
//			assert(!std::isinf(slope));
//						assert(!std::isnan(slope));
//						assert(!std::isinf(intercept));
//						assert(!std::isnan(intercept));
//			updated = true;
//		}

		double b() const {
			return intercept;
		}

		void b(double b){
			intercept = b;
						assert(!std::isinf(intercept));
						assert(!std::isnan(intercept));
			updated = true;
		}

		static inline double b(const pair<double, double> & p, double a){
			return p.second - a * p.first;
		}
		/**
		 * Find the x coordinate of the intersection of this line and other line.
		 */
		double intersection(const line & other) const {
			return (other.intercept - intercept) / (slope - other.slope);
		}
		/**
		 * The difference of this line and other line at x.
		 * @param x
		 * @param line1
		 * @param line2
		 */
		inline double diff(double x, const line & other) const {
			return (slope - other.slope) * x + (intercept - other.intercept);
		}

		double value(double x) const {
			return value(x, slope, intercept);
		}
		static double value(double x, double a, double b) {
			if(a){
				return a * x + b;
			}
			return b;
		}

		pair<double, double> value(pair<double, double> r) const {
			return make_pair(value(r.first), value(r.second));
		}
		inline double increment(const pair<double, double> & r) const {
			return slope * (r.second - r.first);
		}
		/**
		 * Check the relationship between line 1 and line 2 at x.
		 * @param line1
		 * @param line2
		 * @param x
		 * @return 1 if lower, -1 if higher and 0 if equal.
		 */
		inline signed char less(double x, const line & other) const {
			if(!std::isinf(x)){
				return -dclong::sign(diff(x, other));
			}
			int ads = dclong::sign(slope - other.slope);
            // x == inf or x == -inf
			if(x>0){
                // x == inf
				return -ads;
			}
            // x == -inf
			return ads;
		}
		pair<signed char, signed char> less(const pair<double, double> & r, const line & other) const{
			return make_pair(less(r.first, other), less(r.second, other));
		}
		virtual string to_string() const {
			return dclong::to_string(make_pair(slope, intercept), "Line: (", ")");
		}
};

}



#endif /* LINE_HPP_ */
