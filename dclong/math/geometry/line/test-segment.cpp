/*
 * test-line.cpp
 *
 *  Created on: Oct 3, 2012
 *      Author: dclong
 */


#include "dclong/math/geometry/segment.hpp"
#include <iostream>
#include <iomanip>
using namespace std;
/**
 * Line: (1.000000, 0.000000) Xrange: (0.000000, 1.000000) Yrange: (0.000000, 1.000000) Difference: (1.000000, 1.000000)
 * Line: (2.000000, 0.000000) Xrange: (0.000000, 1.000000) Yrange: (0.000000, 2.000000) Difference: (1.000000, 2.000000)
 * Line: (2.000000, 0.000000) Xrange: (-inf, 1.000000) Yrange: (-inf, 2.000000) Difference: (inf, inf)
 * Line: (2.000000, 0.000000) Xrange: (-inf, inf) Yrange: (-inf, inf) Difference: (inf, inf)
 * Line: (2.000000, 0.000000) Xrange: (0.000000, inf) Yrange: (0.000000, inf) Difference: (inf, inf)
 * Line: (2.000000, 0.000000) Xrange: (0.000000, 1.000000) Yrange: (0.000000, 2.000000) Difference: (1.000000, 2.000000)
 * Line: (2.000000, -inf) Xrange: (0.000000, 1.000000) Yrange: (-inf, -inf) Difference: (1.000000, -nan)
 * Line: (1.000000, 0.000000) Xrange: (0.000000, 1.000000) Yrange: (0.000000, 1.000000) Difference: (1.000000, 1.000000)
 *
 */
/**
void test1(){
    cout << "Test1:" << endl;
	static constexpr double inf{numeric_limits<double>::infinity()};
    typedef dclong::segment segt;
	segt seg1{dclong::line(1, 0), make_pair(0, 1)};
    seg1.update_state();
    cout << seg1.to_string() << endl;
    seg1.a(2);
    seg1.update_state();
    cout << seg1.to_string() << endl;
    seg1.x1(-inf);
    seg1.update_state();
    cout << seg1.to_string() << endl;
    seg1.x2(inf);
    seg1.update_state();
    cout << seg1.to_string() << endl;
    seg1.x1(0);
    seg1.update_state();
    cout << seg1.to_string() << endl;
	seg1.x2(1);
    seg1.update_state();
    cout << seg1.to_string() << endl;
    seg1.b(-inf);
    seg1.update_state();
    cout << seg1.to_string() << endl;
    seg1.reset(dclong::line(1, 0), make_pair(0, 1));
    seg1.update_state();
    cout << seg1.to_string() << endl;
}
/**
 * Line: (1.000000, 0.000000) Xrange: (0.000000, 1.000000) Yrange: (0.000000, 1.000000) Difference: (1.000000, 1.000000)
 * Line: (0.000000, 1.000000) Xrange: (-inf, inf) Yrange: (1.000000, 1.000000) Difference: (inf, 0.000000)
 * Line: (0.000000, 1.000000) Xrange: (-inf, inf) Yrange: (1.000000, 1.000000) Difference: (inf, 0.000000)
 * Line: (1.000000, 0.000000) Xrange: (0.000000, 1.000000) Yrange: (0.000000, 1.000000) Difference: (1.000000, 1.000000)
 *
 */
void test2(){
    cout << "Test2:" << endl;
	static constexpr double inf{numeric_limits<double>::infinity()};
    typedef dclong::segment segt;
    segt s1{dclong::line(1, 0), make_pair(0, 1)};
    s1.update_state();
    cout << s1.to_string() << endl;
    segt s2{dclong::line(0, 1), make_pair(-inf, inf)};
    s2.update_state();
    cout << s2.to_string() << endl;
    std::swap(s1, s2);
    cout << s1.to_string() << endl;
    cout << s2.to_string() << endl;
}

int main(){
    //test1();
    test2();
}
