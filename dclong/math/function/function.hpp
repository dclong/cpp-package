/*
 * function.hpp
 *
 *  Created on: Oct 8, 2012
 *      Author: dclong
 */

#ifndef FUNCTION_HPP_
#define FUNCTION_HPP_

#include <cmath>
#include <limits>

namespace dclong{
using namespace std;

	double exp(double x){
		if(!std::isinf(x)){
			return std::exp(x);
		}
		if(x>0){
			return numeric_limits<double>::infinity();
		}
		return 0;
	}

	double log(double x){
		if(x>0&&!std::isinf(x)){
			return std::log(x);
		}
		if(x==0){
			return -numeric_limits<double>::infinity();
		}
		if(x<0){
			return numeric_limits<double>::quiet_NaN();
		}
		return numeric_limits<double>::infinity();
	}

	template <typename T> int sign(T val) {
			return (T(0) < val) - (val < T(0));
		}
}


#endif /* FUNCTION_HPP_ */
