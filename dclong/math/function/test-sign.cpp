/*
 * test-sign.cpp
 *
 *  Created on: Oct 2, 2012
 *      Author: dclong
 */

#include "dclong/math/math.hpp"
#include <iostream>
#include <limits>
using namespace std;

void test1(){
    const double inf = numeric_limits<double>::infinity();
    const double nan = numeric_limits<double>::signaling_NaN();
	cout << "sign of 1: " << dclong::sign(1) << endl;
	cout << "sign of 0: " << dclong::sign(0) << endl;
	cout << "sign of -0: " << dclong::sign(-0) << endl;
	cout << "sign of -1: " << dclong::sign(-1) << endl;
	cout << "sign of 1.0: " << dclong::sign(1.0) << endl;
	cout << "sign of 0.0: " << dclong::sign(0.0) << endl;
	cout << "sign of -0.0: " << dclong::sign(-0.0) << endl;
	cout << "sign of -1.0: " << dclong::sign(-1.0) << endl;
	cout << "sign of inf: " << dclong::sign(inf) << endl;
	cout << "sign of -inf: " << dclong::sign(-inf) << endl;
	cout << "sign of NaN: " << dclong::sign(nan) << endl;
	cout << "sign of -NaN: " << dclong::sign(-nan) << endl;
}
int main(){
    test1();
}
