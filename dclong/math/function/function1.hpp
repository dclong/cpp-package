/*
 * function.hpp
 *
 *  Created on: Oct 8, 2012
 *      Author: dclong
 */

#ifndef FUNCTION_HPP_
#define FUNCTION_HPP_

#include <cmath>
#include <limits>

namespace dclong{
using namespace std;

	template<bool Identical> double exp(double x);

	template<> double exp<true>(double x){
		return x;
	}

	template<> double exp<false>(double x){
		if(!std::isinf(x)){
			return std::exp(x);
		}
		if(x>0){
			return numeric_limits<double>::infinity();
		}
		return 0;
	}

	template<bool Identical> double log(double x);

	template<> double log<true>(double x){
		return x;
	}

	template<> double log<false>(double x){
		if(x>0&&!std::isinf(x)){
			return std::log(x);
		}
		if(x<0){
			return numeric_limits<double>::quiet_NaN();
		}
		if(x==0){
			return -numeric_limits<double>::infinity();
		}
		return numeric_limits<double>::infinity();
	}

	template<bool Diff> double ratiodiff(double x1, double x2);

	template<> double ratiodiff<true>(double x1, double x2){
		return x1 - x2;
	}

	template<> double ratiodiff<false>(double x1, double x2){
		return x1 / x2;
	}

	template<bool Exp, bool Identical> double logexp(double x);

	template<> double logexp<true, true>(double x){
		return dclong::exp<true>(x);
	}
	template<> double logexp<true, false>(double x){
		return dclong::exp<false>(x);
	}
	template<> double logexp<false, true>(double x){
		return dclong::log<true>(x);
	}
	template<> double logexp<false, false>(double x){
		return dclong::log<false>(x);
	}
}

#endif /* FUNCTION_HPP_ */
