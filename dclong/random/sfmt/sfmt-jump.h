#ifndef SFMT_JUMP_H
#define SFMT_JUMP_H

#include "sfmt.h"
#include <string>
/**
 * @file SFMT-jump.h
 *
 * @brief jump header file.
 *
 * @author Mutsuo Saito (Hiroshima University)
 * @author Makoto Matsumoto (The University of Tokyo)
 *
 * Copyright (C) 2012 Mutsuo Saito, Makoto Matsumoto,
 * Hiroshima University and The University of Tokyo.
 * All rights reserved.
 *
 * The 3-clause BSD License is applied to this software, see
 * LICENSE.txt
 */
#if defined(__cplusplus)
extern "C" {
#endif



//void SFMT_jump(sfmt_t * sfmt, const char * jump_str);
void SFMT_jump2(sfmt_t * sfmt, const std::string & jump_string);

#if defined(__cplusplus)
}
#endif

#endif
