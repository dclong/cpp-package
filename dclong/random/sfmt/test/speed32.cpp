
#include "/home/dclong/Dropbox/code/cpp/cackage/util/util.h"
#include "../sfmt19937-32.h"
#include <random>

using namespace std;

int main(){
    mt19937 mt;
    sfmt19937_32 sfmt;
    normal_distribution<double> normal(0,1);
    dclong::timer timer;
    int n = 100000000;
    timer.start();
    for(int i=0; i<n; ++i){
        mt();
    }
    timer.stop();
    timer.print_seconds("generating numbers using mt");
    timer.start();
    for(int i=0; i<n; ++i){
        sfmt();
    }
    timer.stop();
    timer.print_seconds("generating numbers using sfmt");
} 
    
    
