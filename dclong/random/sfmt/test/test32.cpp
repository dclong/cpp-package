#include "../sfmt19937.h"
#include <iostream>
#include <random>
using namespace std;

template<typename T> double mean(const std::vector<T> & x){
    double m = 0;
    for(auto e : x){
        m += e;
    }
    m /= x.size();
    return m;
}

int main(){
    dclong::sfmt19937<uint32_t> sfmt;
    negative_binomial_distribution<int> nb(3,0.3);
    sfmt.seed(119);
    vector<int> x;
    int n = 10000;
    for(int i=0; i<n; ++i){
        x.push_back(nb(sfmt));
    }
    cout<<"The mean is: "<<mean(x)<<"\n";
}


