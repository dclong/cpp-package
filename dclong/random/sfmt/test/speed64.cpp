
#include "/home/dclong/Dropbox/code/cpp/cackage/util/util.h"
#include "../sfmt19937-64.h"
#include <random>

using namespace std;

int main(){
    mt19937_64 mt;
    sfmt19937_64 sfmt;
    normal_distribution<double> normal(0,1);
    dclong::timer timer;
    int n = 100000000;
    timer.start();
    for(int i=0; i<n; ++i){
        mt();
    }
    timer.stop();
    timer.print_seconds("generating numbers using mt");
    timer.start();
    for(int i=0; i<n; ++i){
        sfmt();
    }
    timer.stop();
    timer.print_seconds("generating numbers using sfmt");
} 
    
    
