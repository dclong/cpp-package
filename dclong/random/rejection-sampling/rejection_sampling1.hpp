/*
 * rejection_sampling.hpp
 *
 *  Created on: Oct 7, 2012
 *      Author: dclong
 */

#ifndef REJECTION_SAMPLING_HPP_
#define REJECTION_SAMPLING_HPP_

#include "piecewise_segment_distribution.hpp"
#include <cmath>
#include <random>

namespace dclong {

template<bool Exp, bool Squeeze> struct rejection_sampling_bounds {};

template<bool Exp> struct rejection_sampling_bounds<Exp, true> {
	dclong::piecewise_segment_distribution<Exp> upper_bound;
	dclong::piecewise_segment_distribution<Exp> lower_bound;
	rejection_sampling_bounds<Exp, true>(const dclong::piecewise_segment_distribution<Exp> & upper_bound,
		const dclong::piecewise_segment_distribution<Exp> & lower_bound) :
		upper_bound{upper_bound},
		lower_bound{lower_bound} {}
	rejection_sampling_bounds<Exp, true>(dclong::piecewise_segment_distribution<Exp> && upper_bound,
		const dclong::piecewise_segment_distribution<Exp> & lower_bound) :
		upper_bound{move(upper_bound)},
		lower_bound{move(lower_bound)} {}
};

template<bool Exp> struct rejection_sampling_bounds<Exp, false> {
	dclong::piecewise_segment_distribution<Exp> upper_bound;
	rejection_sampling_bounds<Exp, false>(const dclong::piecewise_segment_distribution<Exp> & upper_bound) :
		upper_bound{upper_bound} {}
	rejection_sampling_bounds<Exp, false>(dclong::piecewise_segment_distribution<Exp> && upper_bound) :
		upper_bound{move(upper_bound)} {}
};

template<class Density, bool LogDensity, bool Exp, bool Squeeze, class RandomEngine> double gen
	(Density & density, rejection_sampling_bounds<Exp, true> & bounds,
	uniform_real_distribution<double> & urdist, RandomEngine & rng);

template<class Density, bool Exp, class RandomEngine> double gen<Density, true, Exp, true, RandomEngine>
		(Density & density, rejection_sampling_bounds<Exp, true> & bounds,
				uniform_real_distribution<double> & urdist, RandomEngine & rng){
	while(true){
		double x{bounds.upper_bound(rng)};
		// use 1 - udist(rng) to avoid possible log(0)
		double y{std::log(1 - urdist(rng)) + bounds.upper_bound.log_value(x)};
		if(y <= bounds.lower_bound.log_value(x)){
			return x;
		}
		if(y <= density.log_value()(x)){
			return x;
		}
	}
}

template<class Density, bool Exp, class RandomEngine> double gen<Density, false, Exp, true, RandomEngine>
		(Density & density, rejection_sampling_bounds<Exp, true> & bounds,
				uniform_real_distribution<double> & urdist, RandomEngine & rng){
	while(true){
		double x{bounds.upper_bound(rng)};
		// use 1 - udist(rng) to avoid possible log(0)
		double y{urdist(rng) * bounds.upper_bound.log_value(x)};
		if(y <= bounds.lower_bound.value(x)){
			return x;
		}
		if(y <= density.value()(x)){
			return x;
		}
	}
}

template<class Density, bool Exp> template<class RandomEngine> double gen
		(rejection_sampling<Density, false, Exp, true> & rs, RandomEngine & rng){
	while(true){
		double x{rs.bounds.upper_bound()};
		// use 1 - udist(rng) to avoid possible log(0)
		double y{rs.urdist(rng) * rs.upper_bound.value(x)};
		if(y <= rs.lower_bound.value(x)){
			return x;
		}
		if(y <= rs.density.value()(x)){
			return x;
		}
	}
}

template<class Density, bool Exp> template<class RandomEngine> double gen
		(rejection_sampling<Density, true, Exp, false> & rs, RandomEngine & rng){
	while(true){
		double x{rs.bounds.upper_bound()};
		// use 1 - udist(rng) to avoid possible log(0)
		double y{std::log(1 - rs.urdist(rng)) + rs.upper_bound.log_value(x)};
		if(y <= rs.density.log_value()(x)){
			return x;
		}
	}
}

template<class Density, bool Exp> template<class RandomEngine> double gen
		(rejection_sampling<Density, false, Exp, false> & rs, RandomEngine & rng){
	while(true){
		double x{rs.bounds.upper_bound()};
		// use 1 - udist(rng) to avoid possible log(0)
		double y{rs.urdist(rng) * rs.upper_bound.value(x)};
		if(y <= rs.density.value()(x)){
			return x;
		}
	}
}

template<class Density, bool LogDensity, bool Exp, bool Squeeze, class RandomEngine> class rejection_sampling {
	private:
		uniform_real_distribution<double> urdist{0, 1};
		rejection_sampling_bounds<Exp, Squeeze> bounds;
		// log density (up to a constant) of the distribution to be sampled from
		Density density;
	public:
		void swap(rejection_sampling<Density, LogDensity, Exp, Squeeze, RandomEngine> & other){
			std::swap(urdist, other.urdist);
			std::swap(bounds, other.bounds);
			std::swap(density, other.density);
		}
		rejection_sampling(const rejection_sampling<Density, LogDensity, Exp, Squeeze, RandomEngine> & other) :
			urdist{other.urdist},
			bounds{other.bounds},
			density{other.density} {}

		rejection_sampling(rejection_sampling<Density, LogDensity, Exp, Squeeze, RandomEngine> && other) :
					urdist{move(other.urdist)},
					bounds{move(other.bounds)},
					density{move(other.density)} {}

		rejection_sampling<Density, LogDensity, Exp, Squeeze, RandomEngine> & operator=(
				const rejection_sampling<Density, LogDensity, Exp, Squeeze, RandomEngine> & other){
			rejection_sampling<Density, LogDensity, Exp, Squeeze>(other).swap(*this);
			return *this;
		}

		rejection_sampling<Density, LogDensity, Exp, Squeeze, RandomEngine> & operator=(
				rejection_sampling<Density, LogDensity, Exp, Squeeze, RandomEngine> && other){
			rejection_sampling<Density, LogDensity, Exp, Squeeze>(other).swap(*this);
			return *this;
		}

		rejection_sampling(Density density, rejection_sampling_bounds<Exp, Squeeze> bounds) :
			density(density),
			bounds{bounds} {}

		rejection_sampling(Density density,
			const dclong::piecewise_segment_distribution<Exp> & upper_bound,
			const dclong::piecewise_segment_distribution<Exp> & lower_bound) :
			density(density),
			bounds{upper_bound, lower_bound} {}

		rejection_sampling(Density density,
			const dclong::piecewise_segment_distribution<Exp> & upper_bound) :
			density(density),
			bounds{upper_bound} {}
		/**
		 * Generate a bound of random numbers from the specified distribution.
		 * @param n the number of draws to generate.
		 */
		template<class InputIt> double operator()(InputIt first, InputIt last, RandomEngine & rng){
			for(InputIt it{first}; it!=last; ++it){
				*it = operator()(rng);
			}
		}

		double operator()(RandomEngine & rng){
			return gen(*this, rng);
		}

		friend double gen(rejection_sampling<Density, LogDensity, Exp, Squeeze> &,
				RandomEngine &);

		void update_state(){// I don't think it's necessary to update lower_bound
			bounds.upper_bound.update_state();
		}

};



}
#endif /* REJECTION_SAMPLING_HPP_ */
