/*
 * test-rejection.cpp
 *
 *  Created on: Oct 7, 2012
 *      Author: dclong
 */
#include "../rejection_sampling.hpp"
#include "/home/dclong/Dropbox/code/cpp/cackage/math/function/function.hpp"
#include <cmath>
#include <utility>
#include <random>
#include <iostream>
#include <fstream>
#include <limits>

using namespace std;
typedef mt19937 RandomEngine;

struct Density {
	double value(double x){
		if(x>-1&&x<1){
			return std::sqrt(1 - x * x);
		}
		return 0;
	}
};



void test1(){
	typedef dclong::piecewise_segment_distribution<false> psdf;
    vector<double> x{-1, 1};
    vector<double> y{1, 1};
    psdf ub(x, y);
    Density density;
	dclong::rejection_sampling<false, Density, false> rs(density, ub);
    RandomEngine rng(1111111);
    int n = 10000;
    vector<double> sam(n);
    for(int i=0; i<n; ++i){
        sam[i] = rs(rng);
    }
    ofstream ofs("circle.bin", ios::binary);
    if(ofs){
        ofs.write(reinterpret_cast<char*>(&sam[0]), sizeof(double) * n);
        ofs.close();
    }
}
struct Density2{
    bool constexpr has_derivative(){
        return true;
    }
    double derivative(double x){
        return -x;
    }
    double value(double x){
        return -0.5 * x * x;
    }
};
void test2(){
	typedef dclong::piecewise_segment_distribution<false> psdf;
    typedef pair<double, double> dpair_t;
    static const double inf{numeric_limits<double>::infinity()};
    dclong::line l1(make_pair(-2, -2), 2);
    dclong::line l2(make_pair(2, -2), -2);
    vector<dclong::line> ls{l1, l2};
    dclong::piecewise_segment_distribution<true> ub(ls, -inf, inf, 2);
    ub.update_state();
    RandomEngine rng(1111111);
    int n = 10000;
    vector<double> x(n);
    for(int i=0; i<n; ++i){
        x[i] = ub(rng);
    }
    ofstream ofs("nub.bin", ios::binary);
    if(ofs){
        ofs.write(reinterpret_cast<char*>(&x[0]), sizeof(double) * n);
        ofs.close();
    }
}
void test3(){
	typedef dclong::piecewise_segment_distribution<false> psdf;
    typedef pair<double, double> dpair_t;
    static const double inf{numeric_limits<double>::infinity()};
    dclong::line l1(make_pair(-2, -2), 2);
    dclong::line l2(make_pair(2, -2), -2);
    vector<dclong::line> ls{l1, l2};
    dclong::piecewise_segment_distribution<true> ub(ls, -inf, inf, 2);
    ub.update_state();
    cout << ub.to_string() << endl;

    Density2 density;
    dclong::rejection_sampling<true, Density2, false> rs(density, ub);
    RandomEngine rng(1111111);
    int n = 100000;
    vector<double> x(n);
    for(int i=0; i<n; ++i){
        x[i] = rs(rng);
    }
    ofstream ofs("normal.bin", ios::binary);
    if(ofs){
        ofs.write(reinterpret_cast<char*>(&x[0]), sizeof(double) * n);
        ofs.close();
    }
}
void test4(){
    typedef pair<double, double> dpair_t;
    static const double inf{numeric_limits<double>::infinity()};
    Density2 density;
    vector<double> xs{-2, 2};
    dclong::rejection_sampling<true, Density2, false> rs(density, xs, -inf, inf);
    cout << rs.to_string() << endl;
    RandomEngine rng(1111111);
    int n = 100000;
    vector<double> sam(n);
    for(int i=0; i<n; ++i){
        sam[i] = rs(rng);
    }
    ofstream ofs("normal.bin", ios::binary);
    if(ofs){
        ofs.write(reinterpret_cast<char*>(&sam[0]), sizeof(double) * n);
        ofs.close();
    }
}
    


int main(){
    test4();
}
