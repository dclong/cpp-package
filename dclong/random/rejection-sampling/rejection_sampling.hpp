/*
 * rejection_sampling.hpp
 *
 *  Created on: Oct 8, 2012
 *      Author: dclong
 */

#ifndef REJECTION_SAMPLING_HPP_
#define REJECTION_SAMPLING_HPP_

#include "piecewise_segment_distribution.hpp"
#include "/home/dclong/Dropbox/code/cpp/cackage/math/function/function.hpp"
#include <cmath>
#include <random>

namespace dclong {
using namespace std;

template<bool Exp, bool Squeeze> struct rejection_sampling_bounds {
};

template<bool Exp> struct rejection_sampling_bounds<Exp, true> {
	dclong::piecewise_segment_distribution<Exp> upper_bound;
	dclong::piecewise_segment_distribution<Exp> lower_bound;
	string to_string() const{
		return "Upper Bound:\n" + upper_bound.to_string() + "\nLower Bound:\n" + lower_bound.to_string();
	}
};

template<bool Exp> struct rejection_sampling_bounds<Exp, false> {
	dclong::piecewise_segment_distribution<Exp> upper_bound;
	string to_string() const{
		return upper_bound.to_string();
	}
};

template<bool Exp, class Density, bool Squeeze> class rejection_sampling {
private:
	uniform_real_distribution<double> urdist { 0, 1 };
	template<bool, bool> struct gen_param {
	};
	void build_upper_bound(const vector<double> & upper_xs, double minx,
			double maxx, int capacity) {
		if (density.has_derivative()) {
			// use derivative
			int size = upper_xs.size();
			vector<pair<double, double> > ps(size);
			vector<double> as(size);
			for (int i = 0; i < size; ++i) {
				ps[i] = make_pair(upper_xs[i], density.value(upper_xs[i]));
				as[i] = density.derivative(upper_xs[i]);
			}
			bounds.upper_bound = piecewise_segment_distribution<Exp>(ps, as,
					minx, maxx, capacity);
		} else {
			// derivative-free, do it from left to right and then from right to left

		}
	}
	void build_lower_bound(const vector<double> & lower_xs, int capacity) {
		int size = lower_xs.size();
		vector<double> ys(size);
		for (int i = 0; i < size; ++i) {
			ys[i] = density.value(lower_xs[i]);
		}
		bounds.lower_bound = piecewise_segment_distribution<Exp>(lower_xs, ys,
				capacity);
	}
	void build_lower_bound(const vector<double> & lower_xs) {
		build_lower_bound(lower_xs, lower_xs.size());
	}
protected:
	rejection_sampling_bounds<Exp, Squeeze> bounds;
	Density density;
public:

	void swap(rejection_sampling<Exp, Density, Squeeze> & other) {
		std::swap(urdist, other.urdist);
		std::swap(bounds, other.bounds);
		std::swap(density, other.density);
	}
	rejection_sampling(const rejection_sampling<Exp, Density, Squeeze> & other) :
			urdist { other.urdist }, bounds { other.bounds }, density {
					other.density } {
	}

	rejection_sampling(rejection_sampling<Exp, Density, Squeeze> && other) :
	urdist {move(other.urdist)},
	bounds {move(other.bounds)},
	density {move(other.density)} {}

	rejection_sampling<Exp, Density, Squeeze> & operator=(
			const rejection_sampling<Exp, Density, Squeeze> & other) {
		rejection_sampling<Exp, Density, Squeeze>(other).swap(*this);
		return *this;
	}

	rejection_sampling<Exp, Density, Squeeze> & operator=(
			rejection_sampling<Exp, Density, Squeeze> && other) {
		rejection_sampling<Exp, Density, Squeeze>(other).swap(*this);
		return *this;
	}

	rejection_sampling(const Density & density, const vector<double> & upper_xs,double minx, double maxx, int capacity) :
	density(density) {
		build_upper_bound(upper_xs, minx, maxx, capacity);
	}

	rejection_sampling(const Density & density, const vector<double> & upper_xs, double minx, double maxx) :
	rejection_sampling(density, upper_xs, minx, maxx, upper_xs.size()) {}

	rejection_sampling(const Density & density, const vector<double> & upper_xs, int capacity) :
		rejection_sampling(density, upper_xs, upper_xs[0],
				upper_xs[upper_xs.size()-1], capacity) {	}

	rejection_sampling(const Density & density, const vector<double> & upper_xs) :
			rejection_sampling(density, upper_xs, upper_xs.size()) {	}

	rejection_sampling(const Density & density, const vector<double> & upper_xs, double minx, double maxx,
			const vector<double> & lower_xs, int capacity) :
	density(density) {
		build_upper_bound(upper_xs, minx, maxx, capacity);
		build_lower_bound(lower_xs, capacity);
	}

	rejection_sampling(const Density & density, const vector<double> & upper_xs, double minx, double maxx,
			const vector<double> & lower_xs) :
		rejection_sampling(density, upper_xs, minx, maxx, lower_xs, upper_xs.size()) {	}

	rejection_sampling(const Density & density, const vector<double> & upper_xs,
				const vector<double> & lower_xs, int capacity) :
			rejection_sampling(density, upper_xs, upper_xs[0], upper_xs[upper_xs.size()-1], lower_xs, capacity) {	}

	rejection_sampling(const Density & density, const vector<double> & upper_xs,
					const vector<double> & lower_xs) :
				rejection_sampling(density, upper_xs, lower_xs, upper_xs.size()) {	}

	rejection_sampling(const Density & density, rejection_sampling_bounds<Exp, Squeeze> bounds) :
	density(density),
	bounds {bounds} {	}

	rejection_sampling(const Density & density,
			const dclong::piecewise_segment_distribution<Exp> & upper_bound,
			const dclong::piecewise_segment_distribution<Exp> & lower_bound) :
	density(density) {
		bounds.upper_bound = upper_bound;
		bounds.lower_bound = lower_bound;	}

	rejection_sampling(const Density & density,
				const dclong::piecewise_segment_distribution<Exp> && upper_bound,
				const dclong::piecewise_segment_distribution<Exp> && lower_bound) :
		density(density) {
			bounds.upper_bound = move(upper_bound);
			bounds.lower_bound = move(lower_bound);		}
	rejection_sampling(const Density & density,
					const dclong::piecewise_segment_distribution<Exp> & upper_bound,
					const dclong::piecewise_segment_distribution<Exp> && lower_bound) :
			density(density) {
				bounds.upper_bound = upper_bound;
				bounds.lower_bound = move(lower_bound);			}
	rejection_sampling(const Density & density,
					const dclong::piecewise_segment_distribution<Exp> && upper_bound,
					const dclong::piecewise_segment_distribution<Exp> & lower_bound) :
			density(density) {
				bounds.upper_bound = move(upper_bound);
				bounds.lower_bound = lower_bound;
			}
	rejection_sampling(const Density & density,
			const dclong::piecewise_segment_distribution<Exp> & upper_bound) :
	density(density) {
		bounds.upper_bound = upper_bound;
	}

	rejection_sampling(const Density & density,
				const dclong::piecewise_segment_distribution<Exp> && upper_bound) :
		density(density) {
			bounds.upper_bound = move(upper_bound);
		}
	/**
	 * Generate a bound of random numbers from the specified distribution.
	 * @param n the number of draws to generate.
	 */
	template<class InputIt, class RandomEngine> double genrnd(InputIt first, InputIt last, RandomEngine & rng) {
		for(InputIt it {first}; it!=last; ++it) {
			*it = genrnd(rng);
		}
	}
	template<class InputIt, class RandomEngine> double operator()(InputIt first, InputIt last, RandomEngine & rng) {
			for(InputIt it {first}; it!=last; ++it) {
				*it = operator()(rng);
			}
		}
	template<class RandomEngine> double genrnd(RandomEngine & rng) {
		return gen(gen_param<Exp, Squeeze>(), rng);
	}
	template<class RandomEngine> double operator()(RandomEngine & rng) {
			return gen(gen_param<Exp, Squeeze>(), rng);
		}
	rejection_sampling_bounds<Exp, Squeeze> get_bounds() const {
		return bounds;
	}
	virtual string to_string() const {
		return bounds.to_string();
	}
	virtual ~rejection_sampling() {}

private:
	template<class RandomEngine> double gen(gen_param<Exp, Squeeze> &, RandomEngine &);

	template<class RandomEngine> double gen(gen_param<true, true>, RandomEngine & rng) {
		while(true) {
			double x {bounds.upper_bound(rng)};
			double y {dclong::log(urdist(rng)) + bounds.upper_bound.value(x)};
			if(y <= bounds.lower_bound.value(x)) {
				return x;
			}
			if(y <= density.value(x)) {
				return x;
			}
		}
	}
	template<class RandomEngine> double gen(gen_param<false, true>, RandomEngine & rng) {
		while(true) {
			double x {bounds.upper_bound(rng)};
			double y {urdist(rng) * bounds.upper_bound.value(x)};
			if(y <= bounds.lower_bound.value(x)) {
				return x;
			}
			if(y <= density.value(x)) {
				return x;
			}
		}
	}
	template<class RandomEngine> double gen(gen_param<true, false>, RandomEngine & rng) {
		while(true) {
			double x {bounds.upper_bound(rng)};
			double y {dclong::log(urdist(rng)) + bounds.upper_bound.value(x)};
			if(y <= density.value(x)) {
				return x;
			}
		}
	}
	template<class RandomEngine> double gen(gen_param<false, false>, RandomEngine & rng) {
		while(true) {
			double x {bounds.upper_bound(rng)};
			double y {urdist(rng) * bounds.upper_bound.value(x)};
			if(y <= density.value(x)) {
				return x;
			}
		}
	}
}
			;}
#endif /* REJECTION_SAMPLING_HPP_ */
