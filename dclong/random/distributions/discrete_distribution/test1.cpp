/*
 * test1.cpp
 *
 *  Created on: Oct 26, 2012
 *      Author: dclong
 */


#include "dclong/utility/utility.hpp"
#include "dclong/string/string.hpp"
#include "dclong/random/distributions/discrete_distribution/discrete_distribution.hpp"
#include <random>
#include <iostream>
using namespace std;
void test1(){
    int k { 1000 };
	vector<int> x(k,1);
	//vector<int> x{1,2,3,4,5,6};
	dclong::discrete_distribution ddist(x.begin(), x.end());
	mt19937 rng;
	cout << ddist(rng) << endl;
	dclong::timer timer;
	int n = 10000000;
	vector<int> sam(n);
	ddist(sam.begin(), sam.end(), rng);
	timer.end();
	timer.print_milliseconds("generating 10,000,000 draws");
	vector<double> freq(k);
	for(const int i : sam){
		++freq[i];
	}
	for(vector<double>::reverse_iterator it{freq.rbegin()}; it!=freq.rend(); ++it){
		*it /= freq.front();
	}
	//cout << dclong::to_string(freq.begin(), freq.end()) << endl;
    std::discrete_distribution<int> ddist2(x.begin(), x.end());
    timer.begin();
    for(vector<int>::iterator it{sam.begin()}; it!=sam.end(); ++it){
        *it = ddist2(rng);
    }
    timer.end();
    timer.print_milliseconds("generating 10,000,000 draws");
}

int main(){
	test1();
}
