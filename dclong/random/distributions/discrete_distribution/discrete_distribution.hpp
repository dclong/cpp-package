/*
 * discrete_distribution.hpp
 *
 *  Created on: Oct 26, 2012
 *      Author: dclong
 */

#ifndef DCLONG_DISCRETE_DISTRIBUTION_HPP_
#define DCLONG_DISCRETE_DISTRIBUTION_HPP_

#include <vector>
#include <utility>
#include <algorithm>
namespace dclong {
using namespace std;

class discrete_distribution {
private:
	vector<pair<double, int>> table;
	vector<pair<double, int>>::iterator mit;
//	std::pdiff_t nlower;
//	std::pdiff_t nupper;
public:
	template<class InputIt> void init(InputIt first, InputIt last) {
		int i { 0 };
		InputIt wit { first };
		vector<pair<double, int> >::iterator tit { table.begin() };
		while (wit != last) {
			tit->first = *wit;
			tit->second = i;
			++tit;
			++wit;
			++i;
		}
		std::sort(table.begin(), table.end());
		vector<pair<double, int> >::iterator tit1 { table.begin() };
		vector<pair<double, int> >::iterator tit2 { ++table.begin() };
		while (tit2 != table.end()) {
			tit2->first += tit1->first;
			++tit1;
			++tit2;
		}
		double s = table.back().first;
		for (vector<pair<double, int>>::iterator tit { table.begin() };
				tit != table.end(); ++tit) {
			tit->first /= s;
		}
		mit = std::upper_bound(table.begin(), table.end(), 0.5,
				[](const double x1, const pair<double, int> & x2) {
					return x1 < x2.first;
				});
//		nlower = std::distance(table.begin(), mit) + 1;
//		nupper = std::distance(mit, table.end());
		table.back().first = 2;
	}
public:
	template<class InputIt> discrete_distribution(InputIt first, InputIt last) :
			table(std::distance(first, last)) {
		init(first, last);
	}

	discrete_distribution(){}

	template<class InputIt> void param(InputIt first, InputIt last){
		table.resize(std::distance(first, last));
		init(first, last);
	}

	template<class RandomEngine> int operator()(RandomEngine & rng){
		static uniform_real_distribution<double> urdist{0, 1};
		double p {urdist(rng)};
		if(p < mit->first){
			return std::upper_bound(table.begin(), mit, p, [](const double x1, const pair<double, int> & x2){
				return x1 < x2.first;
			})->second;
		}
		return std::upper_bound(mit+1, table.end(), p, [](const double x1, const pair<double, int> & x2){
			return x1 < x2.first;
		})->second;
	}
//	template<class RandomEngine> int operator()(RandomEngine & rng){
//			static uniform_real_distribution<double> urdist{0, 1};
//			double p {urdist(rng)};
//			if(p < mit->first){
//				if(nlower<6){
//
//				}
//				return std::upper_bound(table.begin(), mit, p, [](const double x1, const pair<double, int> & x2){
//					return x1 < x2.first;
//				})->second;
//			}
//			return std::upper_bound(mit+1, table.end(), p, [](const double x1, const pair<double, int> & x2){
//				return x1 < x2.first;
//			})->second;
//		}

//	template<class RandomEngine> int operator()(RandomEngine & rng) {
//		static uniform_real_distribution<double> urdist { 0, 1 };
//		double p { urdist(rng) };
//		if (p < mit->first) {
//			vector<pair<double, int> >::iterator it { mit - 1 };
//			while (it >= table.begin() && it->first > p) {
//				--it;
//			}
//			return (++it)->second;
//		}
//		vector<pair<double, int> >::iterator it { mit - 1 };
//		while (it != table.end() && it->first <= p) {
//			++it;
//		}
//		return it->second;
//
//	}

	template<class InputIt, class RandomEngine> void operator()(InputIt first,
			InputIt last, RandomEngine & rng) {
		for (InputIt it { first }; it != last; ++it) {
			*it = operator()(rng);
		}
	}

};

}

#endif /* DISCRETE_DISTRIBUTION_HPP_ */
