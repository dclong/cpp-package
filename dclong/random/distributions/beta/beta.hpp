/*
 * beta.hpp
 *
 *  Created on: Oct 22, 2012
 *      Author: dclong
 */

#ifndef BETA_HPP_
#define BETA_HPP_

#include <random>

namespace dclong {
using namespace std;

template<class RealType=double, class RandomEngine> double rbeta(double alpha, double beta, RandomEngine & rng){
	static gamma_distribution<RealType> gdist1;
	static gamma_distribution<RealType> gdist2;
	gdist1.param(typename gamma_distribution<RealType>::param_type(alpha, 1));
	RealType x{gdist1(rng)};
	gdist2.param(typename gamma_distribution<RealType>::param_type(beta, 1));
	RealType y{gdist2(rng)};
	return x / (x + y);
}

template<class RealType=double, class RandomEngine> double rbeta(const typename gamma_distribution<RealType>::param_type & param, RandomEngine & rng){
	return rbeta(param.alpha(), param.beta(), rng);
}

template<class InputIt, class RealType=double, class RandomEngine>
	void rbeta(InputIt first, InputIt last, double alpha, double beta, RandomEngine & rng){
	static gamma_distribution<RealType> gdist1;
	static gamma_distribution<RealType> gdist2;
	gdist1.param(typename gamma_distribution<RealType>::param_type(alpha, 1));
	gdist2.param(typename gamma_distribution<RealType>::param_type(beta, 1));
	for(InputIt it{first}; it!=last; ++it){
		double x{gdist1(rng)};
		double y{gdist2(rng)};
		*it = x / (x + y);
	}
}

template<class InputIt, class RealType=double, class RandomEngine>
	void rbeta(InputIt first, InputIt last, const typename gamma_distribution<RealType>::param_type & param, RandomEngine & rng){
	rbeta(first, last, param.alpha(), param.beta(), rng);
}
}

#endif /* BETA_HPP_ */
