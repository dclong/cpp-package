/*
 * test-beta.cpp
 *
 *  Created on: Oct 22, 2012
 *      Author: dclong
 */

#include "dclong/random/distributions/beta/beta.hpp"
#include <vector>
#include <fstream>
#include <iostream>
using namespace std;

void test1(){
	mt19937 rng;
	int n = 100000;
	vector<double> sam(n);
	dclong::rbeta(sam.begin(), sam.end(), 2, 3, rng);
	ofstream ofs("beta1.bin", ios::binary);
	if(ofs){
		ofs.write(reinterpret_cast<char*>(&sam[0]), sizeof(double) * sam.size());
		ofs.close();
	}
}

void test2(){
	mt19937 rng;
	int n = 100000;
	vector<double> sam(n);
	for(int i=0; i<n; ++i){
		sam[i] = dclong::rbeta(2, 3, rng);
	}
	ofstream ofs("beta2.bin", ios::binary);
		if(ofs){
			ofs.write(reinterpret_cast<char*>(&sam[0]), sizeof(double) * sam.size());
			ofs.close();
		}
}

int main(){
	test1();
	test2();
}
