/*
 * distributions.hpp
 *
 *  Created on: Oct 22, 2012
 *      Author: dclong
 */

#ifndef DISTRIBUTIONS_HPP_
#define DISTRIBUTIONS_HPP_

#include "dclong/random/distributions/beta/beta.hpp"
#include "dclong/random/distributions/discrete_distribution/discrete_distribution.hpp"


#endif /* DISTRIBUTIONS_HPP_ */
