/*
 * random.h
 *
 *  Created on: Aug 20, 2012
 *      Author: dclong
 */

#ifndef DCLONG_SHUFFLE_H_
#define DCLONG_SHUFFLE_H_

#include <vector>
#include <random>
namespace dclong {
using namespace std;   

template<typename InputIt, typename RandomEngine> 
void shuffle(InputIt first, InputIt last, RandomEngine & rng) {
	static uniform_int_distribution<long int> uid;
    long int i = std::distance(first, last);
     while(i > 1){
         --i;
         //generate an integer between 0 and i (both inclusive)
         long int index = uid(rng, uniform_int_distribution<long int>::param_type(0L, i));
         //swap x[i] and x[index]
         std::swap(*std::next(first, i), *std::next(first, index));
     }
}
/**
 * Shuffle vector x in place to generate random permutation of size k (<=n) at head.
 * @param x a vector to be shuffled.
 * @param k size of permutation.
 */
template<typename InputIt, typename RandomEngine> void shuffle_left(InputIt first, InputIt last, long int k, RandomEngine & rng) {
	long int n = std::distance(first, last);
	long int n_minus_one = n - 1;
	//no need to shuffle if only 1 element is left
        long int upper_bound = std::min(k, n_minus_one);
	static uniform_int_distribution<long int> uid;
	for(long int i = 0; i < upper_bound; ++i){
		//generate an index between i and n - 1 (inclusive)
		long int index = uid(rng, uniform_int_distribution<long int>::param_type(i, n_minus_one));
		//swap x[i] and x[index]
		std::swap(*std::next(first, i), *std::next(first, index));
	}
}

/**
 * Randomly shuffle vector x of size n to generate a random permutation of size k (<=n).
 * @param x a pointer to a double array to be shuffled.
 * @param k a pointer to the length the random permutation to be generated.
 */
template<typename InputIt, typename RandomEngine> void shuffle_right(InputIt first, InputIt last, long int k, RandomEngine & rng) {
	long int n = std::disntance(first, last);
	//no need to shuffle if only 1 element is left
	long int lower_bound = std::max(1, n - k);
	std::uniform_int_distribution<long int> uid;
	for(long int i = n - 1; i >= lower_bound; --i){
		//generate an index between 0 and i (inclusive)
		long int index = uid(rng,std::uniform_int_distribution<long int>::param_type(0, i));
		//swap x[i] and x[index]
            std::swap(*std::next(first, i), *std::next(first, index));
	}
}

}
#endif /* RANDOM_H_ */
