/*
 * test-random.cpp
 *
 *  Created on: Aug 21, 2012
 *      Author: dclong
 *
 *
 *      Test the shuffle algorithm. 
 *      It works well.
 */
#ifndef TEST_SHUFFLE_H_
#define TEST_SHUFFLE_H_

#include "../random.h"
#include "../../io/io.h"
#include <random>
#include <algorithm>
using namespace std;

void test_shuffle(){
	vector<int> data{0,1,2,3,4,5,6,7,8,9};
	vector<int> freq(10);
	mt19937 mt;
	int n = 100000;
	int k = 4;
	for(int i=0; i<n; ++i){
		dclong::shuffle_left(begin(data), end(data), k, mt);
		for_each(begin(data), begin(data) + k, [&freq](int x){
			freq[x]++;
		});
	}
	dclong::print(begin(freq),end(freq));
}

int main(){
	test_shuffle();
}

#endif
