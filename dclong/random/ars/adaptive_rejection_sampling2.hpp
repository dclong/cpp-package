/*
 * adaptive_rejection_sampling.hpp
 *
 *  Created on: Oct 7, 2012
 *      Author: dclong
 */

#ifndef ADAPTIVE_REJECTION_SAMPLING_HPP_
#define ADAPTIVE_REJECTION_SAMPLING_HPP_

#include "rejection_sampling.hpp"

namespace dclong {
using namespace std;
namespace helper{
template<bool> struct adaptive_rejection_sampling_bounds {
	};
	template<> struct adaptive_rejection_sampling_bounds<true> {
		double min_squeeze_ratio { 0.75 };
		double min_density_ratio { 0.8 };
		double observed_squeeze_ratio;
		double observed_density_ratio;
		double last_draw;

	};
	template<> struct adaptive_rejection_sampling_bounds<false> {
		double min_density_ratio { 0.8 };
		double observed_density_ratio;
		double last_draw;
	};
}
template<bool Exp, class Density, bool Squeeze> class adaptive_rejection_sampling: public dclong::rejection_sampling<
		Exp, Density, Squeeze> {
private:
	helper::adaptive_rejection_sampling_bounds<Squeeze> temp;
private:
	int max_segs;
	template<bool> struct update_bounds_param {
	};
	template<bool, bool> struct observed_ratios_param {
	};
	template<bool> void update_bounds(const update_bounds_param<Squeeze> &) {
	}
	void update_bounds(const update_bounds_param<true> &) {
		if (this->bounds.upper_bound.segs_number() < max_segs
				&& temp.observed_density_ratio < temp.min_density_ratio) {
			// update upper bound
			if (this->density.has_derivative()) {
				// use derivative
				pair<double, double> p{make_pair(temp.last_draw, this->density.value(temp.last_draw))};
				double a { this->density.derivative(temp.last_draw) };
				this->bounds.upper_bound.insert_line(dclong::line(p, a));
				this->bounds.upper_bound.update_state();
			} else {
				// derivative-free update

			}
		}
		if (this->bounds.lower_bound.segs_number() < max_segs
				&& temp.observed_squeeze_ratio < temp.min_squeeze_ratio) {
			// update lower bound
			pair<double, double> p{make_pair(temp.last_draw, this->density.value(temp.last_draw))};
			this->bounds.lower_bound.insert_point(p);
			// no need to update state of lower_bound
		}
	}
	void update_bounds(const update_bounds_param<false> &) {
		if (this->bounds.upper_bound.segs_number() < max_segs
				&& temp.observed_density_ratio < temp.min_density_ratio) {
			// update upper bound
			if (this->density.has_derivative()) {
				// use derivative
				pair<double, double> p = make_pair(temp.last_draw, this->density.value(temp.last_draw));
				double a { this->density.derivative(temp.last_draw) };
				this->bounds.upper_bound.insert_line(dclong::line(p, a));
				this->bounds.upper_bound.update_state();
			} else {
				// derivative-free update

			}
		}
	}
	template<bool, bool> void observed_ratios(
			const observed_ratios_param<Exp, Squeeze> &);
	void observed_ratios(const observed_ratios_param<true, true> &) { //Exp Squeeze
		double x { this->density.value(temp.last_draw) };
		double upper { this->bounds.upper_bound.value(temp.last_draw) };
		double lower { this->bounds.lower_bound.value(temp.last_draw) };
		temp.observed_squeeze_ratio = lower - x;
		temp.observed_density_ratio = x - upper;
	}
	void observed_ratios(const observed_ratios_param<true, false> &) {
		double x { this->density.value(temp.last_draw) };
		double upper { this->bounds.upper_bound.value(temp.last_draw) };
		temp.observed_density_ratio = x - upper;
	}
	void observed_ratios(const observed_ratios_param<false, true> &) { //Exp Squeeze
		double x { this->density.value(temp.last_draw) };
		double upper { this->bounds.upper_bound.value(temp.last_draw) };
		double lower { this->bounds.lower_bound.value(temp.last_draw) };
		temp.observed_squeeze_ratio = lower / x;
		temp.observed_density_ratio = x / upper;
	}
	void observed_ratios(const observed_ratios_param<false, false> &) {
		double x { this->density.value(temp.last_draw) };
		double upper { this->bounds.upper_bound.value(temp.last_draw) };
		temp.observed_density_ratio = x / upper;
	}
public:
	void min_density_ratio(double v) {
		temp.min_density_ratio = v;
	}
	double min_density_ratio() const {
		return temp.min_density_ratio;
	}
	void max_density_ratio(double v) {
		temp.max_density_ratio = v;
	}
	double max_density_ratio() const {
		return temp.max_density_ratio;
	}
	adaptive_rejection_sampling(const Density & density,
			const vector<double> & upper_xs, double minx, double maxx,
			int max_segs) :
			dclong::rejection_sampling<Exp, Density, Squeeze>(density, upper_xs, minx, maxx, max_segs),
			max_segs {max_segs } {}

			adaptive_rejection_sampling(const Density & density,
						const vector<double> & upper_xs, double minx, double maxx) :
			adaptive_rejection_sampling(density, upper_xs, minx, maxx, 15) {}

	adaptive_rejection_sampling(const Density & density,
			const vector<double> & upper_xs, int max_segs) :
			adaptive_rejection_sampling(density, upper_xs, upper_xs[0],
					upper_xs[upper_xs.size() - 1], max_segs) {}

	adaptive_rejection_sampling(const Density & density,
			const vector<double> & upper_xs) :
			adaptive_rejection_sampling(density, upper_xs, 15) {
	}

	adaptive_rejection_sampling(const Density & density,
			const vector<double> & upper_xs, double minx, double maxx,
			const vector<double> & lower_xs, int max_segs) :
			dclong::rejection_sampling<Exp, Density, Squeeze>(density, upper_xs, minx, maxx, lower_xs, max_segs),
			max_segs{max_segs} {	}

	adaptive_rejection_sampling(const Density & density,
			const vector<double> & upper_xs, double minx, double maxx,
			const vector<double> & lower_xs) :
			adaptive_rejection_sampling(density, upper_xs, minx, maxx, lower_xs,
					15) {}

	adaptive_rejection_sampling(const Density & density,
			const vector<double> & upper_xs, const vector<double> & lower_xs,
			int max_segs) :
			adaptive_rejection_sampling(density, upper_xs, upper_xs[0],
					upper_xs[upper_xs.size() - 1], lower_xs, max_segs) {	}

	adaptive_rejection_sampling(const Density & density,
			const vector<double> & upper_xs, const vector<double> & lower_xs) :
			adaptive_rejection_sampling(density, upper_xs, lower_xs, 15) {	}

	void swap(
			const adaptive_rejection_sampling<Exp, Density, Squeeze> & other) {
		dclong::rejection_sampling<Exp, Density, Squeeze>::swap(other);
		std::swap(temp, other.temp);
		std::swap(max_segs, other.max_segs);
	}
	adaptive_rejection_sampling & operator=(
			const adaptive_rejection_sampling<Exp, Density, Squeeze> & other) {
		adaptive_rejection_sampling(other).swap(*this);
		return *this;
	}
	adaptive_rejection_sampling & operator=(
			adaptive_rejection_sampling<Exp, Density, Squeeze> && other) {
				adaptive_rejection_sampling(other).swap(*this);
				return *this;
			}
			adaptive_rejection_sampling(const adaptive_rejection_sampling<Exp, Density, Squeeze> & other) :
			temp {other.temp},
			max_segs {other.max_segs} {}

			adaptive_rejection_sampling(const adaptive_rejection_sampling<Exp, Density, Squeeze> && other) :
			temp {move(other.temp)},
			max_segs {move(other.max_segs)} {}

			template<class RandomEngine> double operator()(RandomEngine & rng) {
				update_bounds(update_bounds_param<Squeeze>());
				temp.last_draw = dclong::rejection_sampling<Exp, Density, Squeeze>::operator()(rng);
				observed_ratios(observed_ratios_param<Exp, Squeeze>());
				return temp.last_draw;
			}
			template<class RandomEngine> double agenrnd(RandomEngine & rng) {
				update_bounds(update_bounds_param<Squeeze>());
				temp.last_draw = dclong::rejection_sampling<Exp, Density, Squeeze>::operator()(rng);
				observed_ratios(observed_ratios_param<Exp, Squeeze>());
				return temp.last_draw;
			}
			template<class InputIt, class RandomEngine> void operator()(InputIt first, InputIt last, RandomEngine & rng) {
				for(InputIt it {first}; it!=last; ++it) {
					*it = operator()(rng);
				}
			}
			template<class InputIt, class RandomEngine> void agenrnd(InputIt first, InputIt last, RandomEngine & rng) {
				for(InputIt it {first}; it!=last; ++it) {
					*it = agenrnd(rng);
				}
			}

			virtual ~adaptive_rejection_sampling() {}
		};

		}

#endif /* ADAPTIVE_REJECTION_SAMPLING_HPP_ */
