/*
 * adaptive_rejection_sampling.hpp
 *
 *  Created on: Oct 9, 2012
 *      Author: dclong
 */

#ifndef ADAPTIVE_REJECTION_SAMPLING_HPP_
#define ADAPTIVE_REJECTION_SAMPLING_HPP_

#include "dclong/random/psd/piecewise_segment_distribution.hpp"
#include <string>
#include <limits>
namespace dclong {
using namespace std;

template<bool Exp, bool Squeeze> struct adaptive_rejection_sampling_bounds {
};

template<bool Exp> struct adaptive_rejection_sampling_bounds<Exp, true> {
	double min_squeeze_ratio { 0.75 };
	double min_density_ratio { 0.8 };
	double observed_squeeze_ratio {numeric_limits<double>::infinity()};
	double observed_density_ratio {numeric_limits<double>::infinity()};
	double last_draw;
	double density_value;
	dclong::piecewise_segment_distribution<Exp> upper_bound;
	dclong::piecewise_segment_distribution<Exp> lower_bound;
	string to_string() const {
		return "Upper Bound:\n" + upper_bound.to_string() + "\nLower Bound:\n" + lower_bound.to_string();
	}
};

template<bool Exp> struct adaptive_rejection_sampling_bounds<Exp, false> {
	double min_density_ratio { 0.8 };
	double observed_density_ratio { numeric_limits<double>::infinity() };
	double last_draw;
	double density_value;
	dclong::piecewise_segment_distribution<Exp> upper_bound;
	string to_string() const {
		return upper_bound.to_string();
	}
};

template<bool Exp, class Density, bool Squeeze> class adaptive_rejection_sampling {
private:
	uniform_real_distribution<double> urdist { 0, 1 };
	adaptive_rejection_sampling_bounds<Exp, Squeeze> bounds;
	Density & density;
	int max_segs;
	void update_upper_bound() {
		if (bounds.observed_density_ratio < bounds.min_density_ratio) {
			if (this->density.has_derivative()) {
				// use derivative information to update upper bound
				double a { this->density.derivative(bounds.last_draw) };
				pair<double, double> p { make_pair(bounds.last_draw, this->bounds.density_value) };
				this->bounds.upper_bound.insert_line(dclong::line(p, a));
				this->bounds.upper_bound.update_state();
			} else {
				// derivative-free update

			}
		}
	}
	void update_lower_bound() {
		if (bounds.observed_squeeze_ratio < bounds.min_squeeze_ratio) {
			pair<double, double> p { make_pair(bounds.last_draw, this->bounds.density_value) };
			this->bounds.lower_bound.insert_point(p);
			// no need to update state of lower_bound
		}
	}
	template<bool, bool> struct gen_param {
	};
	template<bool E, bool S, class RandomEngine> double gen(const gen_param<E, S> &, RandomEngine &);
	template<class RandomEngine> double gen(const gen_param<true, true> &, RandomEngine & rng) {
		while (true) {
			double x { bounds.upper_bound(rng) };
			double y { dclong::log(urdist(rng)) + bounds.upper_bound.value(x) };
			if (y <= bounds.lower_bound.value(x)) {
				return x;
			}
			if (y <= density.value(x)) {
				return x;
			}
		}
	}
	template<class RandomEngine> double gen(const gen_param<false, true> &, RandomEngine & rng) {
		while (true) {
			double x { bounds.upper_bound(rng) };
			double y { urdist(rng) * bounds.upper_bound.value(x) };
			if (y <= bounds.lower_bound.value(x)) {
				return x;
			}
			if (y <= density.value(x)) {
				return x;
			}
		}
	}
	template<class RandomEngine> double gen(const gen_param<true, false> &, RandomEngine & rng) {
		while (true) {
			double x { bounds.upper_bound(rng) };
			double y { dclong::log(urdist(rng)) + bounds.upper_bound.value(x) };
			if (y <= density.value(x)) {
				return x;
			}
		}
	}
	template<class RandomEngine> double gen(const gen_param<false, false> &, RandomEngine & rng) {
		while (true) {
			double x { bounds.upper_bound(rng) };
			double y { urdist(rng) * bounds.upper_bound.value(x) };
			if (y <= density.value(x)) {
				return x;
			}
		}
	}

	template<bool E, bool S, class RandomEngine> double agen(gen_param<E, S>, RandomEngine &);
	template<class RandomEngine> double agen(gen_param<true, true>, RandomEngine & rng) {
		if (bounds.upper_bound.segs_number() < max_segs) {
			update_upper_bound();
			if (bounds.lower_bound.segs_number() < max_segs) {
				update_lower_bound();
				while (true) {
					bounds.last_draw = bounds.upper_bound(rng);
					bounds.density_value = density.value(bounds.last_draw);
					double upper { bounds.upper_bound.value(bounds.last_draw) };
					bounds.observed_density_ratio = dclong::exp(bounds.density_value - upper);
					double lower { bounds.lower_bound.value(bounds.last_draw) };
					bounds.observed_squeeze_ratio = dclong::exp(lower - bounds.density_value);
					double p { urdist(rng) };
					if (p <= bounds.observed_density_ratio) {
						return bounds.last_draw;
					}
					update_upper_bound();
					update_lower_bound();
				}
			} else {
				while (true) {
					bounds.last_draw = bounds.upper_bound(rng);
					bounds.density_value = density.value(bounds.last_draw);
					double upper { bounds.upper_bound.value(bounds.last_draw) };
					bounds.observed_density_ratio = dclong::exp(bounds.density_value - upper);
					double p { urdist(rng) };
					if (p <= bounds.observed_density_ratio) {
						return bounds.last_draw;
					}
					update_upper_bound();
				}
			}
		} else {
			if (bounds.lower_bound.segs_number() < max_segs) {
				update_lower_bound();
				while (true) {
					bounds.last_draw = bounds.upper_bound(rng);
					bounds.density_value = density.value(bounds.last_draw);
					double upper { bounds.upper_bound.value(bounds.last_draw) };
					bounds.observed_density_ratio = dclong::exp(bounds.density_value - upper);
					double lower { bounds.lower_bound.value(bounds.last_draw) };
					bounds.observed_squeeze_ratio = dclong::exp(lower - bounds.density_value);
					double p { urdist(rng) };
					if (p <= bounds.observed_density_ratio) {
						return bounds.last_draw;
					}
					update_lower_bound();
				}
			} else {
				return gen(gen_param<true, true>(), rng);
			}
		}
	}
	template<class RandomEngine> double agen(gen_param<true, false>, RandomEngine & rng) {
		if (bounds.upper_bound.segs_number() < max_segs) {
			update_upper_bound();
			while (true) {
				bounds.last_draw = bounds.upper_bound(rng);
				bounds.density_value = density.value(bounds.last_draw);
				double upper { bounds.upper_bound.value(bounds.last_draw) };
				bounds.observed_density_ratio = dclong::exp(bounds.density_value - upper);
				double p { urdist(rng) };
				if (p <= bounds.observed_density_ratio) {
					return bounds.last_draw;
				}
				update_upper_bound();
			}
		} else {
			return gen(gen_param<true, false>(), rng);
		}
	}
	template<class RandomEngine> double agen(gen_param<false, true>, RandomEngine & rng) {
		if (bounds.upper_bound.segs_number() < max_segs) {
			update_upper_bound();
			if (bounds.lower_bound.segs_number() < max_segs) {
				update_lower_bound();
				while (true) {
					bounds.last_draw = bounds.upper_bound(rng);
					bounds.density_value = density.value(bounds.last_draw);
					double upper { bounds.upper_bound.value(bounds.last_draw) };
					bounds.observed_density_ratio = bounds.density_value / upper;
					double lower { bounds.lower_bound.value(bounds.last_draw) };
					bounds.observed_squeeze_ratio = lower / bounds.density_value;
					double p { urdist(rng) };
					if (p <= bounds.observed_density_ratio) {
						return bounds.last_draw;
					}
					update_upper_bound();
					update_lower_bound();
				}
			} else {
				while (true) {
					bounds.last_draw = bounds.upper_bound(rng);
					bounds.density_value = density.value(bounds.last_draw);
					double upper { bounds.upper_bound.value(bounds.last_draw) };
					bounds.observed_density_ratio = bounds.density_value / upper;
					double p { urdist(rng) };
					if (p <= bounds.observed_density_ratio) {
						return bounds.last_draw;
					}
					update_upper_bound();
				}
			}
		} else {
			if (bounds.lower_bound.segs_number() < max_segs) {
				update_lower_bound();
				while (true) {
					bounds.last_draw = bounds.upper_bound(rng);
					bounds.density_value = density.value(bounds.last_draw);
					double upper { bounds.upper_bound.value(bounds.last_draw) };
					bounds.observed_density_ratio = bounds.density_value / upper;
					double lower { bounds.lower_bound.value(bounds.last_draw) };
					bounds.observed_squeeze_ratio = lower / bounds.density_value;
					double p { urdist(rng) };
					if (p <= bounds.observed_density_ratio) {
						return bounds.last_draw;
					}
					update_lower_bound();
				}
			} else {
				return gen(gen_param<false, true>(), rng);
			}
		}
	}
	template<class RandomEngine> double agen(gen_param<false, false>, RandomEngine & rng) {
		if (bounds.upper_bound.segs_number() < max_segs) {
			update_upper_bound();
			while (true) {
				bounds.last_draw = bounds.upper_bound(rng);
				bounds.density_value = density.value(bounds.last_draw);
				double upper { bounds.upper_bound.value(bounds.last_draw) };
				bounds.observed_density_ratio = bounds.density_value / upper;
				double p { urdist(rng) };
				if (p <= bounds.observed_density_ratio) {
					return bounds.last_draw;
				}
				update_upper_bound();
			}
		} else {
			return gen(gen_param<false, false>(), rng);
		}
	}

	void build_upper_bound(const vector<double> & upper_xs, double minx, double maxx, int capacity) {
		if (density.has_derivative()) {
			// use derivative
			int size = upper_xs.size();
			vector<pair<double, double> > ps(size);
			vector<double> as(size);
			for (int i = 0; i < size; ++i) {
				ps[i] = make_pair(upper_xs[i], density.value(upper_xs[i]));
				as[i] = density.derivative(upper_xs[i]);
			}
			bounds.upper_bound = piecewise_segment_distribution<Exp>(ps, as, minx, maxx, capacity);
		} else {
			// derivative-free, do it from left to right and then from right to left

		}
	}
	void build_lower_bound(const vector<double> & lower_xs, int capacity) {
		int size = lower_xs.size();
		vector<double> ys(size);
		for (int i = 0; i < size; ++i) {
			ys[i] = density.value(lower_xs[i]);
		}
		bounds.lower_bound = piecewise_segment_distribution<Exp>(lower_xs, ys, capacity);
	}
public:
	void min_density_ratio(double v) {
		bounds.min_density_ratio = v;
	}
	double min_density_ratio() const {
		return bounds.min_density_ratio;
	}
	void max_density_ratio(double v) {
		bounds.max_density_ratio = v;
	}
	double max_density_ratio() const {
		return bounds.max_density_ratio;
	}
	adaptive_rejection_sampling(const Density & density, const vector<double> & upper_xs, double minx, double maxx,
					int max_segs) :
					density(density), max_segs { max_segs } {
		build_upper_bound(upper_xs, minx, maxx, max_segs);
	}
	dclong::piecewise_segment_distribution<Exp> upper_bound() const {
		return bounds.upper_bound;
	}
	dclong::piecewise_segment_distribution<Exp> lower_bound() const {
			return bounds.lower_bound;
		}

	adaptive_rejection_sampling(const Density & density, const vector<double> & upper_xs, double minx, double maxx) :
					adaptive_rejection_sampling(density, upper_xs, minx, maxx, 15) {
	}

	adaptive_rejection_sampling(const Density & density, const vector<double> & upper_xs, int max_segs) :
					adaptive_rejection_sampling(density, upper_xs, upper_xs[0], upper_xs[upper_xs.size() - 1], max_segs) {
	}

	adaptive_rejection_sampling(const Density & density, const vector<double> & upper_xs) :
					adaptive_rejection_sampling(density, upper_xs, 15) {
	}

	adaptive_rejection_sampling(const Density & density, const vector<double> & upper_xs, double minx, double maxx,
					const vector<double> & lower_xs, int max_segs) :
					density(density), max_segs { max_segs } {
		build_upper_bound(upper_xs, minx, maxx, max_segs);
		build_lower_bound(lower_xs, max_segs);
	}

	adaptive_rejection_sampling(const Density & density, const vector<double> & upper_xs, double minx, double maxx,
					const vector<double> & lower_xs) :
					adaptive_rejection_sampling(density, upper_xs, minx, maxx, lower_xs, 15) {
	}

	adaptive_rejection_sampling(const Density & density, const vector<double> & upper_xs,
					const vector<double> & lower_xs, int max_segs) :
					adaptive_rejection_sampling(density, upper_xs, upper_xs[0], upper_xs[upper_xs.size() - 1], lower_xs,
									max_segs) {
	}

	adaptive_rejection_sampling(const Density & density, const vector<double> & upper_xs,
					const vector<double> & lower_xs) :
					adaptive_rejection_sampling(density, upper_xs, lower_xs, 15) {
	}

	void swap(const adaptive_rejection_sampling<Exp, Density, Squeeze> & other) {
		std::swap(bounds, other.bounds);
		std::swap(density, other.density);
		std::swap(max_segs, other.max_segs);
	}
	adaptive_rejection_sampling & operator=(const adaptive_rejection_sampling<Exp, Density, Squeeze> & other) {
		adaptive_rejection_sampling(other).swap(*this);
		return *this;
	}
	adaptive_rejection_sampling & operator=(adaptive_rejection_sampling<Exp, Density, Squeeze> && other) {
		adaptive_rejection_sampling(other).swap(*this);
		return *this;
	}
	adaptive_rejection_sampling(const adaptive_rejection_sampling<Exp, Density, Squeeze> & other) :
	bounds {other.bounds},
	density {other.density},
	max_segs {other.max_segs} {}

	adaptive_rejection_sampling(const adaptive_rejection_sampling<Exp, Density, Squeeze> && other) :
	bounds {move(other.bounds)},
	density {move(other.density)},
	max_segs {move(other.max_segs)} {}

	template<class RandomEngine> double operator()(RandomEngine & rng) {
		return gen(gen_param<Exp, Squeeze>(), rng);
	}
	template<class RandomEngine> double genrnd(RandomEngine & rng) {
		return gen(gen_param<Exp, Squeeze>(), rng);
	}
	template<class RandomEngine> double agenrnd(RandomEngine & rng) {
		return agen(gen_param<Exp, Squeeze>(), rng);
	}
	template<class InputIt, class RandomEngine> void operator()(InputIt first, InputIt last, RandomEngine & rng) {
		for(InputIt it {first}; it!=last; ++it) {
			*it = operator()(rng);
		}
	}
	template<class InputIt, class RandomEngine> void genrnd(InputIt first, InputIt last, RandomEngine & rng) {
		for(InputIt it {first}; it!=last; ++it) {
			*it = genrnd(rng);
		}
	}
	template<class InputIt, class RandomEngine> void agenrnd(InputIt first, InputIt last, RandomEngine & rng) {
		for(InputIt it {first}; it!=last; ++it) {
			*it = agenrnd(rng);
		}
	}
	adaptive_rejection_sampling_bounds<Exp, Squeeze> get_bounds() const {
		return bounds;
	}
	string to_string() const {
		return bounds.to_string() + "\nMax Number of Segments:" + std::to_string(max_segs);
	}
};

}

#endif /* ADAPTIVE_REJECTION_SAMPLING_HPP_ */
