
curve(-0.5*x^2,-10,10,10000,col=0)
apply(ub2, 2, plot.line, col="red")
apply(lb2, 2, plot.line, col="blue")
curve(-0.5*x^2,-10,10,10000,col=1,add=T)

lines(x,y,col="green")

