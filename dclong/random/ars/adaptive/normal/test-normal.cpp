/*
 * test-rejection.cpp
 *
 *  Created on: Oct 7, 2012
 *      Author: dclong
 */
#include "dclong/random/ars/adaptive_rejection_sampling.hpp"
#include "dclong/math/function/function.hpp"
#include "dclong/util/util.hpp"
#include <cmath>
#include <utility>
#include <random>
#include <iostream>
#include <fstream>
#include <limits>

using namespace std;
typedef mt19937 RandomEngine;

struct Density{
    bool constexpr has_derivative(){
        return true;
    }
    double derivative(double x){
        return -x;
    }
    double value(double x){
        return -0.5 * x * x;
    }
};
void test1(){
    typedef pair<double, double> dpair_t;
    static const double inf{numeric_limits<double>::infinity()};
    Density density;
    vector<double> xs{-2, 2};
    dclong::adaptive_rejection_sampling<true, Density, true> rs(density, xs, -inf, inf, xs);
    cout << rs.to_string() << endl;
    RandomEngine rng(1111111);
    int n = 100000;
    vector<double> sam(n);
    for(int i=0; i<n; ++i){
        sam[i] = rs.agenrnd(rng);
    }
    ofstream ofs("normal.bin", ios::binary);
    if(ofs){
        ofs.write(reinterpret_cast<char*>(&sam[0]), sizeof(double) * n);
        ofs.close();
    }
    auto bounds = rs.get_bounds();
    cout << bounds.to_string() << endl;
}

int main(){
    test1();
}
