plot.line = function(par, col, exp=FALSE){
    a = par[1]
    b = par[2]
    x1 = par[3]
    x2 = par[4]
    if(x1==-Inf){
        x1 = - 100
    }
    if(x2==Inf){
        x2 = 100
    }
    x = seq(x1, x2, length.out=10000)
    y = a * x + b
    if(exp){
        y = exp(y)
    }
    lines(x, y, col=col)
}