/*
 * adaptive_rejection_sampling.hpp
 *
 *  Created on: Oct 1, 2012
 *      Author: dclong
 */

#ifndef ADAPTIVE_REJECTION_SAMPLING_HPP_
#define ADAPTIVE_REJECTION_SAMPLING_HPP_

#include "piecewise_loglinear_distribution.hpp"

namespace dclong {
using namespace std;
template<class RandomEngine, class LogDensity, bool Adaptive = true, bool Squeeze = true> class adaptive_rejection_sampling;
template<class RandomEngine, class LogDensity> class adaptive_rejection_sampling<RandomEngine, LogDensity, false, false> {
	private:
		typedef dclong::piecewise_loglinear_distribution<RandomEngine> pwlld;
		// upper bound of the density
		pwlld upper_bound;
		// a rng
		RandomEngine & rng;
		uniform_real_distribution<double> udist;
		LogDensity log_density;
		void swap(const adaptive_rejection_sampling & other){
			std::swap(upper_bound, other.upper_bound);
			std::swap(rng, other.rng);
			std::swap(log_density, other.log_density);
		}
	public:
		adaptive_rejection_sampling(RandomEngine & rng, LogDensity log_density, pwlld upper_bound) :
		rng(rng), log_density(log_density), upper_bound(upper_bound), udist{0, 1} {
		}
		adaptive_rejection_sampling(const RandomEngine & other) : rng(rng), log_density(log_density),
				upper_bound(upper_bound) {
		}
		adaptive_rejection_sampling(RandomEngine && other) : rng(rng), log_density(log_density),
				upper_bound(move(upper_bound)) {
		}
		adaptive_rejection_sampling & operator=(const adaptive_rejection_sampling & other){
			adaptive_rejection_sampling(other).swap(*this);
			return *this;
		}
		adaptive_rejection_sampling & operator=(adaptive_rejection_sampling && other){
			adaptive_rejection_sampling(other).swap(*this);
			return *this;
		}
		double operator()(){
			while(true){
				double x{upper_bound()};
				// use 1 - udist(rng) to avoid possible log(0)
				double y{log(1 - udist(rng)) + upper_bound.log_density(x)};
				if(y <= log_density(x)){
					return x;
				}
			}
		}
		vector<double> operator()(int n){
			vector<double> x(n);
			for(int i=0; i<n; ++i){
				x[i] = operator()();
			}
			return x;
		}
};
template<class RandomEngine, class LogDensity> class adaptive_rejection_sampling<RandomEngine, LogDensity, false, true> {
	private:
		typedef dclong::piecewise_loglinear_distribution<RandomEngine> pwlld;
		// upper bound of the density
		pwlld upper_bound;
		// lower squeezing bound of the density
		pwlld lower_bound;
		// a rng
		RandomEngine & rng;
		uniform_real_distribution<double> udist;
		LogDensity log_density;
		void swap(const adaptive_rejection_sampling & other){
			std::swap(upper_bound, other.upper_bound);
			std::swap(lower_bound, other.lower_bound);
			std::swap(rng, other.rng);
			std::swap(log_density, other.log_density);
		}
	public:

		adaptive_rejection_sampling(RandomEngine & rng, LogDensity log_density, pwlld upper_bound, pwlld lower_bound) :
		rng(rng), log_density(log_density), upper_bound(upper_bound), lower_bound(lower_bound), udist{0, 1} {
		}
		adaptive_rejection_sampling(const RandomEngine & other) : rng(rng), log_density(log_density),
				upper_bound(upper_bound), lower_bound(lower_bound) {
		}
		adaptive_rejection_sampling(RandomEngine && other) : rng(rng), log_density(log_density),
				upper_bound(move(upper_bound)), lower_bound(move(lower_bound)) {
		}
		adaptive_rejection_sampling & operator=(const adaptive_rejection_sampling & other){
			adaptive_rejection_sampling(other).swap(*this);
			return *this;
		}
		adaptive_rejection_sampling & operator=(adaptive_rejection_sampling && other){
			adaptive_rejection_sampling(other).swap(*this);
			return *this;
		}
		double operator()(){
			while(true){
				double x{upper_bound()};
				// use 1 - udist(rng) to avoid possible log(0)
				double y{log(1 - udist(rng)) + upper_bound.log_density(x)};
				if(y <= lower_bound.log_density(x)){
					return x;
				}
				if(y <= log_density(x)){
					return x;
				}
			}
		}
		vector<double> operator()(int n){
			vector<double> x(n);
			for(int i=0; i<n; ++i){
				x[i] = operator()();
			}
			return x;
		}
};

template<class RandomEngine, class LogDensity> class adaptive_rejection_sampling<RandomEngine, LogDensity, true, true> {
	private:
		// upper bound of the density
		dclong::piecewise_loglinear_distribution<RandomEngine> upper_bound;
		// lower squeezing bound of the density
		dclong::piecewise_loglinear_distribution<RandomEngine> lower_bound;
		// a random number generator
		RandomEngine & rng;
		uniform_real_distribution<double> udist;
		// log density (up to a constant) of the distribution to be sampled from
		LogDensity log_density;
		// the maximum number of points for bounds
		// the bounds will no longer be updated once its number of
		// points reaches this number
		int max_point;
		// boolean value indicating whether the bounds will be updated
		bool update;
		bool update_upper_bound;
		bool update_lower_bound;
		// the maximum squeeze ratio to update bounds
		// if the actual squeeze ratio is greater than this one,
		// then the bounds are not updated
		// the bounds are updated only when the actual squeeze ratio and density ratio are both small
		double log_squeeze_ratio;
		// the maximum density ratio to update bounds
		// if the actual density ratio is greater than this one,
		//then the bounds are not updated
		double log_density_ratio;
		// last generated random number, for updating bounds
		double last_value;
		/**
		 * Update bounds.
		 */
		void update_bounds(){
			if(upper_bound.points_number()<max_point && update_upper_bound){
				// update upper bound
				if(log_density.has_derivative()){


				}
			}
			if(lower_bound.points_number()<max_point && update_lower_bound){
				// update lower bound
			}
		}
	public:
		/**
				 * A constructor.
				 * Do not use this one, unless you are absolutely sure about what you are doing.
				 * @param rng
				 * @param log_density
				 * @param upper_bound
				 * @param lower_bound
				 */
		/**
		 * Generate a random number from the specified distribution.
		 */
		double operator()(){
			if(!update){
				while(true){
					double x{upper_bound()};
					// use 1 - udist(rng) to avoid possible log(0)
					double y{log(1 - udist(rng)) + upper_bound.log_density(x)};
					if(y <= lower_bound.log_density(x)){
						return x;
					}
					if(y <= log_density(x)){
						return x;
					}
				}
			}
			update_bounds();
			while(true){
				double x{upper_bound()};
				// use 1 - udist(rng) to avoid possible log(0)
				double logp{log(1 - udist(rng))};
				double ld_lower{lower_bound.log_density(x)};
				double ld_upper{upper_bound.log_density(x)};
				double ld{log_density(x)};
				double log_sratio{ld_lower - ld};
				if(log_sratio <= log_squeeze_ratio){
					update_lower_bound = true;
				}else{
					update_lower_bound = false;
				}
				double log_dratio{ld - ld_upper};
				if(log_dratio <= log_density_ratio){
					update_upper_bound = true;
				}{
					update_upper_bound = false;
				}
				if(logp <= log_dratio){
					last_value = x;
					return x;
				}
			}
		}
		/**
		 * Generate a bound of random numbers from the specified distribution.
		 * @param n the number of draws to generate.
		 */
		vector<double> operator()(int n){
			vector<double> x(n);
			for(int i=0; i<n; ++i){
				x[i] = operator()();
			}
			return x;
		}
};

template<class RandomEngine, class LogDensity> class adaptive_rejection_sampling<RandomEngine, LogDensity, true, false> {
	private:
		// upper bound of the density
		dclong::piecewise_loglinear_distribution<RandomEngine> upper_bound;
		// a random number generator
		RandomEngine & rng;
		uniform_real_distribution<double> udist;
		// log density (up to a constant) of the distribution to be sampled from
		LogDensity log_density;
		// the maximum number of points for bounds
		// the bounds will no longer be updated once its number of
		// points reaches this number
		int max_point;
		// boolean value indicating whether the bounds will be updated
		bool update;
		bool update_upper_bound;
		// the maximum density ratio to update bounds
		// if the actual density ratio is greater than this one,
		//then the bounds are not updated
		double log_density_ratio;
		// last generated random number, for updating bounds
		double last_value;
		/**
		 * Update bounds.
		 */
		void update_bounds(){

		}
	public:
		/**
		 * Generate a random number from the specified distribution.
		 */
		double operator()(){
			if(!update){
				while(true){
					double x{upper_bound()};
					// use 1 - udist(rng) to avoid possible log(0)
					double y{log(1 - udist(rng)) + upper_bound.log_density(x)};
					if(y <= log_density(x)){
						return x;
					}
				}
			}
			update_bounds();
			while(true){
				double x{upper_bound()};
				// use 1 - udist(rng) to avoid possible log(0)
				double logp{log(1 - udist(rng))};
				double ld_upper{upper_bound.log_density(x)};
				double log_dratio{log_density(x) - ld_upper};
				if(log_dratio <= log_density_ratio){
					update_upper_bound = true;
				}{
					update_upper_bound = false;
				}
				if(logp <= log_dratio){
					last_value = x;
					return x;
				}
			}
		}
		/**
		 * Generate a bound of random numbers from the specified distribution.
		 * @param n the number of draws to generate.
		 */
		vector<double> operator()(int n){
			vector<double> x(n);
			for(int i=0; i<n; ++i){
				x[i] = operator()();
			}
			return x;
		}
};

}



#endif /* ADAPTIVE_REJECTION_SAMPLING_HPP_ */
