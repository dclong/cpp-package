
library(Rcpp)
library(inline)

code = '
int n(as<int>(r_n));
return wrap(test1(n));
'

includes = readText('test-ars.cpp')

settings=getPlugin("Rcpp")
settings$env$PKG_CXXFLAGS = paste('-std=c++0x ',settings$env$PKG_CXXFLAGS)

gen = cxxfunction(signature(r_n="integer"), code=code, includes=includes, settings=settings)

