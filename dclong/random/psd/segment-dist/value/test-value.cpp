
#include "../../segment_distribution.hpp"
#include <limits>
#include <iostream>
using namespace std;

void test1(){
	static constexpr double inf{numeric_limits<double>::infinity()};
    typedef dclong::segment_distribution<true> segt;
    segt s1{dclong::line(1, 0), make_pair(0, 1)};
    s1.update_state();
    cout << s1.to_string() << endl;
    cout << s1.value(0.5) << endl;
    cout << s1.density(0.5) << endl;
    typedef dclong::segment_distribution<false> segf;
    segf s2{dclong::line(1, 0), make_pair(0, 1)};
    s2.update_state();
    cout << s2.to_string() << endl;
    cout << s2.value(0.5) << endl;
    cout << s2.density(0.5) << endl;
}

int main(){
    test1();
}
