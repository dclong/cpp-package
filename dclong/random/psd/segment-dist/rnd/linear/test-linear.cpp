/*
 * test-rnd.cpp
 *
 *  Created on: Oct 4, 2012
 *      Author: dclong
 */

#include "/home/dclong/Dropbox/code/cpp/cackage/util/util.hpp"
#include "/home/dclong/Dropbox/code/cpp/cackage/string/string.hpp"
#include "../../../segment_distribution.hpp"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <algorithm>
using namespace std;
typedef mt19937 RandomEngine;
void test0(){
	static constexpr double inf{numeric_limits<double>::infinity()};
	RandomEngine rng(1111111);
    typedef dclong::segment_distribution<false> segt;
	segt seg1(dclong::line(1, 0), make_pair(0, 1));
    seg1.update_state();
    cout << seg1(rng) << endl;
}
    
void test1(){
	static constexpr double inf{numeric_limits<double>::infinity()};
	RandomEngine rng(1111111);
    typedef dclong::segment_distribution<false> segt;
	segt seg1(dclong::line(1, 0), make_pair(0, 1));
    seg1.update_state();
	vector<double> x(10);
    cout << " y = x on (0, 1):" << endl;
	seg1(x.begin(), x.end(), rng);
    cout << dclong::to_string(x.begin(), x.end()) << endl;

    seg1.a(2);
    seg1.update_state();
    cout << " y = 2x on (0, 1):" << endl;
	seg1(x.begin(), x.end(), rng);
    cout << dclong::to_string(x.begin(), x.end()) << endl;

    seg1.x1(-inf);
    seg1.update_state();
    cout << " y = 2x on (-inf, 1):" << endl;
	seg1(x.begin(), x.end(), rng);
    cout << dclong::to_string(x.begin(), x.end()) << endl;

    seg1.x2(inf);
    seg1.update_state();
    cout << " y = 2x on (-inf, inf):" << endl;
	seg1(x.begin(), x.end(), rng);
    cout << dclong::to_string(x.begin(), x.end()) << endl;

    seg1.x1(0);
    seg1.update_state();
    cout << " y = 2x on (0, inf):" << endl;
	seg1(x.begin(), x.end(), rng);
    cout << dclong::to_string(x.begin(), x.end()) << endl;

    seg1.a(-2);
    seg1.update_state();
    cout << " y = -2x on (0, inf):" << endl;
	seg1(x.begin(), x.end(), rng);
    cout << dclong::to_string(x.begin(), x.end()) << endl;

    seg1.x2(1);
    seg1.update_state();
    cout << " y = -2x on (0, 1):" << endl;
	seg1(x.begin(), x.end(), rng);
    cout << dclong::to_string(x.begin(), x.end()) << endl;

    seg1.x1(-inf);
    seg1.update_state();
    cout << " y = -2x on (-inf, 1):" << endl;
	seg1(x.begin(), x.end(), rng);
    cout << dclong::to_string(x.begin(), x.end()) << endl;

	seg1.a(1);
    seg1.update_state();
    cout << " y = x on (-inf, 1):" << endl;
	seg1(x.begin(), x.end(), rng);
    cout << dclong::to_string(x.begin(), x.end()) << endl;

    seg1.a(0);
    seg1.update_state();
    cout << " y = 0 on (-inf, 1):" << endl;
	seg1(x.begin(), x.end(), rng);
    cout << dclong::to_string(x.begin(), x.end()) << endl;

    seg1.x1(0);
    seg1.update_state();
    cout << " y = 0 on (0, 1):" << endl;
	seg1(x.begin(), x.end(), rng);
    cout << dclong::to_string(x.begin(), x.end()) << endl;

    seg1.x2(inf);
    seg1.update_state();
    cout << " y = 0 on (0, inf):" << endl;
	seg1(x.begin(), x.end(), rng);
    cout << dclong::to_string(x.begin(), x.end()) << endl;

    seg1.x2(1);
    seg1.update_state();
    seg1.b(-inf);
    cout << " y = x - inf on (0, 1):" << endl;
    cout << dclong::to_string(x.begin(), x.end()) << endl;
}
void test2_1(int n, double a, double b, double x1, double x2){
	static constexpr double inf{numeric_limits<double>::infinity()};
	RandomEngine rng(1111111);
    typedef dclong::segment_distribution<false> segt;
	segt seg1{dclong::line(a, b), make_pair(x1, x2)};
    seg1.update_state();
    cout << seg1.to_string() << endl;
	vector<double> r(n);
    seg1(r.begin(), r.end(), rng);
    string sample_name = "linear_" + to_string(a) + "_" + to_string(b) + "_" + to_string(x1) + "_" + to_string(x2) + ".bin";
    ofstream ofs(sample_name,ios::binary);
    if(ofs){
        ofs.write(reinterpret_cast<char*>(&r[0]), sizeof(double) * r.size());
        ofs.close();
    }
    if(x1==-inf){
        x1= -10;
    }
    if(x2==inf){
        x2= 10;
    }
    vector<double> x{dclong::ari_seq(x1, (x2-x1)/n, n)};
    vector<double> y(n);
    transform(x.begin(), x.end(), y.begin(), [&seg1](double x){
               return seg1.density(x); 
    });
    string den_name = 'd' + sample_name;
    ofstream ofs2(den_name, ios::binary);
    if(ofs2){
        ofs2.write(reinterpret_cast<char*>(&x[0]), sizeof(double) * x.size());
        ofs2.write(reinterpret_cast<char*>(&y[0]), sizeof(double) * y.size());
        ofs2.close();
    }
}

void test2(){
	static constexpr double inf{numeric_limits<double>::infinity()};
    test2_1(10000, 1, 0, 0, 1);
    test2_1(10000, 1, 0, -inf, 1);
    test2_1(10000, -3, 50, 1, 10);
    test2_1(10000, -3, 50, 1, inf);
}
 /* 0.5
 * 0.5
 * -0.693147
 *  1
 *  0
 */
int main(){
    test2();
}
