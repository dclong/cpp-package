check = function(dir, name, n){
    readBin(paste(dir,name,sep="/") ,'double', n, 8) -> sam
    hist(sam,n=40,prob=T)
    readBin(paste(dir, "/", 'd', name,sep=""),'double',n*2, 8) -> d
    d = matrix(d,ncol=2)
    lines(d[,1],d[,2],col="red")
}


