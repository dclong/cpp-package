/*
 * test-line.cpp
 *
 *  Created on: Oct 3, 2012
 *      Author: dclong
 */


#include "../../segment_distribution.hpp"
#include <iostream>
#include <iomanip>
using namespace std;
/**
 * Exp = true:
 * Line: (1.000000, 0.000000) Xrange: (0.000000, 1.000000) Yrange: (0.000000, 1.000000) Difference: (1.000000, 1.000000) <1.716667, 1.718282, 1.716667, 0.541325, 0.001000>
 * integral of y = x on (0, 1): 1.71828
 * integral of y = x on (0, 1): 0.541325
 * Line: (1.000000, 0.000000) Xrange: (-inf, 1.000000) Yrange: (-inf, 1.000000) Difference: (inf, inf) <inf, inf, inf, 1.000000, 0.001000>
 * integral of y = x on (-inf ,1): 2.71828
 * integral of y = x on (-inf ,1): 1
 * Line: (1.000000, 0.000000) Xrange: (-inf, inf) Yrange: (-inf, inf) Difference: (inf, inf) <inf, inf, inf, inf, 0.001000>
 * integral of y = x on (-inf, inf): inf
 * integral of y = x on (-inf, inf): inf
 * Line: (1.000000, 0.000000) Xrange: (0.000000, inf) Yrange: (0.000000, inf) Difference: (inf, inf) <inf, inf, inf, inf, 0.001000>
 * integral of y = x on (0, inf): inf
 * integral of y = x on (0, inf): inf
 * Line: (-2.000000, 0.000000) Xrange: (-inf, inf) Yrange: (inf, -inf) Difference: (inf, -inf) <-nan, -1.000000, -nan, inf, 0.001000>
 * integral of y = -2x on (-inf, inf): inf
 * integral of y = -2x on (-inf, inf): inf
 * Line: (-2.000000, 0.000000) Xrange: (0.000000, inf) Yrange: (0.000000, -inf) Difference: (inf, -inf) <-nan, -1.000000, -nan, -0.693147, 0.001000>
 * integral of y = -2x on (0, inf): 0.5
 * integral of y = -2x on (0, inf): -0.693147
 * Line: (-2.000000, 0.000000) Xrange: (0.000000, 1.000000) Yrange: (0.000000, -2.000000) Difference: (1.000000, -2.000000) <-0.933333, -0.864665, 0.466667, -0.838561, 0.001000>
 * integral of y = -2x on (0, 1): 0.432332
 * integral of y = -2x on (0, 1): -0.838561
 * Line: (-2.000000, 0.000000) Xrange: (-inf, 1.000000) Yrange: (inf, -2.000000) Difference: (inf, -inf) <-nan, -1.000000, -nan, inf, 0.001000>
 * integral of y = -2x on (-inf, 1): inf
 * integral of y = -2x on (-inf, 1): inf
 * Line: (0.000000, 0.000000) Xrange: (-inf, 1.000000) Yrange: (0.000000, 0.000000) Difference: (inf, 0.000000) <0.000000, 0.000000, inf, inf, 0.001000>
 * integral of y = 0 on (-inf, 1): inf
 * integral of y = 0 on (-inf, 1): inf
 * Line: (0.000000, 0.000000) Xrange: (0.000000, 1.000000) Yrange: (0.000000, 0.000000) Difference: (1.000000, 0.000000) <0.000000, 0.000000, 1.000000, 0.000000, 0.001000>
 * integral of y = 0 on (0, 1): 1
 * integral of y = 0 on (0, 1): 0
 * Line: (0.000000, 0.000000) Xrange: (0.000000, inf) Yrange: (0.000000, 0.000000) Difference: (inf, 0.000000) <0.000000, 0.000000, inf, inf, 0.001000>
 * integral of y = 0 on (0, inf): inf
 * integral of y = 0 on (0, inf): inf
 * Line: (0.000000, -inf) Xrange: (0.000000, inf) Yrange: (-inf, -inf) Difference: (inf, -nan) <-nan, -nan, -nan, nan, 0.001000>
 * integral of y = -inf on (0, inf): nan
 * integral of y = -inf on (0, inf): nan
 * Line: (0.000000, -inf) Xrange: (0.000000, 1.000000) Yrange: (-inf, -inf) Difference: (1.000000, -nan) <-nan, -nan, -nan, -nan, 0.001000>
 * integral of y = -inf on (0, 1): -nan
 * integral of y = -inf on (0, 1): -nan
 * Line: (1.000000, 0.000000) Xrange: (0.000000, 1.000000) Yrange: (0.000000, 1.000000) Difference: (1.000000, 1.000000) <1.716667, 1.718282, 1.716667, 0.541325, 0.001000>
 * integral of y = x on (0, 10): 22025.5
 * integral of y = x on (0, 10): 9.99995
 * integral of y = x on (0, 1): 1.71828
 * integral of y = x on (0, 1): 0.541325
 * Exp = false:
 * Line: (1.000000, 0.000000) Xrange: (0.000000, 1.000000) Yrange: (0.000000, 1.000000) Difference: (1.000000, 1.000000) <0.000000, 0.500000>
 * integral of y = x on (0, 1): 0.5
 * integral of y = x on (0, 1): -0.693147
 * Line: (1.000000, 0.000000) Xrange: (-inf, 1.000000) Yrange: (-inf, 1.000000) Difference: (inf, inf) <-nan, -inf>
 * integral of y = x on (-inf ,1): -inf
 * integral of y = x on (-inf ,1): nan
 * Line: (1.000000, 0.000000) Xrange: (-inf, inf) Yrange: (-inf, inf) Difference: (inf, inf) <-nan, -nan>
 * integral of y = x on (-inf, inf): -nan
 * integral of y = x on (-inf, inf): -nan
 * Line: (1.000000, 0.000000) Xrange: (0.000000, inf) Yrange: (0.000000, inf) Difference: (inf, inf) <-nan, inf>
 * integral of y = x on (0, inf): inf
 * integral of y = x on (0, inf): inf
 * Line: (-2.000000, 0.000000) Xrange: (-inf, inf) Yrange: (inf, -inf) Difference: (inf, -inf) <-nan, -nan>
 * integral of y = -2x on (-inf, inf): -nan
 * integral of y = -2x on (-inf, inf): -nan
 * Line: (-2.000000, 0.000000) Xrange: (0.000000, inf) Yrange: (0.000000, -inf) Difference: (inf, -inf) <-nan, -inf>
 * integral of y = -2x on (0, inf): -inf
 * integral of y = -2x on (0, inf): nan
 * Line: (-2.000000, 0.000000) Xrange: (0.000000, 1.000000) Yrange: (0.000000, -2.000000) Difference: (1.000000, -2.000000) <2.000000, -1.000000>
 * integral of y = -2x on (0, 1): -1
 * integral of y = -2x on (0, 1): nan
 * Line: (-2.000000, 0.000000) Xrange: (-inf, 1.000000) Yrange: (inf, -2.000000) Difference: (inf, -inf) <-nan, inf>
 * integral of y = -2x on (-inf, 1): inf
 * integral of y = -2x on (-inf, 1): inf
* Line: (0.000000, 0.000000) Xrange: (-inf, 1.000000) Yrange: (0.000000, 0.000000) Difference: (inf, 0.000000) <-nan, 0.000000>
* integral of y = 0 on (-inf, 1): 0
* integral of y = 0 on (-inf, 1): -inf
* Line: (0.000000, 0.000000) Xrange: (0.000000, 1.000000) Yrange: (0.000000, 0.000000) Difference: (1.000000, 0.000000) <-nan, 0.000000>
* integral of y = 0 on (0, 1): 0
* integral of y = 0 on (0, 1): -inf
* Line: (0.000000, 0.000000) Xrange: (0.000000, inf) Yrange: (0.000000, 0.000000) Difference: (inf, 0.000000) <-nan, 0.000000>
* integral of y = 0 on (0, inf): 0
* integral of y = 0 on (0, inf): -inf
* Line: (0.000000, -inf) Xrange: (0.000000, inf) Yrange: (-inf, -inf) Difference: (inf, -nan) <nan, -inf>
* integral of y = -inf on (0, inf): -inf
* integral of y = -inf on (0, inf): nan
* Line: (0.000000, -inf) Xrange: (0.000000, 1.000000) Yrange: (-inf, -inf) Difference: (1.000000, -nan) <nan, -inf>
* integral of y = -inf on (0, 1): -inf
* integral of y = -inf on (0, 1): nan
* Line: (1.000000, 0.000000) Xrange: (0.000000, 1.000000) Yrange: (0.000000, 1.000000) Difference: (1.000000, 1.000000) <0.000000, 0.500000>
* integral of y = x on (0, 10): 50
* integral of y = x on (0, 10): 9.99995
* integral of y = x on (0, 1): 0.5
* integral of y = x on (0, 1): -0.693147
*/
    /** for some reason, this function cannot be tempalted!!!!???
     */
void test1(){
	static constexpr double inf{numeric_limits<double>::infinity()};
    //dclong::segment_distribution<true> seg1{dclong::line(1, 0), make_pair(0, 1)};
    //dclong::segment_distribution<true> seg1{make_pair(-1,-1), make_pair(1, 1)};
    dclong::segment_distribution<true> seg1;
    //seg1.update_state();
    cout << seg1.to_string() << endl;
	cout << "integral of y = x on (0, 1): " << (seg1.integral()) << endl;
	//cout << "integral of y = x on (0, 1): " << seg1.integral<true>() << endl;
    /**
    seg1.x1(-inf);
    seg1.update_state();
    cout << seg1.to_string() << endl;
	cout << "integral of y = x on (-inf ,1): " << seg1.integral<false>() << endl;
	cout << "integral of y = x on (-inf ,1): " << seg1.integral<true>() << endl;
    seg1.x2(inf);
    seg1.update_state();
    cout << seg1.to_string() << endl;
	cout << "integral of y = x on (-inf, inf): " << seg1.integral<false>() << endl;
	cout << "integral of y = x on (-inf, inf): " << seg1.integral<true>() << endl;
    seg1.x1(0);
    seg1.update_state();
    cout << seg1.to_string() << endl;
	cout << "integral of y = x on (0, inf): " << seg1.integral<false>() << endl;
	cout << "integral of y = x on (0, inf): " << seg1.integral<true>() << endl;
    seg1.a(-2);
    seg1.x1(-inf);
    seg1.update_state();
    cout << seg1.to_string() << endl;
	cout << "integral of y = -2x on (-inf, inf): " << seg1.integral<false>() << endl;
	cout << "integral of y = -2x on (-inf, inf): " << seg1.integral<true>() << endl;
    seg1.x1(0);
    seg1.update_state();
    cout << seg1.to_string() << endl;
	cout << "integral of y = -2x on (0, inf): " << seg1.integral<false>() << endl;
	cout << "integral of y = -2x on (0, inf): " << seg1.integral<true>() << endl;
	seg1.x2(1);
    seg1.update_state();
    cout << seg1.to_string() << endl;
	cout << "integral of y = -2x on (0, 1): " << seg1.integral<false>() << endl;
	cout << "integral of y = -2x on (0, 1): " << seg1.integral<true>() << endl;
    seg1.x1(-inf);
    seg1.update_state();
    cout << seg1.to_string() << endl;
	cout << "integral of y = -2x on (-inf, 1): " << seg1.integral<false>() << endl;
	cout << "integral of y = -2x on (-inf, 1): " << seg1.integral<true>() << endl;
    seg1.a(0);
    seg1.update_state();
    cout << seg1.to_string() << endl;
	cout << "integral of y = 0 on (-inf, 1): " << seg1.integral<false>() << endl;
	cout << "integral of y = 0 on (-inf, 1): " << seg1.integral<true>() << endl;
    seg1.x1(0);
    seg1.update_state();
    cout << seg1.to_string() << endl;
	cout << "integral of y = 0 on (0, 1): " << seg1.integral<false>() << endl;
	cout << "integral of y = 0 on (0, 1): " << seg1.integral<true>() << endl;
    seg1.x2(inf);
    seg1.update_state();
    cout << seg1.to_string() << endl;
	cout << "integral of y = 0 on (0, inf): " << seg1.integral<false>() << endl;
	cout << "integral of y = 0 on (0, inf): " << seg1.integral<true>() << endl;
    seg1.b(-inf);
    seg1.update_state();
    cout << seg1.to_string() << endl;
	cout << "integral of y = -inf on (0, inf): " << seg1.integral<false>() << endl;
	cout << "integral of y = -inf on (0, inf): " << seg1.integral<true>() << endl;
    seg1.x2(1);
    seg1.update_state();
    cout << seg1.to_string() << endl;
	cout << "integral of y = -inf on (0, 1): " << seg1.integral<false>() << endl;
	cout << "integral of y = -inf on (0, 1): " << seg1.integral<true>() << endl;
    seg1.reset(dclong::line(1, 0), make_pair(0, 1));
    seg1.update_state();
    cout << seg1.to_string() << endl;
	cout << "integral of y = x on (0, 10): " << seg1.integral<false>(make_pair(0, 10)) << endl;
	cout << "integral of y = x on (0, 10): " << seg1.integral<true>(make_pair(0, 10)) << endl;
	cout << "integral of y = x on (0, 1): " << seg1.integral<false>() << endl;
	cout << "integral of y = x on (0, 1): " << seg1.integral<true>() << endl;
    **/
}
int main(){
    cout << "Exp = true:" << endl;
    test1();
    cout << "Exp = false:" << endl;
    //test1<false>();
}
