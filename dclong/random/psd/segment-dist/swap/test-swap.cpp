
#include "../../segment_distribution.hpp"
#include <limits>
#include <iostream>
using namespace std;
/**
 * True:
 * s1 before swap:Line: (1.000000, 0.000000) Xrange: (0.000000, 1.000000) Yrange: (0.000000, 1.000000) Difference: (1.000000, 1.000000) <1.716667, 1.718282, 1.716667, 0.541325, 0.001000>
 * s2 after swap: Line: (0.000000, 1.000000) Xrange: (-inf, inf) Yrange: (1.000000, 1.000000) Difference: (inf, 0.000000) <0.000000, 0.000000, inf, inf, 0.001000>
 * s1 after swap: Line: (0.000000, 1.000000) Xrange: (-inf, inf) Yrange: (1.000000, 1.000000) Difference: (inf, 0.000000) <0.000000, 0.000000, inf, inf, 0.001000>
 * s2 after swap: Line: (1.000000, 0.000000) Xrange: (0.000000, 1.000000) Yrange: (0.000000, 1.000000) Difference: (1.000000, 1.000000) <1.716667, 1.718282, 1.716667, 0.541325, 0.001000>
 * 
 * False:
 * s1 before swap:Line: (1.000000, 0.000000) Xrange: (0.000000, 1.000000) Yrange: (0.000000, 1.000000) Difference: (1.000000, 1.000000) <0.000000, 0.500000>
 * s2 after swap: Line: (0.000000, 1.000000) Xrange: (-inf, inf) Yrange: (1.000000, 1.000000) Difference: (inf, 0.000000) <-nan, inf>
 * s1 after swap: Line: (0.000000, 1.000000) Xrange: (-inf, inf) Yrange: (1.000000, 1.000000) Difference: (inf, 0.000000) <-nan, inf>
 * s2 after swap: Line: (1.000000, 0.000000) Xrange: (0.000000, 1.000000) Yrange: (0.000000, 1.000000) Difference: (1.000000, 1.000000) <0.000000, 0.500000>
 *
 */
template<bool Exp> void test1(){
	static constexpr double inf{numeric_limits<double>::infinity()};
    typedef dclong::segment_distribution<Exp> segt;
    segt s1{dclong::line(1, 0), make_pair(0, 1)};
    segt s2{dclong::line(0,1), make_pair(-inf, inf)};
    s1.update_state();
    s2.update_state();
    cout << "s1 before swap:" << s1.to_string() << endl << "s2 before swap: " << s2.to_string() << endl;
    std::swap(s1, s2);
    cout << "s1 after swap: " << s1.to_string() << endl << "s2 after swap: " << s2.to_string() << endl;
}
/**
 * Line: (1.000000, 0.000000) Xrange: (0.000000, 1.000000) Yrange: (0.000000, 1.000000) Difference: (1.000000, 1.000000) <1.716667, 1.718282, 1.716667, 0.541325, 0.001000>
 * Line: (1.000000, 0.000000) Xrange: (0.000000, 1.000000) Yrange: (0.000000, 1.000000) Difference: (1.000000, 1.000000) <1.716667, 1.718282, 1.716667, 0.541325, 0.001000>
 * Line: (1.000000, 0.000000) Xrange: (0.000000, 1.000000) Yrange: (0.000000, 1.000000) Difference: (1.000000, 1.000000) <1.716667, 1.718282, 1.716667, 0.541325, 0.001000>
 * Line: (1.000000, 0.000000) Xrange: (0.000000, 1.000000) Yrange: (0.000000, 1.000000) Difference: (1.000000, 1.000000) <1.716667, 1.718282, 1.716667, 0.541325, 0.001000>
 * Line: (1.000000, 0.000000) Xrange: (0.000000, 1.000000) Yrange: (0.000000, 1.000000) Difference: (1.000000, 1.000000) <0.000000, 0.500000>
 * Line: (1.000000, 0.000000) Xrange: (0.000000, 1.000000) Yrange: (0.000000, 1.000000) Difference: (1.000000, 1.000000) <0.000000, 0.500000>
 * Line: (1.000000, 0.000000) Xrange: (0.000000, 1.000000) Yrange: (0.000000, 1.000000) Difference: (1.000000, 1.000000) <0.000000, 0.500000>
 * Line: (1.000000, 0.000000) Xrange: (0.000000, 1.000000) Yrange: (0.000000, 1.000000) Difference: (1.000000, 1.000000) <0.000000, 0.500000>
 *
 */
template<bool Exp> void test2(){
	static constexpr double inf{numeric_limits<double>::infinity()};
    typedef dclong::segment_distribution<Exp> segt;
    segt s1{dclong::line(1, 0), make_pair(0, 1)};
    s1.update_state();
    cout << s1.to_string() << endl;
    segt s2{s1};
    s2.update_state();
    cout << s1.to_string() << endl;
    cout << s1.to_string() << endl;
    segt s3{move(s1)};
    s3.update_state();
    cout << s1.to_string() << endl;
}

int main(){
    test1<true>();
    test1<false>();
    test2<true>();
    test2<false>();
}
