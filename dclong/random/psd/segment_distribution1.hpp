/*
 * segment-distribution.hpp
 *
 *  Created on: Oct 5, 2012
 *      Author: dclong
 */

#ifndef SEGMENT_DISTRIBUTION_HPP_
#define SEGMENT_DISTRIBUTION_HPP_


#include "segment.hpp"
#include <random>
#include <limits>
#include <utility>
#include <iostream>

namespace dclong{
using namespace std;
template<bool Exp=true> class segment_distribution : public dclong::segment {};
template<> class segment_distribution<true> : public dclong::segment {
	private:
		struct {
			double edy_m1_appr;
			double edy_m1_exact;
			double edy_m1_overa_appr;
			double log_exp_integral;
		} temp;
	// u(0, 1) distribution, never change
		uniform_real_distribution<double> urdist{0, 1};
		// might be changed by user, but not suggested
		double epsilon{1e-3};
		/**
		 * Logarithm (for numerical concerns) of the integral of the exponential
		 * of this line function in the specified domain.
		 */
		double integrate(){
            static constexpr double inf{std::numeric_limits<double>::infinity()};
            static constexpr double sig_nan{numeric_limits<double>::signaling_NaN()};
			if(difference.first>0){
				if(xrange.first>-inf){
					if(xrange.second<inf){
						if(std::abs(difference.second) < epsilon){
							if(slope!=0){
								return std::log(temp.edy_m1_overa_appr) + yrange.first;
							}
							// slope == 0
							return std::log(difference.first) + yrange.first;
						}
						return std::log(temp.edy_m1_exact/slope) + yrange.first;
					}
					// x2 == inf
					if(slope>0){
						return inf;
					}
					if(slope<0){
						return yrange.first - std::log(-slope);
					}
					// slope==0
					if(intercept>-inf){
						return inf;
					}
					// slope==0 && intercept==0, not well defined
					return sig_nan;
				}
				// x1 == -inf
				if(slope>0){
					return yrange.second - std::log(slope);
				}
				if(slope<0){
					return inf;
				}
				// slope == 0
				if(intercept>-inf){
					return inf;
				}
				// slope==0 and intercept==-inf not well defined
				return sig_nan;
			}
            // xdiff == 0
			return -inf;
		}
	public:
		void swap(segment_distribution<true> & other){
            dclong::segment::swap(other);
			std::swap(epsilon, other.epsilon);
			std::swap(temp, other.temp);
		}
		segment_distribution(const segment_distribution<true> & other) :
		dclong::segment{other},
		epsilon{other.epsilon},
		temp{other.temp} {}

        //not sure whether it's ok to move other to line ...
        // i guess it should be ok, but just in case ...
        // you can check this ... using big data in base class ...
		segment_distribution(segment_distribution<true> && other) :
		dclong::segment{move(other)},
		epsilon{move(other.epsilon)},
		temp{other.temp} {}

		segment_distribution<true> & operator=(const segment_distribution<true> & other){
			segment_distribution<true>(other).swap(*this);
			return *this;
		}
		segment_distribution<true> & operator=(segment_distribution<true> && other){
			segment_distribution<true>(other).swap(*this);
			return *this;
		}
//		segment_distribution(double a, double b):
//			segment_distribution<true>(a, b, move(make_pair(-numeric_limits<double>::infinity(), numeric_limits<double>::infinity()))) {}
//
//		segment_distribution(double a, double b, const pair<double, double> & range) : line(a, b), range{range} {
//		}

//		segment_distribution(double a, double b, pair<double, double> && range) : line(a, b), range{move(range)} {
//		}
        segment_distribution() :
        	segment_distribution<true>(dclong::segment()) {}

		segment_distribution(const point_t & p1, const point_t & p2) :
			dclong::segment(p1, p2) {}

		segment_distribution(const dclong::line & l, const range_t & r) :
			dclong::segment(l, r) {}

		segment_distribution(const dclong::line & l) :
			dclong::segment(l) {}

		segment_distribution(dclong::line && l, const range_t & r) :
			dclong::segment{move(l), r} {}

		segment_distribution(const dclong::line & l, range_t && r) :
			dclong::segment(l, move(r)) {}

		segment_distribution(dclong::line && l, range_t && range) :
			dclong::segment(move(l), range) {}

		segment_distribution(const dclong::line && l) :
			dclong::segment(move(l)) {}

		void error(double epsilon){
			this -> epsilon = epsilon;
            updated = true;
		}
		double error() const{
			return epsilon;
		}

        /**
         * Must be called before you generate random numbers or query for integral.
         * No need to run again if the segment_distribution<true> is not changed.
         * If you swap two segment_distribution<true>s, and they have run update_state before swapping,
         * no need to run this function after swap.
         */
		void update_state(){
			if(updated){
				double ydiff2 = difference.second * difference.second;
				double ydiff3 = ydiff2 * difference.second;
				double ydiff4 = ydiff3 * difference.second;
				double edy_m1_overdy = 1 + 0.5 * difference.second + ydiff2/6 + ydiff3/24 + ydiff4/120;
				temp.edy_m1_appr = edy_m1_overdy * difference.second;
				temp.edy_m1_overa_appr = edy_m1_overdy * dx();
				temp.edy_m1_exact = std::exp(difference.second) - 1;
				temp.log_exp_integral = integrate();
				updated = false;
			}
		}

        template<bool Log=true> double integral() const;

        template<bool Log=true> double integral(const pair<double, double> & r) const {
        	segment_distribution<true> s{segment_distribution<true>(*this)};
            s.domain(r);
            s.update_state();
            return s.integral<Log>();
        }

        template<bool Log=true> double integral(const pair<double, double> && r) const{
			segment_distribution<true> s{segment_distribution<true>(*this)};
			s.domain(move(r));
            s.update_state();
			return s.integral<Log>();
		}

		template<class RandomEngine, class InputIt> segment_distribution<true> & operator()(InputIt first, InputIt last, RandomEngine & rng) {
			for(InputIt it{first}; it!=last; ++it){
				*it = operator()(rng);
			}
		}

		template<class RandomEngine> double operator()(RandomEngine & rng){
            static constexpr double inf{std::numeric_limits<double>::infinity()};
            static constexpr double sig_nan{numeric_limits<double>::signaling_NaN()};
			if(xrange.first>-inf){
				if(xrange.second<inf){
					if(std::abs(difference.second) < epsilon){
						double p{urdist(rng)};
						double u{p * temp.edy_m1_appr};
						double u2{u * u};
						double u3{u2 * u};
						double u4{u3 * u};
						double log1pu_overu_appr = (1 - 0.5 * u + u2 / 3 - 0.25 * u3 + 0.2 * u4);
						return p * temp.edy_m1_overa_appr * log1pu_overu_appr + xrange.first;
					}
					double p{urdist(rng)};
					return std::log(p * temp.edy_m1_exact + 1) / slope + xrange.first;
				}
				// x2 == inf x1>-inf
				if(slope<0){
					double p{1-urdist(rng)};
					return std::log(p) / slope + xrange.first;
				}
				return sig_nan;
			}
			// x1==-inf
			if(xrange.second<inf){
				//x1==-inf x2<inf
				if(slope>0){
					double p{1-urdist(rng)};
					return std::log(p) /slope + xrange.second;
				}
				return sig_nan;
			}
			// x1==-inf x2==inf
			return sig_nan;
		}
		
        template<bool Log=false> double density(double x) const;

        virtual ostream & print() const override {
            dclong::segment::print();
            cout << "\n<"
            	<< "ydiff: " << difference.second
            	<< ", edy_m1_appr: "<< temp.edy_m1_appr
                << ", edy_m1_exact: " << temp.edy_m1_exact
                << ", edy_m1_overa_appr: " << temp.edy_m1_overa_appr
                << ", log_exp_integral: " << temp.log_exp_integral
                << ">";
            return cout;
        }
};

template<> double segment_distribution<true>::integral<true>() const{
			return temp.log_exp_integral;
}
template<> double segment_distribution<true>::integral<false>() const{
	return std::exp(temp.log_exp_integral);
}
template<> double segment_distribution<true>::density<true>(double x) const {
    return value(x) - temp.log_exp_integral;
}
template<> double segment_distribution<true>::density<false>(double x) const {
    return std::exp(density<true>(x));
}

template<> class segment_distribution<false> : public dclong::segment {
	private:
		struct {
			double w0;
			double line_integral;
		} temp;
        // a uniform distribution
		uniform_real_distribution<double> urdist;
        // calculate integral
		double integrate(){
            if(slope!=0||intercept!=0){
                return 0.5 * yminsum.second * difference.first;
            }
            return 0;
		}

	public:
		void swap(segment_distribution<false> & other){
            dclong::segment::swap(other);
			std::swap(urdist, other.urdist);
			std::swap(temp, other.temp);
		}
		segment_distribution(const segment_distribution<false> & other) :
			dclong::segment{other},
			temp{other.temp} {}

		segment_distribution(segment_distribution<false> && other) :
			dclong::segment{move(other)},
            temp{other.temp} {}

		segment_distribution & operator=(const segment_distribution<false> & other){
			segment_distribution<false>(other).swap(*this);
			return *this;
		}
		segment_distribution & operator=(segment_distribution<false> && other){
			segment_distribution<false>(other).swap(*this);
			return *this;
		}
        segment_distribution() : segment_distribution<false>(dclong::segment()) {}

		segment_distribution(const dclong::line & line):
		segment_distribution(line, make_pair(-numeric_limits<double>::infinity(), numeric_limits<double>::infinity())) {}
//		segment_distribution(double a, double b, double x1, double x2) :
//			line(a, b), urdist{x1, x2}, range{make_pair(x1, x2)} {
//		}

		segment_distribution(const dclong::line & l, const pair<double, double> & r) :
			dclong::segment(l, r) {}

		segment_distribution(const dclong::line & l, pair<double, double> && r) :
			dclong::segment(l, move(r)) {}

		segment_distribution(const pair<double, double> & p1, const pair<double, double> & p2) :
			dclong::segment(p1, p2) {}

		segment_distribution(dclong::line && l, const pair<double, double> & r) :
			dclong::segment(move(l), r) {}

		segment_distribution(dclong::line && l) : dclong::segment(move(l)) {}

		segment_distribution(dclong::line && l, pair<double, double> && r) :
			dclong::segment(move(l), move(r)) {}



        /**
         * Must be called before you generate random numbers or query for integral.
         * No need to run again if the segment_distribution is not changed.
         * If you swap two segment_distributions, and they have run update_state before swapping,
         * no need to run this function after swap.
         */
		void update_state(){
			if(updated){
				dclong::segment::update_state();
				urdist.param(uniform_real_distribution<double>::param_type(xrange.first, xrange.second));
				temp.w0 = yminsum.first * difference.first;
				double w1{0.5 * difference.first * std::abs(difference.second)};
				temp.w0 /= temp.w0 + w1;
				// transform w0 to a cut in range
				temp.w0 = temp.w0 * difference.first + xrange.first;
				temp.line_integral = integrate();
				updated = false;
			}
		}
        template<bool Log=false> double integral(const pair<double, double> & range) const{
            segment_distribution<false> s{segment_distribution<false>(*this)};
            s.domain(range);
            s.update_state();
            return s.integral<Log>();
        }
        template<bool Log=false> double integral(pair<double, double> && range) const {
			segment_distribution<false> s{segment_distribution<false>(*this)};
			s.domain(move(range));
            s.update_state();
			return s.integral<Log>();
		}

        double integral() const;

        double log_integral() const;

		template<class RandomEngine, class InputIt> segment_distribution<false> & operator()(InputIt first, InputIt last, RandomEngine & rng){
			for(InputIt it(first); it!=last; ++it){
				*it = operator()(rng);
			}
		}
		template<class RandomEngine> double operator()(RandomEngine & rng){
			if(urdist(rng)<=temp.w0){
				// in rectangle
				return urdist(rng);
			}
			// in triangle
			if(difference.second>0){
				return std::max(urdist(rng), urdist(rng));
			}
			if(difference.second<0){
				return std::min(urdist(rng), urdist(rng));
			}
			// yleft == yright , this won't happen, but just in case
			return urdist(rng);
		}

		double density(double x) const;
		double log_density(double x) const;

        virtual ostream & print() const override{
        	dclong::segment::print();
            cout << "\n<"

            	<< ", w0: " << temp.w0
            	<< ", line_integral: " << temp.line_integral
            	<< ">";
        }
};

template<> double segment_distribution<false>::integral() const{
	return temp.line_integral;
}
template<> double segment_distribution<false>::log_integral() const{
	return std::log(temp.line_integral);
}
template<> double segment_distribution<false>::density(double x) const{
	return value(x)/temp.line_integral;
}
template<> double segment_distribution<false>::log_density(double x) const{
	return std::log(value(x)/temp.line_integral);
}
}

#endif /* SEGMENT_DISTRIBUTION_HPP_ */
