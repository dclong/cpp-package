
#include "../../piecewise_segment_distribution.hpp"
#include <vector>
using namespace std;
typedef dclong::piecewise_segment_distribution<true> pst;

void test1(){
   vector<double> x{-1, 0, 1};
   vector<double> y{0, 1, 0};
   pst p1(x, y);
   p1.update_state();
   //vector<pair<double, double>> y2{make_pair(0, 2), make_pair(1, 0)};
   //pst p2(x, y2);
   //p2.update_state();
}

int main(){
    test1();
}
