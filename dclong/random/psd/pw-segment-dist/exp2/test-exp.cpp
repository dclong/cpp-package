
#include "../../piecewise_segment_distribution.hpp"
#include <vector>
using namespace std;
typedef dclong::piecewise_segment_distribution<true> pst;

void test1(){
    vector<double> x{-1, 0, 1};
    vector<pair<double, double>> y{make_pair(0, 2), make_pair(1, 0)};
    pst p(x, y);
}
int main(){
    test1();
    return 0;
}
