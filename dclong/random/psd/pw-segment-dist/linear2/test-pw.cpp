
#include "../../piecewise_segment_distribution.hpp"
#include <vector>
using namespace std;
typedef dclong::piecewise_segment_distribution<true> pst;

void test1(){
   vector<double> x{-1, 0, 1};
   vector<double> y{0, 3, 0};
   pst p(x, y);
}

int main(){
    test1();
}
