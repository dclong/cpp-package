
//#include "/home/dclong/Dropbox/code/cpp/cackage/util/util.hpp"
//#include "/home/dclong/Dropbox/code/cpp/cackage/string/string.hpp"
#include "../../piecewise_segment_distribution.hpp"
#include <iostream>
#include <vector>
//#include <fstream>
//#include <cmath>
using namespace std;
typedef dclong::piecewise_segment_distribution<true> pst;
typedef mt19937 RandomEngine;
/**
void test1(int n, string file){
    vector<double> xs{-1, 0, 1};
    vector<double> ys{0, 3, 0};
    pst p(xs, ys);
    static RandomEngine rng(1111111);
    p.update_state();
    vector<double> sam(n);
    p(sam.begin(), sam.end(), rng);
    ofstream ofs(file, ios::binary);
    if(ofs){
       ofs.write(reinterpret_cast<char*>(&sam[0]), sizeof(double) * n);
       ofs.close();
    }
    double x1{p.x1()};
    if(std::isinf(x1)){
       x1 = -10;
    }
    double x2{p.x2()};
    if(std::isinf(x2)){
       x2 = 10;
    }
    vector<double> x(dclong::ari_seq(x1, (x2 - x1)/n, n));
    vector<double> d(n);
    for(int i=0; i<n; ++i){
        d[i] = p.density(x[i]);
    }
    ofs.open("d" + file);
    if(ofs){
        ofs.write(reinterpret_cast<char*>(&x[0]), sizeof(double) * n);
        ofs.write(reinterpret_cast<char*>(&d[0]), sizeof(double) * n);
        ofs.close();
    }
}
void test2(){
    test1(10000, "exp1.bin");
}
void test3(){
    vector<double> x{-1, 0, 1};
    vector<double> y{0, 3, 0};
    pst p(x, y);
    p.insert_line(dclong::line(0,1));
    test1(10000, "exp2.bin");
}
void test4(){
    vector<double> x{-1, 0, 1};
    vector<double> y{0, 3, 0};
    pst p(x, y);
    p.insert_line(dclong::line(0,1));
    cout << p.to_string() << endl << endl;
    p.update_state();
    cout << p.to_string() << endl << endl;
    p.insert_point(make_pair(0, 2));
    cout << p.to_string() << endl << endl;
    p.update_state();
    cout << p.to_string() << endl << endl;
    test1(100000, "exp3.bin");
}
**/
struct Density {
    bool constexpr has_derivative() const{
        return true;
    }
    double value(double x) const{
        return -0.5 * x * x;
    }
    double derivative(double x) const{
        return -x;
    }
};
void test5(){
    static double inf = numeric_limits<double>::infinity();
    vector<double> x{-2, 2};
    Density density;
    RandomEngine rng(11111);
    pst p{density, x, -inf, inf,2};
    cout << dclong::genrnd(density, p, rng) << endl;
    //p1.update_state();
    //cout << p1.to_string() << endl;
}
int main(){
    test5();
    return 0;
}
