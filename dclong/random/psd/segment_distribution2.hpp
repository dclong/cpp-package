
#ifndef SEGMENT_DISTRIBUTION_HPP_
#define SEGMENT_DISTRIBUTION_HPP_

#include "../line/line.hpp"
#include "segment.hpp"
#include "/home/dclong/Dropbox/code/cpp/cackage/math/function/function.hpp"
#include "/home/dclong/Dropbox/code/cpp/cackage/string/string.hpp"
#include <random>
#include <limits>
#include <utility>
#include <iostream>
#include <string>
#include <cmath>

namespace dclong{
using namespace std;

namespace helper{
template<bool Exp=true> struct segment_distribution_temporary {};
template<> struct segment_distribution_temporary<true> {
	double edy_m1_appr;
	double edy_m1_exact;
	double edy_m1_overa_appr;
	// log exp integral
	double integral;
	double epsilon{1e-3};
	string to_string() const {
		return "<" + std::to_string(edy_m1_appr) + ", " + std::to_string(edy_m1_exact) + ", "
				+ std::to_string(edy_m1_overa_appr) + ", " + std::to_string(integral)
		+ ", " + std::to_string(epsilon) + ">";
	}
};

template<> struct segment_distribution_temporary<false> {
	double w0;
	// linear line integral
	double integral;
	string to_string() const {
		return "<" + std::to_string(w0) + ", " + std::to_string(integral) + ">";
	}
};
}

template<bool Exp> class segment_distribution : public dclong::segment {
	private:
		helper::segment_distribution_temporary<Exp> stemp;
		uniform_real_distribution<double> urdist{0, 1};
		double integrate();
	public:
		void swap(segment_distribution<Exp> & other){
			dclong::segment::swap(other);
			std::swap(urdist, other.urdist);
			std::swap(stemp, other.stemp);
		}
		segment_distribution(const segment_distribution<Exp> & other) :
				dclong::segment{other},
				stemp(other.stemp),
				urdist{other.urdist} {}

		segment_distribution(segment_distribution<Exp> && other) :
			dclong::segment{move(other)},
			stemp(move(other.stemp)),
			urdist{move(other.urdist)} {}

		segment_distribution & operator=(const segment_distribution<Exp> & other){
			segment_distribution<Exp>(other).swap(*this);
			return *this;
		}

		segment_distribution & operator=(segment_distribution<Exp> && other){
			segment_distribution<Exp>(other).swap(*this);
			return *this;
		}

        segment_distribution() :
        	segment_distribution<Exp>(dclong::segment()) {}

		segment_distribution(const point_t & p1, const point_t & p2) :
			dclong::segment(p1, p2) {}

		segment_distribution(const dclong::line & l, const range_t & r) :
			dclong::segment(l, r) {}

		segment_distribution(const dclong::line & l) :
			dclong::segment(l) {}

		segment_distribution(dclong::line && l, const range_t & r) :
			dclong::segment{move(l), r} {}

		segment_distribution(const dclong::line & l, range_t && r) :
			dclong::segment(l, move(r)) {}

		segment_distribution(dclong::line && l, range_t && range) :
			dclong::segment(move(l), move(range)) {}

		segment_distribution(const dclong::line && l) :
			dclong::segment(move(l)) {}

//		template<bool LogIntegral> double integral(const range_t & r) const {
//			segment_distribution s{segment_distribution<Exp>(*this)};
//			s.domain(r);
//			s.update_state();
//			return s.integral<LogIntegral>();
//		}
//
//		template<bool LogIntegral> double integral(const range_t && r) const {
//			segment_distribution s{segment_distribution<Exp>(*this)};
//			s.domain(move(r));
//			s.update_state();
//			return s.integral<LogIntegral>();
//		}

        template<class RandomEngine> double operator()(RandomEngine & rng);

		template<class RandomEngine, class InputIt> segment_distribution<Exp> & operator()(InputIt first, InputIt last, RandomEngine & rng) {
			for(InputIt it{first}; it!=last; ++it){
				*it = operator()(rng);
			}
		}

		template<bool LogValue> double value(double x) const override {
			return dclong::logexp<Exp, Exp == LogValue>(dclong::segment::value(x));
		}

		template<bool LogDensity> double density(double x) const {
		    return dclong::logexp<Exp, Exp == LogDensity>(dclong::ratiodiff<Exp>(value<Exp>(x), stemp.integral));
		}

		template<bool LogIntegral> double integral() const {
			return dclong::logexp<Exp, Exp == LogIntegral>(stemp.integral);
		}

        double error() const;

        void error(double);

        virtual void update_state() override;

		string to_string() const override {
			return dclong::segment::to_string() + " " + stemp.to_string();
		}
};
template<> double segment_distribution<true>::integrate(){
	static constexpr double inf{std::numeric_limits<double>::infinity()};
	static constexpr double sig_nan{numeric_limits<double>::signaling_NaN()};
		if(xrange.first>-inf){
			if(xrange.second<inf){
				if(std::abs(difference.second) < temp.epsilon){
					if(slope!=0){
						return dclong::log<false>(temp.edy_m1_overa_appr) + yrange.first;
					}
					// slope == 0
					return dclong::log<false>(difference.first) + yrange.first;
				}
				return dclong::log<false>(temp.edy_m1_exact/slope) + yrange.first;
			}
			// x2 == inf
			if(slope>0){
				return inf;
			}
			if(slope<0){
				return yrange.first - dclong::log<false>(-slope);
			}
			// slope==0
			if(intercept>-inf){
				return inf;
			}
			// slope==0 && intercept==0, not well defined
			return sig_nan;
		}
		// x1 == -inf
		if(slope>0){
			return yrange.second - dclong::log<false>(slope);
		}
		if(slope<0){
			return inf;
		}
		// slope == 0
		if(intercept>-inf){
			return inf;
		}
		// slope==0 and intercept==-inf not well defined
		return sig_nan;
}

template<> double segment_distribution<false>::integrate(){
	if(slope!=0||intercept!=0){
		return 0.5 * yminsum.second * difference.first;
	}
	return 0;
}

template<> template<class RandomEngine> double segment_distribution<true>::operator()(RandomEngine & rng){
	static constexpr double inf{std::numeric_limits<double>::infinity()};
	static constexpr double sig_nan{numeric_limits<double>::signaling_NaN()};
	if(xrange.first>-inf){
		if(xrange.second<inf){
			if(std::abs(difference.second) < temp.epsilon){
				double p{urdist(rng)};
				double u{p * temp.edy_m1_appr};
				double u2{u * u};
				double u3{u2 * u};
				double u4{u3 * u};
				double log1pu_overu_appr = (1 - 0.5 * u + u2 / 3 - 0.25 * u3 + 0.2 * u4);
				return p * temp.edy_m1_overa_appr * log1pu_overu_appr + xrange.first;
			}
			double p{urdist(rng)};
			return dclong::log<false>(p * temp.edy_m1_exact + 1) / slope + xrange.first;
		}
		// x2 == inf x1>-inf
		if(slope<0){
			double p{1-urdist(rng)};
			return dclong::log<false>(p) / slope + xrange.first;
		}
		return sig_nan;
	}
	// x1==-inf
	if(xrange.second<inf){
		//x1==-inf x2<inf
		if(slope>0){
			double p{1-urdist(rng)};
			return dclong::log<false>(p) /slope + xrange.second;
		}
		return sig_nan;
	}
	// x1==-inf x2==inf
	return sig_nan;
}

template<> template<class RandomEngine> double segment_distribution<false>::operator()(RandomEngine & rng){
	if(urdist(rng)<=temp.w0){
		// in rectangle
		return urdist(rng);
	}
	// in triangle
	if(difference.second>0){
		return std::max(urdist(rng), urdist(rng));
	}
	if(difference.second<0){
		return std::min(urdist(rng), urdist(rng));
	}
	// yleft == yright , this won't happen, but just in case
	return urdist(rng);
}

template<> void segment_distribution<true>::error(double epsilon){
	temp.epsilon = epsilon;
	updated = true;
}

template<> double segment_distribution<true>::error() const{
	return temp.epsilon;
}

template<> void segment_distribution<true>::update_state(){
	if(updated){
		dclong::segment::update_state();
		double ydiff2 = difference.second * difference.second;
		double ydiff3 = ydiff2 * difference.second;
		double ydiff4 = ydiff3 * difference.second;
		double edy_m1_overdy = 1 + 0.5 * difference.second + ydiff2/6 + ydiff3/24 + ydiff4/120;
		temp.edy_m1_appr = edy_m1_overdy * difference.second;
		temp.edy_m1_overa_appr = edy_m1_overdy * dx();
		temp.edy_m1_exact = dclong::exp<false>(difference.second) - 1;
		temp.integral = integrate();
		updated = false;
	}
}

template<> void segment_distribution<false>::update_state(){
	if(updated){
		dclong::segment::update_state();
		urdist.param(uniform_real_distribution<double>::param_type(xrange.first, xrange.second));
		temp.w0 = yminsum.first * difference.first;
		double w1{0.5 * difference.first * std::abs(difference.second)};
		temp.w0 /= temp.w0 + w1;
		// transform w0 to a cut in range
		temp.w0 = temp.w0 * difference.first + xrange.first;
		temp.integral = integrate();
		updated = false;
	}
}

}

#endif /* SEGMENT_DISTRIBUTION2_HPP_ */
