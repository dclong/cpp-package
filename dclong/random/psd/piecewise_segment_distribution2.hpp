
#ifndef PIECEWISE_SEGMENT_DISTRIBUTION_HPP_
#define PIECEWISE_SEGMENT_DISTRIBUTION_HPP_

#include "/home/dclong/Dropbox/code/cpp/cackage/string/string.hpp"
#include "/home/dclong/Dropbox/code/cpp/cackage/math/function/function.hpp"
#include "segment_distribution.hpp"
#include <vector>
#include <limits>
#include <algorithm>
#include <utility>
#include <tuple>
#include <iostream>
#include <cmath>
namespace dclong{
using namespace std;

template<bool Exp=true> class piecewise_segment_distribution {
	private:
		struct piecewise_sgement_distribution_temporary{
			discrete_distribution<int> ddist;
			double ws_ave;
			double integral;
			vector<double> scaled_ws;
			piecewise_sgement_distribution_temporary(int n) :
				scaled_ws(n) {}
		} temp;
        typedef pair<double, double> range_t;
        typedef pair<double, double> point_t;
        typedef pair<double, double> dpair_t;
        typedef pair<int, int> ipair_t;
		vector<dclong::segment_distribution<Exp>> segments;
		/**
		 * This allows more convenient constructors if the density function is continuous.
		 */
		void segment_domain(double minx, double maxx){
			const int last = segments.size() - 1;
			double xleft{minx};
			for(int i=0; i<last; ++i){
				double xright{segments[i].intersection(segments[i+1])};
				segments[i].domain(make_pair(xleft, xright));
				xleft = xright;
			}
			segments[last].domain(make_pair(xleft, maxx));
		}
		void resize(const int size){
			segments.resize(size);
			temp.scaled_ws.resize(size);
		}
		/**
		 * Update segments.
		 * This is the case when seg is complete below segs[i].
		 * @param seg a segment whose domain is the complete domain.
		 */
		int update_segs_bb(const dclong::segment_distribution<Exp> & seg, int i, const vector<ipair_t> & comparison){
			// seg is complete under segs[i]
			// find the consecutive smallest one that is also true
			int i_lower;
			for(i_lower=i-1; i_lower>=0; --i_lower){
				if(comparison[i_lower].first<0 || comparison[i_lower].second<0){
					++ i_lower;
					break;
				}
			}
			if(i_lower > 0){
				int is_below_right = comparison[i_lower-1].second;
				if(is_below_right>0){
					// seg is below the right part of segs[i_lower-1]
					segments.erase(segments.begin() + i_lower + 1, segments.begin() + i);
					segments[i_lower] = seg;
					double x{seg.intersection(segments[i_lower - 1])};
					segments[i_lower].x1(x);
					if(i_lower + 1<segments.size()){
						segments[i_lower].x2(segments[i+1].x1());
					}
					segments[i_lower-1].x2(x);
					// segs[i_lower-1] need not to be updated again
					return i_lower - 1;
				}
				if(is_below_right<0){
					if(comparison[i_lower-1].first>0){
						// seg is over the right part of segs[i_lower - 1]
						// do thing, let segs[i_lower - 1] address itself
						return i_lower;
					}
					// seg is complete over segs[i_lower - 1], it need not to be updated
					return i_lower - 1;
				}
				// is_below_right==0
				//seg is complete over segs[i_lower - 1], it need not to be updated
				return i_lower - 1;
			}
			// i_lower == 0
			segments.erase(segments.begin() + 1, segments.begin() + i);
			segments[0] = seg;
			if(1 < segments.size()){
				segments[0].x2(segments[1].x1());
			}
			return i_lower;
		}
		int update_segs_ob(const dclong::segment_distribution<Exp> & seg, int i, const vector<ipair_t> & comparison){
			// seg is below the right part of segs[i] but over the left part of it
			// cut segs[i] into two parts
			segments.insert(segments.begin() + i + 1, seg);
			double x{segments[i+1].intersection(segments[i])};
			segments[i+1].x1(x);
			if(i + 2 < segments.size()){
				segments[i+1].x2(segments[i+2].x1());
			}
			segments[i].x2(x);
			segments[i].update_state();
			return i;
		}
		int update_segs_bo(const dclong::segment_distribution<Exp> & seg, const int i, const vector<ipair_t> & comparison){
			// seg is below the left part of segs[i] but over the right part of it
			// what to do depend on the behavior segs[i-1] if exists
			if(i>0){
				int iminus1{i - 1};
				int is_below_right{comparison[iminus1].second};
				if(is_below_right>0){
					// seg is below the right part segs[i-1]
					if(comparison[i-1].first>=0){
						// seg is also below the left part of segs[i-1]
						// only need to update the right end point of seg
						// because it will get address by segs[i-1] itself
						double x{seg.intersection(segments[i])};
						segments[i].x1(x);
						return i;
					}
					// seg is over the left part of segs[i-1]
					// need to insert an intersection, update xs[i] to be another intersection
					segments.insert(segments.begin() + i, seg);
					double x{segments[i].intersection(segments[i+1])};
					segments[i+1].x1(x);
					segments[i].x2(x);
					x = segments[i].intersection(segments[i-1]);
					segments[i].x1(x);
					segments[i-1].x2(x);
					// segs[i-1] need not to be updated again
					return i - 1;
				}
				if(is_below_right<1){
					// seg is over the right part of segs[i-1] or intersect with segs[i-1] at xs[i]
					// cut seg[i] into two parts
					segments.insert(segments.begin() + i, seg);
					double x{segments[i].intersection(segments[i+1])};
					segments[i+1].x1(x);
					segments[i].x2(x);
					return i;
				}
				// seg intersect with segs[i-1] at xs[i]
				if(comparison[i-1].first>=0){
					// seg is also below the left part of segs[i-1]
					// only need to update the right end point of seg
					// because it will get address by segs[i-1] itself
					double x{seg.intersection(segments[i])};
					segments[i].x1(x);
					return i;
				}
				// seg is complete over segs[i-1], no need to updage segs[i-1]
				return i - 1;
			}
			// i == 0, cut seg[0] into two parts
			segments.insert(segments.begin(), seg);
			double x{segments[0].intersection(segments[1])};
			segments[1].x1(x);
			segments[0].x2(x);// no need to update left point
			return i;
		}
		/**
		 * Update the upper bound segs.
		 * This method updates all segs function and ranges if necessary.
		 * And it is preferred over add_seg unless you have good reasons to add_seg.
		 * It assumes that (xs[0], xs[xs.size()-1]) is already the largest possible range.
		 * @param seg
		 */
		void update_segs(const dclong::segment_distribution<Exp> & seg){
			// the idea is to use lower segs to replace upper segs
			const vector<pair<int, int>> comparison{move(seg.less(segments))};
			const int size = comparison.size();
			for(int i{size-1}; i>=0; --i){
				int is_below_right{comparison[i].second};
				if(is_below_right>0){
					int is_below_left{comparison[i].first};
					// seg is below the right part of segs[i]
					if(is_below_left>=0){
						// seg is completely below segs[i]
						i = update_segs_bb(seg, i, comparison);
						continue;
					}
					// seg is under the right part of segs[i] but over the left part of it
					i = update_segs_ob(seg, i, comparison);
					continue;
				}
				if(is_below_right<0){
					// seg is over the right part of segs[i]
					if(comparison[i].first>0){
						// seg is below the left part of segs[i] but over the right part of it
						i = update_segs_bo(seg, i, comparison);
						continue;
					}
					// seg is completely over segs[i]
					// do nothing
					continue;
				}
				// seg intersect with segs[i] at xs[i+1]
				if(comparison[i].first>=0){
					// seg is below segs[i] completely (might overlap)
					i = update_segs_bb(seg, i, comparison);
					continue;
				}
				// seg is over (might overlap) segs[i] completely
				// do nothing
				continue;
			}
			// resize  scaled_ws
			temp.scaled_ws.resize(segments.size());
		}
		void insert_point(const point_t & p, const int i){
			int size = segments.size();
			if(i>=0 && i<size){
				const pair<double, double> point1{segments[i].p1()};
				const pair<double, double>  point2{segments[i].p2()};
				segments[i] = dclong::segment_distribution<Exp>(p, point2);
				segments.insert(segments.begin()+i, dclong::segment_distribution<Exp>(point1, p));
				temp.scaled_ws.resize(segments.size());
				return;
			}
			if(i==size){
				segments.push_back(dclong::segment_distribution<Exp>(segments[segments.size()-1].p2(), p));
				temp.scaled_ws.resize(segments.size());
				return;
			}
			segments.insert(segments.begin(), dclong::segment_distribution<Exp>(p, segments[0].p1()));
			temp.scaled_ws.resize(segments.size());
		}
		void scale_weights();
		int which_seg(double x) const {
			const int i = upper_bound(segments.begin(), segments.end(), x, [](double v1, const segment_distribution<Exp> & seg){
				return v1 < seg.x2();
			}) - segments.begin();
			if(i>0){
				return i;
			}
			// i==0
			if(x>=segments[0].x1()){
				return 0;
			}
			return -1;
		}
	public:
		void insert_point(const point_t & point){
			insert_point(point, which_seg(point.first));
		}
	public:
		template<class RandomEngine> double operator()(RandomEngine & rng){
			return segments[temp.ddist(rng)](rng);
		}

		template<class InputIt, class RandomEngine> void operator()(InputIt first, InputIt last, RandomEngine & rng){
            for(InputIt it{first}; it!=last; ++it){
                *it = operator()(rng);
            }
		}

		void swap(piecewise_segment_distribution<Exp> & other){
			std::swap(segments, other.segments);
			std::swap(temp, other.temp);
		}

		piecewise_segment_distribution(const piecewise_segment_distribution<Exp> & other) :
            segments{other.segments},
            temp{other.temp} {}

		piecewise_segment_distribution(piecewise_segment_distribution<Exp> && other) :
            segments{move(other.segments)},
            temp{move(other.temp)} {}

		piecewise_segment_distribution<Exp> & operator=(const piecewise_segment_distribution<Exp> & other){
			piecewise_segment_distribution<Exp>(other).swap(*this);
			return *this;
		}

		piecewise_segment_distribution<Exp> & operator=(piecewise_segment_distribution<Exp> && other){
			piecewise_segment_distribution<Exp>(other).swap(*this);
			return *this;
		}
		/**
		 * A constructor that takes the x and y coordiantes as arguments.
		 * @param xs a vector of x coordinates.
		 * @param ys a vector of y coordinates.
		 */
		piecewise_segment_distribution(const vector<double> & xs,
				const vector<double> & ys, size_t capacity=20) :
		segments(std::max(xs.size() -1, capacity)), temp(segments.size()) {
			const int size = xs.size() - 1;
			resize(size);
			for(int i=0; i<size; ++i){
				segments[i] = dclong::segment_distribution<Exp>(make_pair(xs[i], ys[i]), make_pair(xs[i+1], ys[i+1]));
			}
		}
		/**
		 * A constructor that takes a bound of points as the argument.
		 * @param points a vector of points,
		 * where a point is a pair of x and y coordinates.
		 */
		piecewise_segment_distribution(const vector<pair<double, double>> & points, int capacity=20) :
		segments(points.size() - 1), temp(segments.size()) {
			const int size = points.size() - 1;
			resize(size);
			for(int i=0; i<size; ++i){
				segments[i] = dclong::segment_distribution<Exp>(points[i], points[i+1]);
			}
		}
		/**
		 * A constructor that takes x coordinates and y coordinates as arguments.
		 * @param xs a vector of x coordinates.
		 * @param ys a vector of pairs of y coordinates.
		 * The pair of y coordinates is the y value of the left and right end point
		 * of the corresponding seg defined on the intervals specified by the x coordinates.
		 * The size of ys is 1 less than the size of xs.
		 *
		 * This allows non continuous density functions.
		 */
		piecewise_segment_distribution(const vector<double> & xs, const vector<dpair_t> & ys, size_t capacity=20) :
        segments(std::max(ys.size(), capacity)), 
        temp(segments.size()) {
			// calculate seg functions
			const int size = ys.size() - 1;
			resize(size);
			for(int i=0; i<size; ++i){
				segments[i] = dclong::segment_distribution<Exp>(make_pair(xs[i], ys[i].first),
						make_pair(xs[i+1], ys[i].second));
			}
		}
		/**
		 * A constructor that takes a seg functions
		 * and the domain of density as arguments.
		 * @param segs a vector of segs,
		 * where a seg is a pair of slope (a) and intercept (b).
		 * @param minx the minimum value of x.
		 * @param maxx the maximum value of x.
		 */
		piecewise_segment_distribution(const vector<dclong::line> & lines, double minx, double maxx, size_t capacity=20) :
        segments(std::max(capacity, lines.size())), 
        temp(segments.size()) {
			const int size = lines.size();
			resize(size);
			for(int i=0; i<size; ++i){
				segments[i] = dclong::segment_distribution<Exp>(lines[i]);
			}
			segment_domain(minx, maxx);
		}
		/**
		 * A constructor that takes slopes, intercepts and the
		 * domain of density as arguments.
		 * @param as a vector of slope of segs.
		 * @param bs a vector of intercept of segs.
		 * @param minx the minimum value of x.
		 * @param maxx the maximum value of x.
		 */
		piecewise_segment_distribution(const vector<double> & as, const vector<double> & bs, 
                double minx, double maxx, size_t capacity=20) :
		segments(std::max(capacity, as.size())), 
        temp(segments.size()) {
			const int size = as.size();
			resize(size);
			for(int i=0; i<size; ++i){
				segments[i] = dclong::segment_distribution<Exp>(dclong::line(as[i], bs[i]));
			}
			segment_domain(minx, maxx);
		}
		/**
		 * A constructor that takes a bound of points and the
		 * domain of density as arguments.
		 * @param segs a vector of segs.
		 * Each seg is represented by (x, y, a),
		 * where (x,y) is a point on the seg and a is the slope of the seg.
		 * @param minx
		 * @param maxx
		 */
		piecewise_segment_distribution(const vector<dpair_t> & points, const vector<double> & as,
                double minx, double maxx, size_t capacity=20) :
		segments(std::max(capacity, as.size())), 
        temp(segments.size()) {
			const int size = as.size();
			resize(size);
			for(int i=0; i<size; ++i){
				segments[i] = dclong::segment_distribution<Exp>(dclong::line(points[i], as[i]));
			}
			segment_domain(minx, maxx);
		}
		/**
		 * Set seg i.
		 * This is needed when the domain is infinite,
		 * i.e., x[0] is -inf or x[x.size() -1] is inf.
		 * @param i the index of the seg to be set.
		 * You must call the function update_weights after you finished
		 * setting segs in order to get weights updated.
		 */
		void set_seg(dclong::line line, int i){
			segments[i].a(line.a());
			segments[i].b(line.b());
		}

		int segs_number() const {
			return segments.size();
		}
		double x1() const {
			if(segments.size()>0){
				return segments[0].x1();
			}
			return numeric_limits<double>::signaling_NaN();

		}
		double x2() const {
			int size = segments.size();
			if(size>0){
				return segments[size-1].x2();
			}
			return numeric_limits<double>::signaling_NaN();

		}
		range_t domain() const{
			return make_pair(x1(), x2());
		}

		void insert_line(const dclong::line line){
			update_segs(dclong::segment_distribution<Exp>(line, domain()));
		}

		template<bool LogDensity> double density(double x) const {
			return dclong::logexp<Exp, Exp == LogDensity>(
					dclong::ratiodiff<Exp>(value<Exp>(x), integral<Exp>()));
		}
		template<bool LogIntegral> double integral() const {
			return dclong::logexp<Exp, Exp==LogIntegral>(temp.integral);
		}
		template<bool LogValue> double value(double x) const {
		    int i = which_seg(x);
		    if(i>=0 && i<segments.size()){
		        return segments[i].value<LogValue>(x);
		    }
		    return dclong::log<!LogValue>(0.0);
		}
		/**
		 * Must be called before you do useful things.
		 * No need to be called again if no change.
		 */
		void update_state(){
			int size = segments.size();
			for(int i=0; i<size; ++i){
				segments[i].update_state();
			}
			scale_weights();
		}
		string to_string() const {
			string s{""};
			int size = segments.size();
			for(int i=0; i<size; ++i){
				s += "Seg " + std::to_string(i) + ": " + segments[i].to_string() + "\n";
			}
			s += "[" + dclong::to_string(temp.scaled_ws.begin(), temp.scaled_ws.end()) + "]";
			return s;
		}
};

template<> void piecewise_segment_distribution<true>::scale_weights(){
	// scale and recover the weights
	temp.ws_ave = accumulate(segs.begin(), segs.end(), 0.0,
			[](double init, dclong::segment_distribution<true> seg){
		return init + seg.integral<true>();
	}) / segs.size();
	transform(segments.begin(), segments.end(), temp.scaled_ws.begin(),
			[ws_ave=this->temp.ws_ave](const dclong::segment_distribution<true> & seg){
		return dclong::exp<false>(seg.integral<true>() - ws_ave);
	});
	temp.ddist.param(discrete_distribution<int>::param_type(temp.scaled_ws.begin(), temp.scaled_ws.end()));
	temp.integral = temp.ws_ave + dclong::log<false>(accumulate(temp.scaled_ws.begin(), temp.scaled_ws.end(), 0.0));
}

template<> void piecewise_segment_distribution<false>::scale_weights(){
	transform(segs.begin(), segs.end(), temp.scaled_ws.begin(),
			[](const dclong::segment_distribution<false> & seg){
		return seg.integral<false>();
	});
	temp.ddist.param(discrete_distribution<int>::param_type(temp.scaled_ws.begin(), temp.scaled_ws.end()));
	temp.integral = accumulate(temp.scaled_ws.begin(), temp.scaled_ws.end(), 0.0);
}

}
#endif /* PIECEWISE_SEGMENT_DISTRIBUTION_HPP_ */
