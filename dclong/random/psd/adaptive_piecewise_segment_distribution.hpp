/*
 * adaptive_piecewise_segment_distribution.hpp
 *
 *  Created on: Oct 16, 2012
 *      Author: dclong
 */

#ifndef ADAPTIVE_PIECEWISE_SEGMENT_DISTRIBUTION_HPP_
#define ADAPTIVE_PIECEWISE_SEGMENT_DISTRIBUTION_HPP_

#include "dclong/random/psd/piecewise_segment_distribution.hpp"
#include <limits>

namespace dclong { // I think this is what you should do ...
using namespace std;
template<bool Exp> class adaptive_piecewise_segment_distribution: public dclong::piecewise_segment_distribution<Exp> {
private:
	static constexpr int default_capacity { 15 };
	static constexpr double default_min_ratio { 0.8 };
	struct {
		double density_value;
		double observed_ratio;
		bool update;
	} atemp;
	double min_ratio;
	template<class Density> void update_bound(const Density & density) {
		if (atemp.observed_ratio<min_ratio) {
//			cout << "updating upper bound: " << endl;
//			cout << "observed ratio: " << temp.observed_ratio << ", min ratio: " << min_ratio << endl;
//			cout << "last draw: " << temp.last_draw << ", density value: " << temp.density_value << endl;
			if (density.has_derivative()) {
				// use derivative information to update upper bound
				double a { density.derivative(this->ptemp.last_draw) };
				if (!std::isinf(a) && !std::isnan(a)) {
					update_segs(
									dclong::segment_distribution<Exp>(this->ptemp.last_draw, atemp.density_value, a,
													this->domain()));
				}
			} else {
				// derivative-free update

			}
		}
	}

//	void update_lower_bound() {
//		if (atemp.observed_ratio < min_ratio) {
////			cout << "updating lower bound" << endl;
////					cout << "observed ratio: " << temp.observed_ratio << ", min ratio: " << min_ratio << endl;
////					cout << "last draw: " << temp.last_draw << ", density value: " << temp.density_value << endl;
//			insert_point(make_pair(atemp.last_draw, atemp.density_value));
//		}
//	}

	virtual void segs_extra() override {
		atemp.observed_ratio = numeric_limits<double>::infinity();
	}
	/**
	 * Update segments.
	 * This is the case when seg is complete below segs[i].
	 * @param seg a segment whose domain is the complete domain.
	 */
	int update_segs_bb(const dclong::segment_distribution<Exp> & seg, int i,
					const vector<pair<signed char, signed char> > & comparison) {
		// seg is complete under segs[i]
		// find the consecutive smallest one that is also true
		int i_lower;
		for (i_lower = i - 1; i_lower >= 0; --i_lower) {
			if (comparison[i_lower].first < 0 || comparison[i_lower].second < 0) {
				break;
			}
		}
		++i_lower;
		//		cout << "i: " << i << "i_lower: " << i_lower << endl;
		if (i_lower > 0) {
			int is_below_right = comparison[i_lower - 1].second;
			if (is_below_right > 0) {
				// seg is below the right part of segs[i_lower-1]
				this->segments.erase(this->segments.begin() + i_lower + 1, this->segments.begin() + i + 1);
				this->segments[i_lower] = seg;
				double x { seg.intersection(this->segments[i_lower - 1]) };
				this->segments[i_lower].x1(x);
				if (i_lower + 1 < this->segments.size()) {
					this->segments[i_lower].x2(this->segments[i + 1].x1());
				}
				this->segments[i_lower - 1].x2(x);
				// segs[i_lower-1] need not to be updated again
				return i_lower - 1;
			}
			if (is_below_right < 0) {
				if (comparison[i_lower - 1].first > 0) {
					// seg is over the right part of segs[i_lower - 1]
					// do thing, let segs[i_lower - 1] address itself
					return i_lower;
				}
				// seg is complete over segs[i_lower - 1], it need not to be updated
				return i_lower - 1;
			}
			// is_below_right==0
			//seg is complete over segs[i_lower - 1], it need not to be updated
			return i_lower - 1;
		}
		// i_lower == 0
		this->segments.erase(this->segments.begin() + 1, this->segments.begin() + i + 1);
		this->segments[0] = seg;
		if (1 < this->segments.size()) {
			this->segments[0].x2(this->segments[1].x1());
		}
		return i_lower;
	}

	int update_segs_ob(const dclong::segment_distribution<Exp> & seg, int i,
					const vector<pair<signed char, signed char> > & comparison) {
		// seg is below the right part of segs[i] but over the left part of it
		// cut segs[i] into two parts
		this->segments.insert(this->segments.begin() + i + 1, seg);
		double x { this->segments[i + 1].intersection(this->segments[i]) };
		this->segments[i + 1].x1(x);
		if (i + 2 < this->segments.size()) {
			this->segments[i + 1].x2(this->segments[i + 2].x1());
		}
		this->segments[i].x2(x);
		return i;
	}

	int update_segs_bo(const dclong::segment_distribution<Exp> & seg, const int i,
					const vector<pair<signed char, signed char> > & comparison) {
		// seg is below the left part of segs[i] but over the right part of it
		// what to do depend on the behavior segs[i-1] if exists
		if (i > 0) {
			int iminus1 { i - 1 };
			int is_below_right { comparison[iminus1].second };
			if (is_below_right > 0) {
				// seg is below the right part segs[i-1]
				if (comparison[i - 1].first >= 0) {
					// seg is also below the left part of segs[i-1]
					// only need to update the right end point of seg
					// because it will get address by segs[i-1] itself
					double x { seg.intersection(this->segments[i]) };
					this->segments[i].x1(x);
					return i;
				}
				// seg is over the left part of segs[i-1]
				// need to insert an intersection, update xs[i] to be another intersection
				this->segments.insert(this->segments.begin() + i, seg);
				double x { this->segments[i].intersection(this->segments[i + 1]) };
				this->segments[i + 1].x1(x);
				this->segments[i].x2(x);
				x = this->segments[i].intersection(this->segments[i - 1]);
				this->segments[i].x1(x);
				this->segments[i - 1].x2(x);
				// segs[i-1] need not to be updated again
				return i - 1;
			}
			if (is_below_right < 1) {
				// seg is over the right part of segs[i-1] or intersect with segs[i-1] at xs[i]
				// cut seg[i] into two parts
				this->segments.insert(this->segments.begin() + i, seg);
				double x { this->segments[i].intersection(this->segments[i + 1]) };
				this->segments[i + 1].x1(x);
				this->segments[i].x2(x);
				return i;
			}
			// seg intersect with segs[i-1] at xs[i]
			if (comparison[i - 1].first >= 0) {
				// seg is also below the left part of segs[i-1]
				// only need to update the right end point of seg
				// because it will get address by segs[i-1] itself
				double x { seg.intersection(this->segments[i]) };
				this->segments[i].x1(x);
				return i;
			}
			// seg is complete over segs[i-1], no need to updage segs[i-1]
			return i - 1;
		}
		// i == 0, cut seg[0] into two parts
		this->segments.insert(this->segments.begin(), seg);
		double x { this->segments[0].intersection(this->segments[1]) };
		this->segments[1].x1(x);
		this->segments[0].x2(x);					// no need to update left point
		return i;
	}

	void insert_point(const pair<double, double> & p, const int i) {
		int size = this->segments.size();
		if (i >= 0 && i < size) {
			const pair<double, double> point1 { this->segments[i].p1() };
			const pair<double, double> point2 { this->segments[i].p2() };
			this->segments[i] = dclong::segment_distribution<Exp>(p, point2);
			this->segments.insert(this->segments.begin() + i, dclong::segment_distribution<Exp>(point1, p));
			dclong::piecewise_segment_distribution<Exp>::temp.scaled_ws.resize(this->segments.size());
			return;
		}
		if (i == size) {
			this->segments.push_back(dclong::segment_distribution<Exp>(this->segments.back().p2(), p));
			dclong::piecewise_segment_distribution<Exp>::temp.scaled_ws.resize(this->segments.size());
			return;
		}
		this->segments.insert(this->segments.begin(),
						dclong::segment_distribution<Exp>(p, this->segments.front().p1()));
		dclong::piecewise_segment_distribution<Exp>::temp.scaled_ws.resize(this->segments.size());
	}
public:
	/**
	 * Update the upper bound segs.
	 * This method updates all segs function and ranges if necessary.
	 * And it is preferred over add_seg unless you have good reasons to add_seg.
	 * It assumes that (xs[0], xs[xs.size()-1]) is already the largest possible range.
	 * @param seg
	 */
	void update_segs(const dclong::segment_distribution<Exp> & seg) {
		// the idea is to use lower segs to replace upper segs
		const vector<pair<signed char, signed char>> comparison { move(seg.less(this->segments)) };
		const int size = comparison.size();
		for (int i { size - 1 }; i >= 0; --i) {
			int is_below_right { comparison[i].second };
			if (is_below_right > 0) {
				int is_below_left { comparison[i].first };
				// seg is below the right part of segs[i]
				if (is_below_left >= 0) {
					// seg is completely below segs[i]
					i = update_segs_bb(seg, i, comparison);
					continue;
				}
				// seg is under the right part of segs[i] but over the left part of it
				i = update_segs_ob(seg, i, comparison);
				continue;
			}
			if (is_below_right < 0) {
				// seg is over the right part of segs[i]
				if (comparison[i].first > 0) {
					// seg is below the left part of segs[i] but over the right part of it
					i = update_segs_bo(seg, i, comparison);
					continue;
				}
				// seg is completely over segs[i]
				// do nothing
				continue;
			}
			// seg intersect with segs[i] at xs[i+1]
			if (comparison[i].first >= 0) {
				// seg is below segs[i] completely (might overlap)
				i = update_segs_bb(seg, i, comparison);
				continue;
			}
			// seg is over (might overlap) segs[i] completely
			// do nothing
			continue;
		}
		// resize  scaled_ws
		this->ptemp.scaled_ws.resize(this->segments.size());
		this->update_state();
	}
	void insert_point(const pair<double, double> & point) {
		insert_point(point, this->which_seg(point.first));
	}

	virtual ~adaptive_piecewise_segment_distribution() {
	}
	void swap(adaptive_piecewise_segment_distribution<Exp> & other) {
		dclong::piecewise_segment_distribution<Exp>::swap(other);
		std::swap(atemp, other.atemp);
		std::swap(min_ratio, other.min_ratio);
	}

	adaptive_piecewise_segment_distribution(const adaptive_piecewise_segment_distribution<Exp> & other) :
					dclong::piecewise_segment_distribution<Exp> { other }, atemp { other.atemp }, min_ratio {
									other.min_ratio } {
	}
	adaptive_piecewise_segment_distribution(const adaptive_piecewise_segment_distribution<Exp> && other) :
	dclong::piecewise_segment_distribution<Exp> {move(other)},
	atemp {move(other.atemp)},
	min_ratio {move(other.min_ratio)}
	{}

	adaptive_piecewise_segment_distribution(double min_ratio, int capacity) :
	dclong::piecewise_segment_distribution<Exp>(capacity),
	min_ratio {min_ratio}
	{}

	adaptive_piecewise_segment_distribution(unsigned int capacity) :
	adaptive_piecewise_segment_distribution<Exp>(default_min_ratio, capacity)
	{}

	adaptive_piecewise_segment_distribution() :
	adaptive_piecewise_segment_distribution<Exp>(default_min_ratio, default_capacity)
	{}

	template<class Density> adaptive_piecewise_segment_distribution(const Density & density,
					const vector<double> & xs, double minx, double maxx, double min_ratio, int capacity) :
	dclong::piecewise_segment_distribution<Exp>(density, xs, minx, maxx, capacity),
	min_ratio {min_ratio} {
	}

	template<class Density> adaptive_piecewise_segment_distribution(const Density & density,
					const vector<double> & xs, double min_ratio, int capacity, bool upper_bound) :
	dclong::piecewise_segment_distribution<Exp>(density, xs, capacity, upper_bound),
	min_ratio {min_ratio} {
	}

	template<class Density> adaptive_piecewise_segment_distribution(const Density & density,
					const vector<double> & xs, double min_ratio, bool upper_bound) :
	dclong::adaptive_piecewise_segment_distribution<Exp>(density, xs, min_ratio, default_capacity, upper_bound) {
	}

	template<class Density> adaptive_piecewise_segment_distribution(const Density & density,
					const vector<double> & xs, int capacity, bool upper_bound) :
	dclong::adaptive_piecewise_segment_distribution<Exp>(density, xs, default_min_ratio, capacity, upper_bound) {
	}

	template<class Density> adaptive_piecewise_segment_distribution(const Density & density,
					const vector<double> & xs, bool upper_bound) :
	dclong::adaptive_piecewise_segment_distribution<Exp>(density, xs, default_min_ratio, default_capacity, upper_bound) {
	}

	template<class Density> adaptive_piecewise_segment_distribution(const Density & density,
					const vector<double> & xs, double minx, double maxx, double min_ratio) :
	dclong::adaptive_piecewise_segment_distribution<Exp>(density, xs, minx, maxx, min_ratio, default_capacity) {
	}
	template<class Density> adaptive_piecewise_segment_distribution(const Density & density,
					const vector<double> & xs, double minx, double maxx, int capacity) :
	dclong::adaptive_piecewise_segment_distribution<Exp>(density, xs, minx, maxx, default_min_ratio, capacity) {
	}
	template<class Density> adaptive_piecewise_segment_distribution(const Density & density,
					const vector<double> & xs, double minx, double maxx) :
	dclong::adaptive_piecewise_segment_distribution<Exp>(density, xs, minx, maxx, default_min_ratio, default_capacity) {
	}

	template<class Density, class RandomEngine> friend double agenrnd(const Density & density,
					adaptive_piecewise_segment_distribution<true> & upper_bound, RandomEngine & rng);

//	template<class Density, class RandomEngine> friend double agenrnd(const Density & density,
//					adaptive_piecewise_segment_distribution<true> & upper_bound,
//					adaptive_piecewise_segment_distribution<true> & lower_bound, RandomEngine & rng);

	template<class Density, class RandomEngine> friend double agenrnd(const Density & density,
					adaptive_piecewise_segment_distribution<false> & upper_bound, RandomEngine & rng);

//	template<class Density, class RandomEngine> friend double agenrnd(const Density & density,
//					adaptive_piecewise_segment_distribution<false> & upper_bound,
//					adaptive_piecewise_segment_distribution<false> & lower_bound, RandomEngine & rng);

} ;

template<class Density, class RandomEngine> inline double agenrnd(const Density & density,
				adaptive_piecewise_segment_distribution<true> & upper_bound, RandomEngine & rng) {
	static uniform_real_distribution<double> urdist { 0, 1 };
	if (upper_bound.segments.size() < upper_bound.segments.capacity()) {
		upper_bound.update_bound(density);
		while (true) {
			upper_bound(rng);
//			upper_bound.ptemp.density_value = density.value(upper_bound.temp.last_draw);
//			double upper { upper_bound.value(upper_bound.ptemp.last_draw) };
//			upper_bound.ptemp.observed_ratio = dclong::exp(upper_bound.ptemp.density_value - upper);
			double p { urdist(rng) };
			upper_bound.atemp.density_value = density.value(upper_bound.ptemp.last_draw);
			upper_bound.atemp.observed_ratio = std::exp(upper_bound.atemp.density_value - upper_bound.value());
			if (p <= upper_bound.atemp.observed_ratio) {
				return upper_bound.ptemp.last_draw;
			}
			upper_bound.update_bound(density);
		}
	} else {
		return dclong::genrnd(density, upper_bound, rng);
	}
}

//template<class Density, class RandomEngine> inline double agenrnd(const Density & density,
//				adaptive_piecewise_segment_distribution<true> & upper_bound,
//				adaptive_piecewise_segment_distribution<true> & lower_bound, RandomEngine & rng) {
//	static uniform_real_distribution<double> urdist { 0, 1 };
//	if (upper_bound.segments.size() < upper_bound.segments.capacity()) {
//		upper_bound.update_upper_bound(density);
//		if (lower_bound.segments.size() < lower_bound.segments.capacity()) {
//			lower_bound.update_lower_bound();
//			while (true) {
//				upper_bound.temp.last_draw = upper_bound(rng);
//				lower_bound.temp.last_draw = upper_bound.temp.last_draw;
//				upper_bound.temp.density_value = density.value(upper_bound.temp.last_draw);
//				lower_bound.temp.density_value = upper_bound.temp.density_value;
//				double upper { upper_bound.value(upper_bound.temp.last_draw) };
//				upper_bound.temp.observed_ratio = dclong::exp(upper_bound.temp.density_value - upper);
//				double lower { lower_bound.value(lower_bound.temp.last_draw) };
//				lower_bound.temp.observed_ratio = dclong::exp(lower - lower_bound.temp.density_value);
//				double p { urdist(rng) };
//				if (p <= upper_bound.temp.observed_ratio) {
//					return upper_bound.temp.last_draw;
//				}
//				upper_bound.update_upper_bound(density);
//				lower_bound.update_lower_bound();
//			}
//		} else {
//			while (true) {
//				upper_bound.temp.last_draw = upper_bound(rng);
//				upper_bound.temp.density_value = density.value(upper_bound.temp.last_draw);
//				double upper { upper_bound.value(upper_bound.temp.last_draw) };
//				upper_bound.temp.observed_ratio = dclong::exp(upper_bound.temp.density_value - upper);
//				double p { urdist(rng) };
//				if (p <= upper_bound.temp.observed_ratio) {
//					return upper_bound.temp.last_draw;
//				}
//				upper_bound.update_upper_bound(density);
//			}
//		}
//	} else {
//		if (lower_bound.segments.size() < lower_bound.segments.capacity()) {
//			lower_bound.update_lower_bound();
//			while (true) {
//				upper_bound.temp.last_draw = upper_bound(rng);
//				lower_bound.temp.last_draw = upper_bound.temp.last_draw;
//				upper_bound.temp.density_value = density.value(upper_bound.temp.last_draw);
//				lower_bound.temp.density_value = upper_bound.temp.density_value;
//				double upper { upper_bound.value(upper_bound.temp.last_draw) };
//				upper_bound.temp.observed_ratio = dclong::exp(upper_bound.temp.density_value - upper);
//				double lower { lower_bound.value(lower_bound.temp.last_draw) };
//				lower_bound.temp.observed_ratio = dclong::exp(lower - lower_bound.temp.density_value);
//				double p { urdist(rng) };
//				if (p <= upper_bound.temp.observed_ratio) {
//					return upper_bound.temp.last_draw;
//				}
//				lower_bound.update_lower_bound();
//			}
//		} else {
//			return dclong::genrnd(density, upper_bound, lower_bound, rng);
//		}
//	}
//}

template<class InputIt, class Density, class RandomEngine> double agenrnd(InputIt first, InputIt last,
				const Density & density, adaptive_piecewise_segment_distribution<true> & upper_bound,
				RandomEngine & rng) {
	for (InputIt it { first }; it != last; ++it) {
		*it = agenrnd(density, upper_bound, rng);
	}
}

//template<class InputIt, class Density, class RandomEngine> double agenrnd(InputIt first, InputIt last,
//				const Density & density, adaptive_piecewise_segment_distribution<true> & upper_bound,
//				adaptive_piecewise_segment_distribution<true> & lower_bound, RandomEngine & rng) {
//	for (InputIt it { first }; it != last; ++it) {
//		*it = agenrnd(density, upper_bound, lower_bound, rng);
//	}
//}

template<class Density, class RandomEngine> double inline agenrnd(const Density & density,
				adaptive_piecewise_segment_distribution<false> & upper_bound, RandomEngine & rng) {
	static uniform_real_distribution<double> urdist { 0, 1 };
	if (upper_bound.segments.size() < upper_bound.segments.capacity()) {
		upper_bound.update_bound(density);
		while (true) {
			upper_bound(rng);
//			upper_bound.temp.density_value = density.value(upper_bound.temp.last_draw);
//			double upper { upper_bound.value(upper_bound.temp.last_draw) };
//			upper_bound.temp.observed_ratio = upper_bound.temp.density_value / upper;

			double p { urdist(rng) };
			upper_bound.atemp.density_value = density.value(upper_bound.ptemp.last_draw);
			upper_bound.atemp.observed_ratio = upper_bound.atemp.density_value / upper_bound.value();
			if (p <= upper_bound.atemp.observed_ratio) {
				return upper_bound.ptemp.last_draw;
			}
			upper_bound.update_bound(density);
		}
	} else {
		return genrnd(density, upper_bound, rng);
	}
}

//template<class Density, class RandomEngine> inline double agenrnd(const Density & density,
//				adaptive_piecewise_segment_distribution<false> & upper_bound,
//				adaptive_piecewise_segment_distribution<false> & lower_bound, RandomEngine & rng) {
//	static uniform_real_distribution<double> urdist { 0, 1 };
//	if (upper_bound.segments.size() < upper_bound.segments.capacity()) {
//		upper_bound.update_upper_bound(density);
//		if (lower_bound.segments.size() < lower_bound.segments.capacity()) {
//			lower_bound.update_lower_bound();
//			while (true) {
//				upper_bound.temp.last_draw = upper_bound(rng);
//				lower_bound.temp.last_draw = upper_bound.temp.last_draw;
//				upper_bound.temp.density_value = density.value(upper_bound.temp.last_draw);
//				lower_bound.temp.density_value = upper_bound.temp.density_value;
//				double upper { upper_bound.value(upper_bound.temp.last_draw) };
//				upper_bound.temp.observed_ratio = upper_bound.temp.density_value / upper;
//				double lower { lower_bound.value(lower_bound.temp.last_draw) };
//				lower_bound.temp.observed_ratio = lower / lower_bound.temp.density_value;
//				double p { urdist(rng) };
//				if (p <= upper_bound.temp.observed_ratio) {
//					return upper_bound.temp.last_draw;
//				}
//				upper_bound.update_upper_bound(density);
//				lower_bound.update_lower_bound();
//			}
//		} else {
//			while (true) {
//				upper_bound.temp.last_draw = upper_bound(rng);
//				upper_bound.temp.density_value = density.value(upper_bound.temp.last_draw);
//				double upper { upper_bound.value(upper_bound.temp.last_draw) };
//				upper_bound.temp.observed_ratio = upper_bound.temp.density_value / upper;
//				double p { urdist(rng) };
//				if (p <= upper_bound.temp.observed_ratio) {
//					return upper_bound.temp.last_draw;
//				}
//				upper_bound.update_upper_bound(density);
//			}
//		}
//	} else {
//		if (lower_bound.segments.size() < lower_bound.segments.capacity()) {
//			lower_bound.update_lower_bound();
//			while (true) {
//				upper_bound.temp.last_draw = upper_bound(rng);
//				lower_bound.temp.last_draw = upper_bound.temp.last_draw;
//				upper_bound.temp.density_value = density.value(upper_bound.temp.last_draw);
//				lower_bound.temp.density_value = upper_bound.temp.density_value;
//				double upper { upper_bound.value(upper_bound.temp.last_draw) };
//				upper_bound.temp.observed_ratio = upper_bound.temp.density_value / upper;
//				double lower { lower_bound.value(lower_bound.temp.last_draw) };
//				lower_bound.temp.observed_ratio = lower / lower_bound.temp.density_value;
//				double p { urdist(rng) };
//				if (p <= upper_bound.temp.observed_ratio) {
//					return upper_bound.temp.last_draw;
//				}
//				lower_bound.update_lower_bound();
//			}
//		} else {
//			return dclong::genrnd(density, upper_bound, lower_bound, rng);
//		}
//	}
//}

//template<class InputIt, class Density, class RandomEngine> double agenrnd(InputIt first, InputIt last,
//				const Density & density, adaptive_piecewise_segment_distribution<false> & upper_bound,
//				adaptive_piecewise_segment_distribution<false> & lower_bound, RandomEngine & rng) {
//	for (InputIt it { first }; it != last; ++it) {
//		*it = agenrnd(density, upper_bound, lower_bound, rng);
//	}
//}

template<class InputIt, class Density, class RandomEngine> double agenrnd(InputIt first, InputIt last,
				const Density & density, adaptive_piecewise_segment_distribution<false> & upper_bound,
				RandomEngine & rng) {
	for (InputIt it { first }; it != last; ++it) {
		*it = agenrnd(density, upper_bound, rng);
	}
}

}

#endif /* ADAPTIVE_PIECEWISE_SEGMENT_DISTRIBUTION_HPP_ */
