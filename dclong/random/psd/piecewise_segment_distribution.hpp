/*
 * piecewise_segment_distribution.hpp
 *
 *  Created on: Oct 8, 2012
 *      Author: dclong
 */

#ifndef PIECEWISE_SEGMENT_DISTRIBUTION_HPP_
#define PIECEWISE_SEGMENT_DISTRIBUTION_HPP_

#include "dclong/string.hpp"
#include "dclong/random/psd/segment_distribution.hpp"
#include "dclong/random/distributions/distributions.hpp"
#include <vector>
#include <limits>
#include <algorithm>
#include <utility>
#include <tuple>
#include <iostream>
#include <cmath>
#include <cstddef>
namespace dclong {
using namespace std;

template<bool Exp = true> class piecewise_segment_distribution {
protected:
	vector<dclong::segment_distribution<Exp>> segments;
	struct piecewise_segment_distribution_temporary {
        dclong::discrete_distribution ddist;
		vector<double> scaled_ws;
		double ws_ave;
		double integral;
		double last_draw;
		int last_index;
		piecewise_segment_distribution_temporary(size_t n) :
						scaled_ws(n) {
		}
	} ptemp;

	void update_state() {
		int size = segments.size();
		for (int i = 0; i < size; ++i) {
			bool updated = segments[i].update_state();
		}
		scale_weights();
	}

	int which_seg(double x) const {
			const int i = std::upper_bound(segments.begin(), segments.end(), x,
							[](double v1, const segment_distribution<Exp> & seg) {
								return v1 < seg.x2();
							}) - segments.begin();
			if (i > 0) {
				return i;
			}
			// i==0
			if (x >= segments[0].x1()) {
				return 0;
			}
			return -1;
		}

	inline double value() const {
			return segments[ptemp.last_index].value(ptemp.last_draw);
		}

//	typename vector<dclong::segment_distribution<Exp>>::const_iterator which_seg(double x) const {
//		return std::upper_bound(segments.begin(), segments.end(), x,
//						[](double v1, const segment_distribution<Exp> & seg) {
//							return v1 < seg.x2();
//						});
//	}

private:
	/**
	 * This allows more convenient constructors if the density function is continuous.
	 */
	void segment_domain(double minx, double maxx) {
		const int last = segments.size() - 1;
		double xleft { minx };
		for (int i = 0; i < last; ++i) {
			double xright { segments[i].intersection(segments[i + 1]) };
			segments[i].domain(make_pair(xleft, xright));
			xleft = xright;
		}
		segments[last].domain(make_pair(xleft, maxx));
	}
	void resize(const int size) {
		segments.resize(size);
		ptemp.scaled_ws.resize(size);
	}

	void scale_weights();

	virtual void segs_extra() {
	}



	double value(double x) const;

public:
	virtual ~piecewise_segment_distribution() {
	}

	template<class RandomEngine> double operator()(RandomEngine & rng) {
		ptemp.last_index = ptemp.ddist(rng);
		ptemp.last_draw = segments[ptemp.last_index](rng);
		return ptemp.last_draw;
	}
	template<class RandomEngine> double genrnd(RandomEngine & rng) {
		ptemp.last_index = ptemp.ddist(rng);
				ptemp.last_draw = segments[ptemp.last_index](rng);
				return ptemp.last_draw;
	}
	template<class InputIt, class RandomEngine> void operator()(InputIt first, InputIt last, RandomEngine & rng) {
		for (InputIt it { first }; it != last; ++it) {
			*it = operator()(rng);
		}
	}
	template<class InputIt, class RandomEngine> void genrnd(InputIt first, InputIt last, RandomEngine & rng) {
		for (InputIt it { first }; it != last; ++it) {
			*it = genrnd(rng);
		}
	}
	void swap(piecewise_segment_distribution<Exp> & other) {
		std::swap(segments, other.segments);
		std::swap(ptemp, other.ptemp);
	}

	piecewise_segment_distribution(const piecewise_segment_distribution<Exp> & other) :
					segments { other.segments }, ptemp { other.ptemp } {
	}

	piecewise_segment_distribution(piecewise_segment_distribution<Exp> && other) :
	segments {move(other.segments)},
	ptemp {move(other.ptemp)} {}

	piecewise_segment_distribution<Exp> & operator=(const piecewise_segment_distribution<Exp> & other) {
		piecewise_segment_distribution<Exp>(other).swap(*this);
		return *this;
	}

	piecewise_segment_distribution<Exp> & operator=(piecewise_segment_distribution<Exp> && other) {
		piecewise_segment_distribution<Exp>(other).swap(*this);
		return *this;
	}

	piecewise_segment_distribution() :
	piecewise_segment_distribution(0) {}

	piecewise_segment_distribution(unsigned int capacity) : ptemp {capacity}, segments {capacity} {
	}
	/**
	 * A constructor that takes the x and y coordiantes as arguments.
	 * @param xs a vector of x coordinates.
	 * @param ys a vector of y coordinates.
	 */
	piecewise_segment_distribution(const vector<double> & xs,
					const vector<double> & ys, int capacity) :
	segments(capacity),
	ptemp(capacity) {
		segs(xs, ys);
	}

	template<class Density> void segs(const vector<double> & xs, const vector<double> & ys) {
		const int size = xs.size() - 1;
		resize(size);
		for(int i=0; i<size; ++i) {
			segments[i].ab(xs[i], ys[i], xs[i+1], ys[i+1]);
			segments[i].domain(xs[i], xs[i+1]);
		}
		update_state();
		segs_extra();
	}

	piecewise_segment_distribution(const vector<double> & xs,
					const vector<double> & ys) :
	piecewise_segment_distribution(xs, ys, xs.size()) {}
	/**
	 * A constructor that takes a bound of points as the argument.
	 * @param points a vector of points,
	 * where a point is a pair of x and y coordinates.
	 */
	piecewise_segment_distribution(const vector<pair<double, double>> & points, int capacity) :
	segments(capacity),
	ptemp(capacity) {
		segs(points);
	}
	template<class Density> void segs(const vector<pair<double, double>> & points) {
		const int size = points.size() - 1;
		resize(size);
		for(int i=0; i<size; ++i) {
			segments[i].ab(points[i], points[i+1]);
			segments[i].domain(points[i].first, points[i+1].first);
		}
		update_state();
		segs_extra();
	}
	piecewise_segment_distribution(const vector<pair<double, double>> & points) :
	piecewise_segment_distribution(points, points.size()) {}
	/**
	 * A constructor that takes x coordinates and y coordinates as arguments.
	 * @param xs a vector of x coordinates.
	 * @param ys a vector of pairs of y coordinates.
	 * The pair of y coordinates is the y value of the left and right end point
	 * of the corresponding seg defined on the intervals specified by the x coordinates.
	 * The size of ys is 1 less than the size of xs.
	 *
	 * This allows non continuous density functions.
	 */
	piecewise_segment_distribution(const vector<double> & xs, const vector<pair<double, double>> & ys, int capacity) :
	segments(capacity),
	ptemp(capacity) {
		segs(xs, ys);
	}
	void segs(const vector<double> & xs, const vector<pair<double, double>> & ys) {
		const int size = ys.size();
		resize(size);
		for(int i=0; i<size; ++i) {
			segments[i].ab(xs[i], ys[i].first, xs[i+1], ys[i].second);
			segments[i].domain(xs[i], xs[i+1]);
		}
		update_state();
		segs_extra();
	}
	piecewise_segment_distribution(const vector<double> & xs, const vector<pair<double, double>> & ys) :
	piecewise_segment_distribution(xs, ys, ys.size()) {}
	/**
	 * A constructor that takes a seg functions
	 * and the domain of density as arguments.
	 * @param segs a vector of segs,
	 * where a seg is a pair of slope (a) and intercept (b).
	 * @param minx the minimum value of x.
	 * @param maxx the maximum value of x.
	 */
//	piecewise_segment_distribution(const vector<dclong::line> & lines, double minx, double maxx, int capacity) :
//	segments(capacity),
//	ptemp(capacity) {
//		segs(lines, minx, maxx);
//	}
//	void segs(const vector<dclong::line> & lines, double minx, double maxx) {
//		const int size = lines.size();
//		resize(size);
//		for(int i=0; i<size; ++i) {
//			segments[i].ab(lines[i]);
//		}
//		segment_domain(minx, maxx);
//		update_state();
//		segs_extra();
//	}
//	piecewise_segment_distribution(const vector<dclong::line> & lines, double minx, double maxx) :
//	piecewise_segment_distribution(lines, minx, maxx, lines.size()) {}
					/**
					 * A constructor that takes slopes, intercepts and the
					 * domain of density as arguments.
					 * @param as a vector of slope of segs.
					 * @param bs a vector of intercept of segs.
					 * @param minx the minimum value of x.
					 * @param maxx the maximum value of x.
					 */

					piecewise_segment_distribution(const vector<double> & as, const vector<double> & bs,
									double minx, double maxx, int capacity) :
					segments(capacity),
					ptemp(capacity) {
						segs(density, as, bs, minx, maxx);
					}
					void segs(const vector<double> & as, const vector<double> & bs, double minx, double maxx) {
						const int size = as.size();
						resize(size);
						for(int i=0; i<size; ++i) {
							segments[i].ab(as[i], bs[i]);
						}
						segment_domain(minx, maxx);
						update_state();
						segs_extra();
					}
					piecewise_segment_distribution(const vector<double> & as, const vector<double> & bs,
									double minx, double maxx) :
					piecewise_segment_distribution(as, bs, minx, maxx, as.size()) {}
					/**
					 * A constructor that takes a bound of points and the
					 * domain of density as arguments.
					 * @param segs a vector of segs.
					 * Each seg is represented by (x, y, a),
					 * where (x,y) is a point on the seg and a is the slope of the seg.
					 * @param minx
					 * @param maxx
					 */
					piecewise_segment_distribution(const vector<pair<double, double>> & points, const vector<double> & as,
									double minx, double maxx, int capacity) :
					segments(capacity),
					ptemp(capacity) {
						segs(points, as, minx, maxx);
					}
					vector<dclong::segment_distribution<Exp>> segs() const {
						return segments;
					}
					void segs(const vector<pair<double, double>> & points, const vector<double> & as, double minx, double maxx) {
						const int size = as.size();
						resize(size);
						for(int i=0; i<size; ++i) {
							segments[i].ab(points[i], as[i]);
						}
						segment_domain(minx, maxx);
						update_state();
						segs_extra();
					}
					piecewise_segment_distribution(const vector<pair<double, double>> & points, const vector<double> & as,
									double minx, double maxx) :
					piecewise_segment_distribution(points, as, minx, maxx, as.size()) {}

					piecewise_segment_distribution(const vector<double> & xs, const vector<double> & ys, const vector<double> & as,
									double minx, double maxx, int capacity) :
					segments(capacity),
					ptemp(capacity) {
						segs(xs, ys, as, minx, maxx);
					}

					template<class Density> piecewise_segment_distribution(const Density & density, const vector<double> & xs, double minx, double maxx, int capacity) :
					segments(capacity),
					ptemp(capacity) {
						upper_bound(density, xs, minx, maxx);
					}

					template<class Density> piecewise_segment_distribution(const vector<double> & xs, double minx, double maxx) :
					piecewise_segment_distribution(xs, minx, maxx, xs.size()) {
					}

					template<class Density> piecewise_segment_distribution(const Density & density, const vector<double> & xs,
									int capacity, bool upper_bound) :
					segments(capacity),
					ptemp(capacity) {
							this -> upper_bound(density, xs, xs.front(), xs.back());

					}

					template<class Density> piecewise_segment_distribution(const Density & density, const vector<double> & xs,
									bool upper_bound) :
					piecewise_segment_distribution(density, xs, xs.size(), upper_bound)
					{}

//					template<class Density> void lower_bound(const Density & density, const vector<double> & xs) {
//						int size = xs.size();
//						if(size<2) {
//							cerr << "At least two points are required to build the lower bound.\n";
//							exit(1);
//						}
//						if(std::isinf(xs.front())||std::isinf(xs.back())) {
//							cerr << "Lower bound cannot container positive/negative infinity.\n";
//							exit(1);
//						}
//						vector<double> ys(size);
//						for(int i=0; i<size; ++i) {
//							ys[i] = density.value(xs[i]);
//						}
//						segs(density, xs, ys);
//					}

					template<class Density> void upper_bound(const Density & density, const vector<double> & xs, double minx, double maxx) {
						if (density.has_derivative()) {
							// use derivative
							int size = xs.size();
							vector<double> ys(size);
							vector<double> as(size);
							for (int i = 0; i < size; ++i) {
								ys[i] = density.value(xs[i]);
								as[i] = density.derivative(xs[i]);
							}
							segs(xs, ys, as, minx, maxx);
						} else {
							// derivative-free, do it from left to right and then from right to left

						}
					}

					template<class Density> void upper_bound(const Density & density, const vector<double> & xs) {
						upper_bound(density, xs, xs.front(), xs.back());
					}

					void segs(const vector<double> & xs, const vector<double> & ys, const vector<double> & as, double minx, double maxx) {
						const int size = as.size();
						resize(size);
						for(int i=0; i<size; ++i) {
							segments[i].ab(xs[i], ys[i], as[i]);
						}
						segment_domain(minx, maxx);
						update_state();
						segs_extra();
					}

					/**
					 * Set seg i.
					 * This is needed when the domain is infinite,
					 * i.e., x[0] is -inf or x[x.size() -1] is inf.
					 * @param i the index of the seg to be set.
					 * You must call the function update_weights after you finished
					 * setting segs in order to get weights updated.
					 */
//					void seg(const dclong::line & line, int i) {
//						segments[i].ab(line);
//					}

					void seg(double a, double b, int i) {
						segments[i].ab(a, b);
					}

					int segs_number() const {
						return segments.size();
					}

					double x1() const {
						if(segments.size()>0) {
							return segments[0].x1();
						}
						return numeric_limits<double>::signaling_NaN();
					}

					double x2() const {
						if(segments.size()>0) {
							return segments.back().x2();
						}
						return numeric_limits<double>::signaling_NaN();
					}

					pair<double, double> domain() const {
						return make_pair(x1(), x2());
					}

					double density(double x) const;



					/**
					 * Must be called before you do useful things.
					 * No need to be called again if no change.
					 */
					string to_string() const {
						string s {""};
						int size = segments.size();
						for(int i=0; i<size; ++i) {
							s += "Segment " + std::to_string(i) + ": " + segments[i].to_string() + "\n";
						}
						s += "[" + dclong::to_string(ptemp.scaled_ws.begin(), ptemp.scaled_ws.end()) + "]";
						return s;
					}

					template<class Density, class RandomEngine> friend double genrnd(const Density & density,
									piecewise_segment_distribution<true> & upper_bound, RandomEngine & rng);

//					template<class Density, class RandomEngine> friend double genrnd(const Density & density,
//									piecewise_segment_distribution<true> & upper_bound,
//									piecewise_segment_distribution<true> & lower_bound, RandomEngine & rng);

					template<class Density, class RandomEngine> friend double genrnd(const Density & density,
									piecewise_segment_distribution<false> & upper_bound, RandomEngine & rng);

//					template<class Density, class RandomEngine> friend double genrnd(const Density & density,
//									piecewise_segment_distribution<false> & upper_bound,
//									piecewise_segment_distribution<false> & lower_bound, RandomEngine & rng);
				};

template<class Density, class RandomEngine> double genrnd(const Density & density,
				piecewise_segment_distribution<false> & upper_bound, RandomEngine & rng) {
	static uniform_real_distribution<double> urdist { 0, 1 };
	while (true) {
		upper_bound(rng);
		double p { urdist(rng) };
		if(p <= density.value(upper_bound.ptemp.last_draw) / upper_bound.value()){
			return upper_bound.ptemp.last_draw;
		}
	}
}

//template<class Density, class RandomEngine> double genrnd(const Density & density,
//				piecewise_segment_distribution<false> & upper_bound,
//				piecewise_segment_distribution<false> & lower_bound, RandomEngine & rng) {
//	static uniform_real_distribution<double> urdist { 0, 1 };
//	while (true) {
//		double x { upper_bound(rng) };
//		double y { urdist(rng) * upper_bound.value() };
//		if (y <= lower_bound.value(x)) {
//			return x;
//		}
//		if (y <= density.value(x)) {
//			return x;
//		}
//	}
//}

template<class InputIt, class Density, class RandomEngine> double genrnd(InputIt first, InputIt last,
				const Density & density, piecewise_segment_distribution<false> & upper_bound, RandomEngine & rng) {
	for (InputIt it { first }; it != last; ++it) {
		*it = genrnd(density, upper_bound, rng);
	}
}

//template<class InputIt, class Density, class RandomEngine> double genrnd(InputIt first, InputIt last,
//				const Density & density, piecewise_segment_distribution<false> & upper_bound,
//				piecewise_segment_distribution<false> & lower_bound, RandomEngine & rng) {
//	for (InputIt it { first }; it != last; ++it) {
//		*it = genrnd(density, upper_bound, lower_bound, rng);
//	}
//}

template<class Density, class RandomEngine> double genrnd(const Density & density,
				piecewise_segment_distribution<true> & upper_bound, RandomEngine & rng) {
	static uniform_real_distribution<double> urdist { 0, 1 };
	while (true) {
		upper_bound(rng);
		double p { urdist(rng) };
		if(p <= std::exp(density.value(upper_bound.ptemp.last_draw) - upper_bound.value())){
			return upper_bound.ptemp.last_draw;
		}
	}
}

//template<class Density, class RandomEngine> double genrnd(const Density & density,
//				piecewise_segment_distribution<true> & upper_bound, piecewise_segment_distribution<true> & lower_bound,
//				RandomEngine & rng) {
//	static uniform_real_distribution<double> urdist { 0, 1 };
//	while (true) {
//		double x { upper_bound(rng) };
//		double y { dclong::log(urdist(rng)) + upper_bound.value() };
//		if (y <= lower_bound.value(x)) {
//			return x;
//		}
//		if (y <= density.value(x)) {
//			return x;
//		}
//	}
//}

template<class InputIt, class Density, class RandomEngine> double genrnd(InputIt first, InputIt last,
				const Density & density, piecewise_segment_distribution<true> & upper_bound, RandomEngine & rng) {
	for (InputIt it { first }; it != last; ++it) {
		*it = genrnd(density, upper_bound, rng);
	}
}

//template<class InputIt, class Density, class RandomEngine> double genrnd(InputIt first, InputIt last,
//				const Density & density, piecewise_segment_distribution<true> & upper_bound,
//				piecewise_segment_distribution<true> & lower_bound, RandomEngine & rng) {
//	for (InputIt it { first }; it != last; ++it) {
//		*it = genrnd(density, upper_bound, lower_bound, rng);
//	}
//}

template<> double piecewise_segment_distribution<true>::value(double x) const {
	int i = which_seg(x);
	if (i >= 0 && i < segments.size()) {
		return segments[i].value(x);
	}
	return -numeric_limits<double>::infinity();
}

template<> double piecewise_segment_distribution<false>::value(double x) const {
	int i = which_seg(x);
	if (i >= 0 && i < segments.size()) {
		return segments[i].value(x);
	}
	return 0;
}

template<> double piecewise_segment_distribution<true>::density(double x) const {
	return dclong::exp(value(x) - ptemp.integral);
}

template<> double piecewise_segment_distribution<false>::density(double x) const {
	return value(x) / ptemp.integral;
}

template<> void piecewise_segment_distribution<true>::scale_weights() {
	// scale and recover the weights
	double max_ws = -numeric_limits<double>::infinity();
	int size = segments.size();
	for (int i = 0; i < size; ++i) {
		double current_ws = segments[i].integral();
		if (current_ws > max_ws) {
			max_ws = current_ws;
		}
	}
	ptemp.ws_ave = max_ws - 5;
	for (int i = segments.size() - 1; i >= 0; --i) {
		ptemp.scaled_ws[i] = dclong::exp(segments[i].integral() - ptemp.ws_ave);
	}
	//ptemp.ddist.param(discrete_distribution<int>::param_type(ptemp.scaled_ws.begin(), ptemp.scaled_ws.end()));
	ptemp.ddist.param(ptemp.scaled_ws.begin(), ptemp.scaled_ws.end());
	ptemp.integral = ptemp.ws_ave + dclong::log(accumulate(ptemp.scaled_ws.begin(), ptemp.scaled_ws.end(), 0.0));
}

template<> void piecewise_segment_distribution<false>::scale_weights() {
	transform(segments.begin(), segments.end(), ptemp.scaled_ws.begin(),
					[](const dclong::segment_distribution<false> & seg) {
						return seg.integral();
					});
	//ptemp.ddist.param(discrete_distribution<int>::param_type(ptemp.scaled_ws.begin(), ptemp.scaled_ws.end()));
	ptemp.ddist.param(ptemp.scaled_ws.begin(), ptemp.scaled_ws.end());
	ptemp.integral = accumulate(ptemp.scaled_ws.begin(), ptemp.scaled_ws.end(), 0.0);
}

}

#endif /* PIECEWISE_SEGMENT_DISTRIBUTION_HPP_ */
