/*
 * segment_distribution.hpp
 *
 *  Created on: Oct 8, 2012
 *      Author: dclong
 */

#ifndef SEGMENT_DISTRIBUTION_HPP_
#define SEGMENT_DISTRIBUTION_HPP_

#include "dclong/math/geometry/line/segment.hpp"
#include "dclong/math/function/function.hpp"
#include "dclong/string.hpp"
#include <random>
#include <limits>
#include <utility>
#include <iostream>
#include <string>
#include <cmath>

namespace dclong{
using namespace std;

namespace helper{
template<bool Exp=true> struct segment_distribution_temporary {};
template<> struct segment_distribution_temporary<true> {
	double edy_m1_appr;
	double edy_m1_exact;
	double edy_m1_overa_appr;
	// log exp integral
	double integral;
	double epsilon{1e-3};
	string to_string() const {
		return "<" + std::to_string(edy_m1_appr) + ", " + std::to_string(edy_m1_exact) + ", "
				+ std::to_string(edy_m1_overa_appr) + ", " + std::to_string(integral)
		+ ", " + std::to_string(epsilon) + ">";
	}
};

template<> struct segment_distribution_temporary<false> {
	double w0;
	// linear line integral
	double integral;
	string to_string() const {
		return "<" + std::to_string(w0) + ", " + std::to_string(integral) + ">";
	}
};
}

template<bool Exp> class segment_distribution : public dclong::segment {
	private:
		helper::segment_distribution_temporary<Exp> stemp;
		uniform_real_distribution<double> urdist{0, 1};
		double integrate();
		virtual void update_state_extra() override;
//	protected:
	public:
		virtual ~segment_distribution(){};

		void swap(segment_distribution<Exp> & other){
			dclong::segment::swap(other);
			std::swap(urdist, other.urdist);
			std::swap(stemp, other.stemp);
		}
		segment_distribution(const segment_distribution<Exp> & other) :
				dclong::segment{other},
				stemp(other.stemp),
				urdist{other.urdist} {}

		segment_distribution(segment_distribution<Exp> && other) :
			dclong::segment{move(other)},
			stemp(move(other.stemp)),
			urdist{move(other.urdist)} {}

		segment_distribution & operator=(const segment_distribution<Exp> & other){
			segment_distribution<Exp>(other).swap(*this);
			return *this;
		}

		segment_distribution & operator=(segment_distribution<Exp> && other){
			segment_distribution<Exp>(other).swap(*this);
			return *this;
		}

        segment_distribution() :
        	dclong::segment()
        	{}
        segment_distribution(double x, double y, double a, pair<double, double> xrange) :
        	dclong::segment(x, y, a, xrange) {}

		segment_distribution(const pair<double, double> & p1, const pair<double, double> & p2) :
			dclong::segment(p1, p2) {}

//		segment_distribution(const dclong::line & l, const pair<double, double> & r) :
//			dclong::segment(l, r) {}
//
//		segment_distribution(const dclong::line & l) :
//			dclong::segment(l) {}
//
//		segment_distribution(dclong::line && l, const pair<double, double> & r) :
//			dclong::segment{move(l), r} {}
//
//		segment_distribution(const dclong::line & l, pair<double, double> && r) :
//			dclong::segment(l, move(r)) {}
//
//		segment_distribution(dclong::line && l, pair<double, double> && range) :
//			dclong::segment(move(l), move(range)) {}
//
//		segment_distribution(const dclong::line && l) :
//			dclong::segment(move(l)) {}

        template<class RandomEngine> double operator()(RandomEngine & rng);

		template<class RandomEngine, class InputIt> segment_distribution<Exp> &
		operator()(InputIt first, InputIt last, RandomEngine & rng) {
			for(InputIt it{first}; it!=last; ++it){
				*it = operator()(rng);
			}
		}

		double density(double x) const;

		inline double integral() const {
			return stemp.integral;
		}

		inline double ratio() const {
			return stemp.ratio;
		}

        double error() const;

        void error(double);

		virtual string to_string() const override {
			return dclong::segment::to_string() + " " + stemp.to_string();
		}
};

template<> double segment_distribution<true>::density(double x) const {
	return dclong::exp(value(x) - stemp.integral);
}

template<> double segment_distribution<false>::density(double x) const{
	return value(x)/stemp.integral;
}

template<> double segment_distribution<true>::integrate(){
	static constexpr double inf{std::numeric_limits<double>::infinity()};
	static constexpr double sig_nan{numeric_limits<double>::signaling_NaN()};
		if(xrange.first>-inf){
			if(xrange.second<inf){
				if(std::abs(difference.second) < stemp.epsilon){
					if(slope!=0){
						return dclong::log(stemp.edy_m1_overa_appr) + y1();
					}
					// slope == 0
					return dclong::log(difference.first) + y1();
				}
				return dclong::log(stemp.edy_m1_exact/slope) + y1();
			}
			// x2 == inf
			if(slope>0){
				return inf;
			}
			if(slope<0){
				return y1() - dclong::log(-slope);
			}
			// slope==0
			if(intercept>-inf){
				return inf;
			}
			// slope==0 && intercept==0, not well defined
			return sig_nan;
		}
		// x1 == -inf
		if(slope>0){
			return y2() - dclong::log(slope);
		}
		if(slope<0){
			return inf;
		}
		// slope == 0
		if(intercept>-inf){
			return inf;
		}
		// slope==0 and intercept==-inf not well defined
		return sig_nan;
}

template<> double segment_distribution<false>::integrate(){
	if(slope!=0||intercept!=0){
		return 0.5 * yminsum.second * difference.first;
	}
	return 0;
}
/**
 * You should put these conditions into the constructor of this class
 * and setter methods.
 */
template<> template<class RandomEngine> double segment_distribution<true>::operator()(RandomEngine & rng){
	static constexpr double inf{std::numeric_limits<double>::infinity()};
	static constexpr double sig_nan{numeric_limits<double>::signaling_NaN()};
	if(xrange.first>-inf){
		if(xrange.second<inf){
			if(std::abs(difference.second) < stemp.epsilon){
				double p{urdist(rng)};
				double u{p * stemp.edy_m1_appr};
				double u2{u * u};
				double u3{u2 * u};
				double u4{u3 * u};
				double log1pu_overu_appr = (1 - 0.5 * u + u2 / 3 - 0.25 * u3 + 0.2 * u4);
				return p * stemp.edy_m1_overa_appr * log1pu_overu_appr + xrange.first;
			}
			double p{urdist(rng)};
			return dclong::log(p * stemp.edy_m1_exact + 1) / slope + xrange.first;
		}
		// x2 == inf x1>-inf
		if(slope<0){
			double p{urdist(rng)};
			return dclong::log(p) / slope + xrange.first;
		}
		return sig_nan;
	}
	// x1==-inf
	if(xrange.second<inf){
		//x1==-inf x2<inf
		if(slope>0){
			double p{urdist(rng)};
			return dclong::log(p) /slope + xrange.second;
		}
		return sig_nan;
	}
	// x1==-inf x2==inf
	return sig_nan;
}

template<> template<class RandomEngine> double segment_distribution<false>::operator()(RandomEngine & rng){
	if(urdist(rng)<=stemp.w0){
		// in rectangle
		return urdist(rng);
	}
	// in triangle
	if(difference.second>0){
		return std::max(urdist(rng), urdist(rng));
	}
	if(difference.second<0){
		return std::min(urdist(rng), urdist(rng));
	}
	// yleft == yright , this won't happen, but just in case
	return urdist(rng);
}

template<> void segment_distribution<true>::error(double epsilon){
	stemp.epsilon = epsilon;
	updated = true;
}

template<> double segment_distribution<true>::error() const{
	return stemp.epsilon;
}

template<> void segment_distribution<true>::update_state_extra(){
	double ydiff2 = difference.second * difference.second;
    double ydiff3 = ydiff2 * difference.second;
    double ydiff4 = ydiff3 * difference.second;
    double edy_m1_overdy = 1 + 0.5 * difference.second + ydiff2/6 + ydiff3/24 + ydiff4/120;
    stemp.edy_m1_appr = edy_m1_overdy * difference.second;
    stemp.edy_m1_overa_appr = edy_m1_overdy * difference.first;
    stemp.edy_m1_exact = dclong::exp(difference.second) - 1;
    stemp.integral = integrate();
}

template<> void segment_distribution<false>::update_state_extra(){
	urdist.param(uniform_real_distribution<double>::param_type(xrange.first, xrange.second));
	stemp.w0 = yminsum.first * difference.first;
	double w1{0.5 * difference.first * std::abs(difference.second)};
	stemp.w0 /= stemp.w0 + w1;
	// transform w0 to a cut in range
	stemp.w0 = stemp.w0 * difference.first + xrange.first;
	stemp.integral = integrate();
}

}


#endif /* SEGMENT_DISTRIBUTION_HPP_ */
