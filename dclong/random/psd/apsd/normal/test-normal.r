
test.normal=function(n.data, n.hist, file, data,...){
    if(missing(data)){
        readBin(file,'double',n.data,8)-> data
    }
    hist(data,n=n.hist,prob=T,...)
    x=seq(-100,100,length.out=10000)
    y = dnorm(x)
    lines(x,y,col="red")
}