
#include "dclong/utility/utility.hpp"
#include "dclong/random/psd/adaptive_piecewise_segment_distribution.hpp"
#include <iostream>
#include <vector>
#include <set>
#include <fstream>
#include <random>
//#include <cmath>
using namespace std;
typedef dclong::adaptive_piecewise_segment_distribution<true> apst;
typedef mt19937 RandomEngine;
struct Density {
    double _mu;
    void mu(double mu){
        _mu = mu;
    }
    bool constexpr has_derivative() const{
        return true;
    }
    double value(double x) const{
        double xmmu = x - _mu;
        return -0.5 * xmmu * xmmu;
    }
    double derivative(double x) const{
        return _mu - x;
    }
};
void test1(){
    static double inf = numeric_limits<double>::infinity();
    vector<double> x{-2, 2};
    Density density;
    density.mu(0);
    RandomEngine rng(1111111);
    apst p{density, x, -inf, inf, 1.8};
    int n = 10000000;
    dclong::timer timer;
    vector<double> sam(n);
    agenrnd(sam.begin(), sam.end(), density, p, rng);
    timer.end();
    timer.print_milliseconds("generating 10,000,000 draws from n(0,1) adaptively");
    ofstream ofs("normal1.bin", ios::binary);
        if(ofs){
           ofs.write(reinterpret_cast<char*>(&sam[0]), sizeof(double) * n);
           ofs.close();
        }
    timer.reset();
    genrnd(sam.begin(), sam.end(), density, p, rng);
    timer.stop();
    timer.print_milliseconds("generating 10,000,000 draws from n(0,1) non-adaptively based on previous bounds");
    ofs.open("normal2.bin");
        if(ofs){
           ofs.write(reinterpret_cast<char*>(&sam[0]), sizeof(double) * n);
           ofs.close();
        }
    timer.reset();
    p.genrnd(sam.begin(), sam.end(), rng);
    timer.stop();
    timer.print_milliseconds("generating 10,000,000 draws from previous bounds");
    timer.reset();
    dclong::piecewise_segment_distribution<true> p2{p};
    genrnd(sam.begin(), sam.end(), density, p2, rng);
    timer.end();
    timer.print_milliseconds("generating 10,000,000 draws from n(0,1) non-adaptively based on casted previous bounds");
    ofs.open("normal3.bin");
    if(ofs){
       ofs.write(reinterpret_cast<char*>(&sam[0]), sizeof(double) * n);
       ofs.close();
    }
    //cout << p.to_string() << endl;
    //cout << p2.to_string() << endl;
    //p1.update_state();
    //cout << p1.to_string() << endl;
}
void test2(){
    static double inf = numeric_limits<double>::infinity();
    uniform_real_distribution<double> urdist{0,1};
    vector<double> x{-2, 2};
    Density density;
    RandomEngine rng(11111);
    apst p;
    int n = 1000000;
    vector<double> sam(n);
    for(int i=0; i<n; ++i){
        if(urdist(rng)<0.5){
            density.mu(-10);
            p.upper_bound(density, {-11, 9},-inf, inf);
        }else{
            density.mu(10);
            p.upper_bound(density, {9, 11},-inf, inf);
        }
        sam[i] = agenrnd(density, p, rng);
    }
    ofstream ofs("mix-normal.bin", ios::binary);
    if(ofs){
       ofs.write(reinterpret_cast<char*>(&sam[0]), sizeof(double) * n);
       ofs.close();
    }
    cout << p.to_string() << endl;
    //p1.update_state();
    //cout << p1.to_string() << endl;
}
void test3(){
    normal_distribution<double> nd;
    mt19937 mt(11111);
    int n = 10000000;
    dclong::timer timer;
    vector<double> sam(n);
    for(int i=0; i<n; ++i){
        sam[i] = nd(mt);
    }
    timer.stop();
    timer.print_milliseconds("generating 10,000,000 draws from normal distribution");
}
void test4(){
    uniform_real_distribution<double> udist{0,1};
    mt19937 mt(11111);
    int n = 10000000;
    dclong::timer timer;
    vector<double> sam(n);
    for(int i=0; i<n; ++i){
        sam[i] = udist(mt);
    }
    timer.stop();
    timer.print_milliseconds("generating 10,000,000 draws from U(0, 1) distribution");
}
void test5(){
    uniform_real_distribution<double> udist{0,1};
    mt19937 mt(11111);
    int n = 10000000;
    dclong::timer timer;
    vector<double> sam(n);
    for(int i=0; i<n; ++i){
        sam[i] = sqrt(udist(mt));
    }
    timer.stop();
    timer.print_milliseconds("generating 10,000,000 draws from L(0, 1) distribution");
}
/**
 * generating a u(0,1) random variable takes more 
 * time than simple calculations such as sqrt
 * so if you really care about performance, 
 * you have to use the right way ...
 */
void test6(){
    uniform_real_distribution<double> udist{0,1};
    mt19937 mt(11111);
    int n = 10000000;
    dclong::timer timer;
    vector<double> sam(n);
    for(int i=0; i<n; ++i){
        sam[i] = std::max(udist(mt), udist(mt));
    }
    timer.stop();
    timer.print_milliseconds("generating 10,000,000 draws from L(0, 1) distribution");
}
void test7(){
    vector<double> x{1,2,3,4,5,6};
    discrete_distribution<int> ddist{x.begin(), x.end()};
    mt19937 mt(11111);
    int n = 10000000;
    dclong::timer timer;
    vector<double> sam(n);
    for(int i=0; i<n; ++i){
        sam[i] = ddist(mt);
    }
    timer.stop();
    timer.print_milliseconds("generating 10,000,000 draws from L(0, 1) distribution");
}
template<class RandomEngine> int test8_1(const vector<pair<double, int>> & weights, RandomEngine & rng){
    static uniform_real_distribution<double> urdist{0, 1};
    double p{urdist(rng)};
    return std::upper_bound(weights.begin(), weights.end(), p, [](const double x1, const pair<double, int> & x2){
               return x1 < (x2.first); 
    }) -> second;
}
/**
 * It seems that the ordered algorithm is much faster than the non-ordered STL algorithm
 * you can write you own library... 
 * a even better way is to calculate the expected value, and always start from there.
 * you can write you own version to do this ...
 * i.e., check which is the first one greater than 0.5, start from there is a good idea.
 * you can write your own search algorithm ...
 *
 *
 * a even better way generate lots of draws, is to generate, sort, and assign together ...
 *
 *
 * it seems that sample in R is super fast, it must uses some good algorithm that I don't know ...
 */
void test8(){
    vector<double> x{6,11,15,18,20,22};
    vector<pair<double, int>> weights;
    for(int i=0; i<6; ++i){
        weights.push_back(make_pair(x[i]/21.0, i));
    }
    mt19937 mt(11111);
    int n = 10000000;
    dclong::timer timer;
    vector<double> sam(n);
    for(int i=0; i<n; ++i){
        sam[i] = test8_1(weights, mt);
    }
    timer.stop();
    timer.print_milliseconds("generating 10,000,000 draws from L(0, 1) distribution");
}
void test9(){
    uniform_real_distribution<double> urdist{0, 1};
    mt19937 mt(11111);
    int n = 10000000;
    dclong::timer timer;
    set<double> sam;
    for(int i=0; i<n; ++i){
        sam.insert(urdist(mt));
    }
    timer.stop();
    timer.print_milliseconds("generating 10,000,000 draws from L(0, 1) distribution");
}
void test10(){
    uniform_real_distribution<double> urdist{0, 1};
    mt19937 mt(11111);
    int n = 10000000;
    dclong::timer timer;
    vector<double> sam(n);
    for(int i=0; i<n; ++i){
        sam[i] = urdist(mt);
    }
    std::sort(sam.begin(), sam.end());
    timer.stop();
    timer.print_milliseconds("generating 10,000,000 draws from L(0, 1) distribution");
}
void test11(){
    uniform_real_distribution<double> urdist{0, 1};
    mt19937 mt(11111);
    int n = 10000000;
    dclong::timer timer;
    vector<double> sam(n);
    for(int i=0; i<n; ++i){
        sam[i] = urdist(mt);
    }
    //std::sort(sam.begin(), sam.end());
    timer.stop();
    timer.print_milliseconds("generating 10,000,000 draws from L(0, 1) distribution");
}
/**
 * working on the original sequence generated by the RNG is even faster.
 */
void test12(){
    mt19937 mt(11111);
    int n = 10000000;
    dclong::timer timer;
    vector<size_t> sam(n);
    for(int i=0; i<n; ++i){
        sam[i] = mt();
    }
    //std::sort(sam.begin(), sam.end());
    timer.stop();
    timer.print_milliseconds("generating 10,000,000 draws from L(0, 1) distribution");
}

int main(){
    //test1();
    //test2();
    //test3();
    //test4();
    //test5();
    //test6();
    //test7();
    //test8();
    //test9();
    //test10();
    test11();
    test12();
    return 0;
}
