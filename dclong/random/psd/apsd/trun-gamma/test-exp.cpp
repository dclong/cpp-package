
//#include "/home/dclong/Dropbox/code/cpp/cackage/util/util.hpp"
//#include "/home/dclong/Dropbox/code/cpp/cackage/string/string.hpp"
#include "dclong/random/psd/adaptive_piecewise_segment_distribution.hpp"
#include <iostream>
#include <vector>
#include <fstream>
//#include <cmath>
using namespace std;
typedef dclong::adaptive_piecewise_segment_distribution<true> apst;
typedef mt19937 RandomEngine;
struct Density {
    double _a;
    double _p;
    double _s;
    void a(double a){
        _a = a;
    }
    void p(double p){
        _p = p;
        _s = dclong::log(1-_p);
    }
    void ap(double a, double p){
        this->a(a);
        this->p(p);
    }
    bool constexpr has_derivative() const{
        return true;
    }
    double value(double x) const{
        return _s * x + _a * dclong::log(x);
    }
    double derivative(double x) const{
        return _s + _a/x;
    }
};
void test1(){
    static double inf = numeric_limits<double>::infinity();
    vector<double> x{4, 20};
    Density density;
    density.ap(0.6, 0.1);
    RandomEngine rng(1111111);
    apst p{density, x, 1, inf, 0.8};
    int n = 100000;
    vector<double> sam(n);
    agenrnd(sam.begin(), sam.end(), density, p, rng);
    ofstream ofs("trun-gamma.bin", ios::binary);
    if(ofs){
       ofs.write(reinterpret_cast<char*>(&sam[0]), sizeof(double) * n);
       ofs.close();
    }
    cout << p.to_string() << endl;
    //p1.update_state();
    //cout << p1.to_string() << endl;
}
void test2(){
    static double inf = numeric_limits<double>::infinity();
    vector<double> x{4, 20};
    uniform_real_distribution<double> urdist(0,1);
    Density density;
    density.ap(0.6, 0.1);
    RandomEngine rng(1111111);
    apst p{density, x, 1, inf, 0.8};
    int n = 100000;
    vector<double> sam(n);
    for(int i=0; i<n; ++i){
        if(urdist(rng)<0.5){
            density.ap(0.6, 0.1);
        }else{
            density.ap(0.2, 0.7);
        }
        p.segs(density, x, 1, inf);
        sam[i] = agenrnd(density, p, rng);
    }
    ofstream ofs("mix-gamma.bin", ios::binary);
    if(ofs){
       ofs.write(reinterpret_cast<char*>(&sam[0]), sizeof(double) * n);
       ofs.close();
    }
    cout << p.to_string() << endl;
    //p1.update_state();
    //cout << p1.to_string() << endl;
}
int main(){
    test1();
    return 0;
}
