
readBin('trun-gamma.bin','double',10000,8)->j
hist(j,prob=T,n=40)
x =seq(1,100,length.out=10000)
shape = 1.6
rate = -log(0.9)
y = dgamma(x, shape, rate)/pgamma(1,shape,rate,lower.tail=F)
lines(x,y,col="red")
