
plot.bounds = function(x,y,ub,lb,xlim,ylim){
    plot(x,y,type="l",col="green",xlim=xlim,ylim=ylim)
    if(!missing(ub)){
        apply(ub, 2, plot.line, col="red")
    }
    if(!missing(lb)){
        apply(lb, 2, plot.line, col="blue")
    }  
}


