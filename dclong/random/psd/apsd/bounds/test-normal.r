
test.normal=function(file,n){
    readBin(file,'double',n,8)-> j
    hist(j,n=50,prob=T,main=file)
    x=seq(-100,100,length.out=10000)
    y = dnorm(x)
    lines(x,y,col="red")
}