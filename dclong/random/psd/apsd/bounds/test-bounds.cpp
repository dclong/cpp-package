
#include "dclong/utility/utility.hpp"
#include "dclong/random/psd/adaptive_piecewise_segment_distribution.hpp"
#include <iostream>
#include <vector>
#include <fstream>
#include <random>
//#include <cmath>
using namespace std;
typedef dclong::adaptive_piecewise_segment_distribution<true> apst;
typedef mt19937 RandomEngine;
struct Density {
    double _mu;
    void mu(double mu){
        _mu = mu;
    }
    bool constexpr has_derivative() const{
        return true;
    }
    double value(double x) const{
        double xmmu = x - _mu;
        return -0.5 * xmmu * xmmu;
    }
    double derivative(double x) const{
        return _mu - x;
    }
};
void test1(){
    static double inf = numeric_limits<double>::infinity();
    vector<double> x{-10, 10};
    Density density;
    density.mu(0);
    RandomEngine rng(1111111);
    apst ub{density, x, -inf, inf, 0.8};
    apst lb{density, x, false};
    int n = 1000000;
    vector<double> sam(n);
    agenrnd(sam.begin(), sam.end(), density, ub, lb, rng);
    ofstream ofs("normal1.bin", ios::binary);
    if(ofs){
        ofs.write(reinterpret_cast<char*>(&sam[0]), sizeof(double)*sam.size());
        ofs.close();
    }
    cout << ub.to_string() << endl;
    cout << lb.to_string() << endl;
}
void test2(){
    static double inf = numeric_limits<double>::infinity();
    vector<double> x{-2, 2};
    Density density;
    RandomEngine rng(11111);
    apst ub;
    apst lb;
    int n = 100000;
    vector<double> sam(n);
    for(int i=0; i<n; ++i){
        density.mu(0);
        ub.upper_bound(density, {-2, 0, 2}, -inf, inf);
        lb.lower_bound(density, {-2, 0, 2});
        sam[i] = agenrnd(density, ub, lb, rng);
    }
    ofstream ofs("normal.bin", ios::binary);
    if(ofs){
        ofs.write(reinterpret_cast<char*>(&sam[0]), sizeof(double)*sam.size());
        ofs.close();
    }
    cout << ub.to_string() << endl;
    cout << lb.to_string() << endl;
}
int main(){
    //test1();
    test2();
    return 0;
}
