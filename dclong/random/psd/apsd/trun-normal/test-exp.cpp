
//#include "/home/dclong/Dropbox/code/cpp/cackage/util/util.hpp"
//#include "/home/dclong/Dropbox/code/cpp/cackage/string/string.hpp"
#include "dclong/random/psd/adaptive_piecewise_segment_distribution.hpp"
#include <iostream>
#include <vector>
#include <fstream>
//#include <cmath>
using namespace std;
typedef dclong::adaptive_piecewise_segment_distribution<true> apst;
typedef mt19937 RandomEngine;
struct Density {
    bool constexpr has_derivative() const{
        return true;
    }
    double value(double x) const{
        return -0.5 * x * x;
    }
    double derivative(double x) const{
        return -x;
    }
};
void test1(){
    static double inf = numeric_limits<double>::infinity();
    vector<double> x{4, 20};
    Density density;
    RandomEngine rng(1111111);
    apst p{density, x, 2, inf, 0.8};
    int n = 100000;
    vector<double> sam(n);
    agenrnd(sam.begin(), sam.end(), density, p, rng);
    ofstream ofs("trun-normal.bin", ios::binary);
    if(ofs){
       ofs.write(reinterpret_cast<char*>(&sam[0]), sizeof(double) * n);
       ofs.close();
    }
    cout << p.to_string() << endl;
    //p1.update_state();
    //cout << p1.to_string() << endl;
}
int main(){
    test1();
    return 0;
}
