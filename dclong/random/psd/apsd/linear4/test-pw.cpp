
#include "../../piecewise_segment_distribution.hpp"
#include <iostream>
#include <vector>
#include <fstream>
using namespace std;
typedef dclong::piecewise_segment_distribution<true> pst;
typedef dclong::piecewise_segment_distribution<false> psf;
typedef mt19937 RandomEngine;
void test1(){
   vector<double> x{-1, 0, 1};
   vector<double> y{0, 1, 0};
   pst p1(x, y);
   p1.update_state();
   vector<pair<double, double>> y2{make_pair(0, 2), make_pair(1, 0)};
   pst p2(x, y2);
   p2.update_state();
}

int main(){
    test1();
}
