/*
 * piecewise-segment.hpp
 *
 *  Created on: Oct 4, 2012
 *      Author: dclong
 */

#ifndef PIECEWISE_SEGMENT_DISTRIBUTION_HPP_
#define PIECEWISE_SEGMENT_DISTRIBUTION_HPP_

#include "/home/dclong/Dropbox/code/cpp/cackage/io/io.hpp"
#include "/home/dclong/Dropbox/code/cpp/cackage/math/math.hpp"
#include <vector>
#include <limits>
#include <algorithm>
#include <utility>
#include <tuple>
#include <iostream>
#include <cmath>
namespace dclong{
using namespace std;
template<bool Exp=true> class piecewise_segment {};
template<> class piecewise_segment<true>{
	private:
		uniform_real_distribution<double> udist{0, 1};
		discrete_distribution<int> ddist;
		vector<dclong::segment<true>> segs;
		double ws_ave;
		vector<double> scaled_ws;
		/**
		 * This allows more convenient constructors if the density function is continuous.
		 */
		void segment_domain(double minx, double maxx){
			const int last = segs.size() - 1;
			double xleft{minx};
			for(int i=0; i<last; ++i){
				double xright{segs[i].intersection(segs[i+1])};
				segs[i].domain(make_pair(xleft, xright));
				xleft = xright;
			}
			segs[last].domain(make_pair(xleft, maxx));
		}
		void resize(const int size){
			segs.resize(size);
			scaled_ws.resize(size);
		}
		/**
		 * Update segments.
		 * This is the case when seg is complete below segs[i].
		 * @param seg a segment whose domain is the complete domain.
		 */
		int update_segs_bb(const dclong::segment<true> & seg, int i, const vector<pair<int, int>> & comparison){
			// seg is complete under segs[i]
			// find the consecutive smallest one that is also true
			int i_lower;
			for(i_lower=i-1; i_lower>=0; --i_lower){
				if(comparison[i_lower].first<0 || comparison[i_lower].second<0){
					++ i_lower;
					break;
				}
			}
			if(i_lower > 0){
				int is_below_right = comparison[i_lower-1].second;
				if(is_below_right>0){
					// seg is below the right part of segs[i_lower-1]
					segs.erase(segs.begin() + i_lower + 1, segs.begin() + i);
					segs[i_lower] = seg;
					double x{seg.intersection(segs[i_lower - 1])};
					segs[i_lower].x1(x);
					if(i_lower + 1<segs.size()){
						segs[i_lower].x2(segs[i+1].x1());
					}
					segs[i_lower-1].x2(x);
					// segs[i_lower-1] need not to be updated again
					return i_lower - 1;
				}
				if(is_below_right<0){
					if(comparison[i_lower-1].first>0){
						// seg is over the right part of segs[i_lower - 1]
						// do thing, let segs[i_lower - 1] address itself
						return i_lower;
					}
					// seg is complete over segs[i_lower - 1], it need not to be updated
					return i_lower - 1;
				}
				// is_below_right==0
				//seg is complete over segs[i_lower - 1], it need not to be updated
				return i_lower - 1;
			}
			// i_lower == 0
			segs.erase(segs.begin() + 1, segs.begin() + i);
			segs[0] = seg;
			if(1 < segs.size()){
				segs[0].x2(segs[1].x1());
			}
			return i_lower;
		}
		int update_segs_ob(const dclong::segment<true> & seg, int i, const vector<pair<int, int>> & comparison){
			// seg is below the right part of segs[i] but over the left part of it
			// cut segs[i] into two parts
			segs.insert(segs.begin() + i + 1, seg);
			double x{segs[i+1].intersection(segs[i])};
			segs[i+1].x1(x);
			if(i + 2 < segs.size()){
				segs[i+1].x2(segs[i+2].x1());
			}
			segs[i].x2(x);
			segs[i].update_state();
			return i;
		}
		int update_segs_bo(const dclong::segment<true> & seg, const int i, const vector<pair<int, int>> & comparison){
			// seg is below the left part of segs[i] but over the right part of it
			// what to do depend on the behavior segs[i-1] if exists
			if(i>0){
				int iminus1{i - 1};
				int is_below_right{comparison[iminus1].second};
				if(is_below_right>0){
					// seg is below the right part segs[i-1]
					if(comparison[i-1].first>=0){
						// seg is also below the left part of segs[i-1]
						// only need to update the right end point of seg
						// because it will get address by segs[i-1] itself
						double x{seg.intersection(segs[i])};
						segs[i].x1(x);
						return i;
					}
					// seg is over the left part of segs[i-1]
					// need to insert an intersection, update xs[i] to be another intersection
					segs.insert(segs.begin() + i, seg);
					double x{segs[i].intersection(segs[i+1])};
					segs[i+1].x1(x);
					segs[i].x2(x);
					x = segs[i].intersection(segs[i-1]);
					segs[i].x1(x);
					segs[i-1].x2(x);
					// segs[i-1] need not to be updated again
					return i - 1;
				}
				if(is_below_right<1){
					// seg is over the right part of segs[i-1] or intersect with segs[i-1] at xs[i]
					// cut seg[i] into two parts
					segs.insert(segs.begin() + i, seg);
					double x{segs[i].intersection(segs[i+1])};
					segs[i+1].x1(x);
					segs[i].x2(x);
					return i;
				}
				// seg intersect with segs[i-1] at xs[i]
				if(comparison[i-1].first>=0){
					// seg is also below the left part of segs[i-1]
					// only need to update the right end point of seg
					// because it will get address by segs[i-1] itself
					double x{seg.intersection(segs[i])};
					segs[i].x1(x);
					return i;
				}
				// seg is complete over segs[i-1], no need to updage segs[i-1]
				return i - 1;
			}
			// i == 0, cut seg[0] into two parts
			segs.insert(segs.begin(), seg);
			double x{segs[0].intersection(segs[1])};
			segs[1].x1(x);
			segs[0].x2(x);// no need to update left point
			return i;
		}
		/**
		 * Update the upper bound segs.
		 * This method updates all segs function and ranges if necessary.
		 * And it is preferred over add_seg unless you have good reasons to add_seg.
		 * It assumes that (xs[0], xs[xs.size()-1]) is already the largest possible range.
		 * @param seg
		 */
		void update_segs(const dclong::segment<true> & seg){
			// the idea is to use lower segs to replace upper segs
			const vector<pair<int, int>> comparison{move(seg.is_lower(segs))};
			const int size = comparison.size();
			for(int i{size-1}; i>=0; --i){
				int is_below_right{comparison[i].second};
				if(is_below_right>0){
					int is_below_left{comparison[i].first};
					// seg is below the right part of segs[i]
					if(is_below_left>=0){
						// seg is completely below segs[i]
						i = update_segs_bb(seg, i, comparison);
						continue;
					}
					// seg is under the right part of segs[i] but over the left part of it
					i = update_segs_ob(seg, i, comparison);
					continue;
				}
				if(is_below_right<0){
					// seg is over the right part of segs[i]
					if(comparison[i].first>0){
						// seg is below the left part of segs[i] but over the right part of it
						i = update_segs_bo(seg, i, comparison);
						continue;
					}
					// seg is completely over segs[i]
					// do nothing
					continue;
				}
				// seg intersect with segs[i] at xs[i+1]
				if(comparison[i].first>=0){
					// seg is below segs[i] completely (might overlap)
					i = update_segs_bb(seg, i, comparison);
					continue;
				}
				// seg is over (might overlap) segs[i] completely
				// do nothing
				continue;
			}
			// resize  scaled_ws
			scaled_ws.resize(segs.size());
		}
		void insert_point(const pair<double, double> & point, const int i){
			int size = segs.size();
			if(i>=0 && i<size){
				const pair<double, double> point1{segs[i].point1()};
				const pair<double, double>  point2{segs[i].point2()};
				segs[i] = dclong::segment<true>(point, point2);
				segs.insert(segs.begin()+i, dclong::segment<true>(point1, point));
			}
			if(i==size){
				segs.push_back(dclong::segment<true>(segs[segs.size()-1].point2(), point));
			}
			segs.insert(segs.begin(), dclong::segment<true>(point, segs[0].point1()));
		}
	protected:
		void swap(piecewise_segment<true> & other){
			std::swap(ddist, other.ddist);
			std::swap(segs, other.segs);
			std::swap(ws_ave, other.ws_ave);
			std::swap(scaled_ws, other.scaled_ws);
		}
	public:
		piecewise_segment(const piecewise_segment<true> & other) :
		ddist{other.ddist}, segs{other.segs},
		ws_ave{other.ws_ave},
		scaled_ws{other.scaled_ws} {
		}

		piecewise_segment(piecewise_segment<true> && other) :
		ddist{move(other.ddist)}, segs{move(other.segs)},
		ws_ave{move(other.ws_ave)},
		scaled_ws{move(other.scaled_ws)} {
		}

		piecewise_segment<true> & operator=(const piecewise_segment<true> & other){
			piecewise_segment<true>(other).swap(*this);
			return *this;
		}

		piecewise_segment<true> & operator=(piecewise_segment<true> && other){
			piecewise_segment<true>(other).swap(*this);
			return *this;
		}
		/**
		 * A constructor that takes the x and y coordiantes as arguments.
		 * @param xs a vector of x coordinates.
		 * @param ys a vector of y coordinates.
		 */
		piecewise_segment(const vector<double> & xs,
				const vector<double> & ys, size_t capacity=20) :
		segs(std::max(xs.size() -1, capacity)), scaled_ws(segs.size()) {
			const int size = xs.size() - 1;
			resize(size);
			for(int i=0; i<size; ++i){
				segs[i] = dclong::segment<true>(make_pair(xs[i], ys[i]), make_pair(xs[i+1], ys[i+1]));
			}
		}
		/**
		 * A constructor that takes a bound of points as the argument.
		 * @param points a vector of points,
		 * where a point is a pair of x and y coordinates.
		 */
		piecewise_segment(const vector<pair<double, double>> & points, int capacity=20) :
		segs(points.size() - 1), scaled_ws(segs.size()) {
			const int size = points.size() - 1;
			resize(size);
			for(int i=0; i<size; ++i){
				segs[i] = dclong::segment<true>(points[i], points[i+1]);
			}
		}
		/**
		 * A constructor that takes x coordinates and y coordinates as arguments.
		 * @param xs a vector of x coordinates.
		 * @param ys a vector of pairs of y coordinates.
		 * The pair of y coordinates is the y value of the left and right end point
		 * of the corresponding seg defined on the intervals specified by the x coordinates.
		 * The size of ys is 1 less than the size of xs.
		 *
		 * This allows non continuous density functions.
		 */
		piecewise_segment(const vector<double> & xs,
				const vector<pair<double, double>> & ys, size_t capacity=20) :
		segs(std::max(ys.size(), capacity)), scaled_ws(segs.size()) {
			// calculate seg functions
			const int size = ys.size() - 1;
			resize(size);
			for(int i=0; i<size; ++i){
				segs[i] = dclong::segment<true>(make_pair(xs[i], ys[i].first),
						make_pair(xs[i+1], ys[i].second));
			}
		}
		/**
		 * A constructor that takes a seg functions
		 * and the domain of density as arguments.
		 * @param segs a vector of segs,
		 * where a seg is a pair of slope (a) and intercept (b).
		 * @param minx the minimum value of x.
		 * @param maxx the maximum value of x.
		 */
		piecewise_segment(const vector<dclong::line> & lines,
				double minx, double maxx, size_t capacity=20) :
		segs(std::max(capacity, lines.size())), scaled_ws(segs.size()) {
			const int size = lines.size();
			resize(size);
			for(int i=0; i<size; ++i){
				segs[i] = dclong::segment<true>(lines[i]);
			}
			segment_domain(minx, maxx);
		}
		/**
		 * A constructor that takes slopes, intercepts and the
		 * domain of density as arguments.
		 * @param as a vector of slope of segs.
		 * @param bs a vector of intercept of segs.
		 * @param minx the minimum value of x.
		 * @param maxx the maximum value of x.
		 */
		piecewise_segment(const vector<double> & as, const vector<double> & bs,
				double minx, double maxx, size_t capacity=20) :
		segs(std::max(capacity, as.size())), scaled_ws(segs.size()) {
			const int size = as.size();
			resize(size);
			for(int i=0; i<size; ++i){
				segs[i] = dclong::segment<true>(dclong::line(as[i], bs[i]));
			}
			segment_domain(minx, maxx);
		}
		/**
		 * A constructor that takes a bound of points and the
		 * domain of density as arguments.
		 * @param segs a vector of segs.
		 * Each seg is represented by (x, y, a),
		 * where (x,y) is a point on the seg and a is the slope of the seg.
		 * @param minx
		 * @param maxx
		 */
		piecewise_segment(const vector<pair<double, double>> & points, const vector<double> & as,
				double minx, double maxx, size_t capacity=20) :
		segs(std::max(capacity, as.size())), scaled_ws(segs.size()) {
			const int size = as.size();
			resize(size);
			for(int i=0; i<size; ++i){
				segs[i] = dclong::segment<true>(dclong::line(points[i], as[i]));
			}
			segment_domain(minx, maxx);
		}
		/**
		 * Set seg i.
		 * This is needed when the domain is infinite,
		 * i.e., x[0] is -inf or x[x.size() -1] is inf.
		 * @param a the slope of seg i.
		 * @param b the intercept of seg i.
		 * @param i the index of the seg to be set.
		 * You must call the function update_weights after you finished
		 * setting segs in order to get weights updated.
		 */
		inline void set_seg(dclong::line line, int i){
			segs[i].slope(line.slope());
			segs[i].slope(line.intercept());
		}

		int segs_number(){
			return segs.size();
		}
		pair<double, double> domain(){
			int size = segs.size();
			return make_pair(segs[0].x1(), segs[size-1].x2());
		}
		void update_segs(const dclong::line line){
			update_segs(dclong::segment<true>(line, domain()));
		}
		/**
		 * Add a point.
		 * This is for updating lower bound.
		 * inner intercept segs
		 */
	private:
		int which_seg(double x){
			const int i = upper_bound(segs.begin(), segs.end(), x, [](double v1, const segment<true> & seg){
				return v1 < seg.x2();
			}) - segs.begin();
			if(i>0){
				return i;
			}
			// i==0
			if(x>=segs[0].x1()){
				return 0;
			}
			return -1;
		}
	public:
		void insert_point(const pair<double, double> & point){
			insert_point(point, which_seg(point.first));
		}
//		/**
//		 * Update the upper bound segs.
//		 *
//		 * This method is very dangeous,
//		 * because it make too many assumptions,
//		 * but these assumptions might not hold in real situation.
//		 * Only use this function when all segs are tangent segs of the log density function.
//		 *
//		 * Add a seg and a cut-off point.
//		 * This is for the purpose of adaptive rejection sampling.
//		 * This method assumes that the slope of the added seg is between
//		 * the slope of its two adjacent segs,
//		 * and it only updates domain (xs) of these two segs.
//		 * If the original bound is the tangent seg of a log density function,
//		 * and the added seg is also a tangent seg,
//		 * then this method works well.
//		 * This is usually the case if the derivative of the density function is known.
//		 * @param a the slope of the new seg.
//		 * @param b the intercept of the new seg.
//		 * @param x a x coordinate value for the purpose of finding
//		 * the right position in xs and segs.
//		 * Note that x is not necessarily the cut-off point to be added
//		 * to xs. Also other elements of xs might be updated if necessary.
//		 */
//		void add_seg(const dclong::segment<true> & seg, double x){
//			const vector<double>::iterator it = upper_bound(xs.begin(), xs.end(), x);
//			if(it>xs.begin() && it<xs.end()){
//				// corresponding index in segs
//				int index = it - xs.begin() - 1;
//				double xupper{*it};
//				int is_below_at_xupper{seg.is_lower(xupper, segs[index])};
//				if(is_below_at_xupper>0){
//					++ index;
//				}
//				segs.insert(segs.begin() + index, seg);
//				// calculate the x coordinate to be added and update other x coordinates if necessary
//				double x1{segs[index].intersection(segs[index-1])};
//				// need to check that x1 is greater than xs[index-1], o.w. have to update more
//				xs[index] = x1;
//				double x2{segs[index].intersection(segs[index+1])};
//				// have to check that x2 is smaller than the element on its right, o.w. have to update more
//				xs.insert(xs.begin() + index + 1, x2);
//				return;
//			}
//			// in adaptive rejection sampling, the following two cases
//			// usually won't happen
//			// but it is supported in case it is used in other cases
//			if(it==xs.begin()){
//				// insert a seg to the begin of segs
//				segs.insert(segs.begin(), seg);
//				xs.insert(it, x);
//				// calculate the x coordinate to be added
//				double xnew{segs[0].intersection(segs[1])};
//				if(xnew <= x){
//					// wrong seg function
//					cerr << "The seg is not suitable to add in the specified range.\n";
//					exit(1);
//				}
//				xs[1] = xnew;
//				return;
//			}
//			// insert a seg to the end of segs
//			const int size = segs.size();
//			segs.insert(segs.end(), seg);
//			xs.insert(it, x);
//			// calculate the x coordinate to be added
//			double xnew{segs[size-1].intersection(segs[size])};
//			if(xnew>=x){
//				// wrong seg function
//				cerr << "The seg is not suitable to add in the specified range.\n";
//				exit(1);
//			}
//			xs[size] = xnew;
//		}
//		/**
//		 * Add a new and a cut-off point.
//		 * This is for the purpose of adaptive rejection sampling.
//		 * @param point a point on the seg to be added.
//		 * For adaptive rejection sampling purpose,
//		 * a point on the density function is usual used.
//		 * The point use the (x, y) convention.
//		 * @param a the slope of the seg to be added.
//		 * In adaptive rejection sampling,
//		 * if the point is on the density function,
//		 * this slope is derivative (if exists) of the density function
//		 * at the point.
//		 * Note that the after the seg is added,
//		 * new cut-off (not necessarily point.first) will be add to xs,
//		 * and other elements of xs might be updated if necessary.
//		 */
//		void add_seg(const pair<double, double> & point, double a){
//			add_seg(dclong::segment<true>(point, a), point.first);
//		}
//		/**
//		 * Update weights of specified segs.
//		 * Only needed when one or more segs are changed.
//		 * @param index a vector of indices of segs to be updated.
//		 */
//		void update_weights(const vector<int> & index){
//			int size = index.size();
//			for(int i=0; i<size; ++i){
//				weight(i);
//			}
//			scale_weights();
//		}
//		/**
//		 * Update weights of all segs.
//		 * Only needed when one or more segs are changed.
//		 */
//		void update_weights(){
//			weights();
//		}
		/**
		 * Generate a random number from the specified distribution.
		 */
		template<class RandomEngine> double operator()(RandomEngine & rng){
			return segs[ddist(rng)](rng);
		}
		template<class InputIt, class RandomEngine> void operator()(InputIt first, InputIt last, RandomEngine & rng){
            for(InputIt it{first}; it!=last; ++it){
                *it = operator()(rng);
            }
		}

		template<bool Log=false> double density(double x);

		template<bool Log=true> double value(double x);
		/**
		 * Must be called before you do useful things.
		 * No need to be called again if no change.
		 */
		void update_state(){
			int size = segs.size();
			for(int i=0; i<size; ++i){
				segs[i].update_state();
			}
			scale_weights();
		}
    private:
        void scale_weights(){
            // scale and recover the weights
            ws_ave = accumulate(segs.begin(), segs.end(), 0.0, [](double init, dclong::segment<true> seg){
                return init + seg.integral<true>();
            }) / segs.size();
            transform(segs.begin(), segs.end(), scaled_ws.begin(), [ws_ave=this->ws_ave](const dclong::segment<true> & seg){
                return std::exp(seg.integral<true>() - ws_ave);
            });
            ddist.param(discrete_distribution<int>::param_type(scaled_ws.begin(), scaled_ws.end()));
        }
};

template<> double piecewise_segment<true>::value<true>(double x) {
    int i = which_seg(x);
    if(i>=0 && i<segs.size()){
        return segs[i].value(x);
    }
    return -numeric_limits<double>::infinity();
}
template<> double piecewise_segment<true>::value<false>(double x) {
    int i = which_seg(x);
    if(i>=0 && i<segs.size()){
        return exp(segs[i].value(x));
    }
    return 0;
}

template<> double piecewise_segment<true>::density<true>(double x){
	return value<true>(x) - ws_ave - std::log(accumulate(scaled_ws.begin(), scaled_ws.end(), 0.0));
}
template<> double piecewise_segment<true>::density<false>(double x){
	return std::exp(density<true>(x));
}


template<> class piecewise_segment<false>{
	private:
		uniform_real_distribution<double> udist{0, 1};
		discrete_distribution<int> ddist;
		vector<dclong::segment<false>> segs;
		vector<double> scaled_ws;
		/**
		 * This allows more convenient constructors if the density function is continuous.
		 */
		void segment_domain(double minx, double maxx){
			const int last = segs.size() - 1;
			double xleft{minx};
			for(int i=0; i<last; ++i){
				double xright{segs[i].intersection(segs[i+1])};
				segs[i].domain(make_pair(xleft, xright));
				xleft = xright;
			}
			segs[last].domain(make_pair(xleft, maxx));
		}
		void resize(const int size){
			segs.resize(size);
			scaled_ws.resize(size);
		}
		/**
		 * Update segments.
		 * This is the case when seg is complete below segs[i].
		 * @param seg a segment whose domain is the complete domain.
		 */
		int update_segs_bb(const dclong::segment<false> & seg, int i, const vector<pair<int, int>> & comparison){
			// seg is complete under segs[i]
			// find the consecutive smallest one that is also true
			int i_lower;
			for(i_lower=i-1; i_lower>=0; --i_lower){
				if(comparison[i_lower].first<0 || comparison[i_lower].second<0){
					++ i_lower;
					break;
				}
			}
			if(i_lower > 0){
				int is_below_right = comparison[i_lower-1].second;
				if(is_below_right>0){
					// seg is below the right part of segs[i_lower-1]
					segs.erase(segs.begin() + i_lower + 1, segs.begin() + i);
					segs[i_lower] = seg;
					double x{seg.intersection(segs[i_lower - 1])};
					segs[i_lower].x1(x);
					if(i_lower + 1<segs.size()){
						segs[i_lower].x2(segs[i+1].x1());
					}
					segs[i_lower-1].x2(x);
					// segs[i_lower-1] need not to be updated again
					return i_lower - 1;
				}
				if(is_below_right<0){
					if(comparison[i_lower-1].first>0){
						// seg is over the right part of segs[i_lower - 1]
						// do thing, let segs[i_lower - 1] address itself
						return i_lower;
					}
					// seg is complete over segs[i_lower - 1], it need not to be updated
					return i_lower - 1;
				}
				// is_below_right==0
				//seg is complete over segs[i_lower - 1], it need not to be updated
				return i_lower - 1;
			}
			// i_lower == 0
			segs.erase(segs.begin() + 1, segs.begin() + i);
			segs[0] = seg;
			if(1 < segs.size()){
				segs[0].x2(segs[1].x1());
			}
			return i_lower;
		}
		int update_segs_ob(const dclong::segment<false> & seg, int i, const vector<pair<int, int>> & comparison){
			// seg is below the right part of segs[i] but over the left part of it
			// cut segs[i] into two parts
			segs.insert(segs.begin() + i + 1, seg);
			double x{segs[i+1].intersection(segs[i])};
			segs[i+1].x1(x);
			if(i + 2 < segs.size()){
				segs[i+1].x2(segs[i+2].x1());
			}
			segs[i].x2(x);
			segs[i].update_state();
			return i;
		}
		int update_segs_bo(const dclong::segment<false> & seg, const int i, const vector<pair<int, int>> & comparison){
			// seg is below the left part of segs[i] but over the right part of it
			// what to do depend on the behavior segs[i-1] if exists
			if(i>0){
				int iminus1{i - 1};
				int is_below_right{comparison[iminus1].second};
				if(is_below_right>0){
					// seg is below the right part segs[i-1]
					if(comparison[i-1].first>=0){
						// seg is also below the left part of segs[i-1]
						// only need to update the right end point of seg
						// because it will get address by segs[i-1] itself
						double x{seg.intersection(segs[i])};
						segs[i].x1(x);
						return i;
					}
					// seg is over the left part of segs[i-1]
					// need to insert an intersection, update xs[i] to be another intersection
					segs.insert(segs.begin() + i, seg);
					double x{segs[i].intersection(segs[i+1])};
					segs[i+1].x1(x);
					segs[i].x2(x);
					x = segs[i].intersection(segs[i-1]);
					segs[i].x1(x);
					segs[i-1].x2(x);
					// segs[i-1] need not to be updated again
					return i - 1;
				}
				if(is_below_right<1){
					// seg is over the right part of segs[i-1] or intersect with segs[i-1] at xs[i]
					// cut seg[i] into two parts
					segs.insert(segs.begin() + i, seg);
					double x{segs[i].intersection(segs[i+1])};
					segs[i+1].x1(x);
					segs[i].x2(x);
					return i;
				}
				// seg intersect with segs[i-1] at xs[i]
				if(comparison[i-1].first>=0){
					// seg is also below the left part of segs[i-1]
					// only need to update the right end point of seg
					// because it will get address by segs[i-1] itself
					double x{seg.intersection(segs[i])};
					segs[i].x1(x);
					return i;
				}
				// seg is complete over segs[i-1], no need to updage segs[i-1]
				return i - 1;
			}
			// i == 0, cut seg[0] into two parts
			segs.insert(segs.begin(), seg);
			double x{segs[0].intersection(segs[1])};
			segs[1].x1(x);
			segs[0].x2(x);// no need to update left point
			return i;
		}
		/**
		 * Update the upper bound segs.
		 * This method updates all segs function and ranges if necessary.
		 * And it is preferred over add_seg unless you have good reasons to add_seg.
		 * It assumes that (xs[0], xs[xs.size()-1]) is already the largest possible range.
		 * @param seg
		 */
		void update_segs(const dclong::segment<false> & seg){
			// the idea is to use lower segs to replace upper segs
			const vector<pair<int, int>> comparison{move(seg.is_lower(segs))};
			const int size = comparison.size();
			for(int i{size-1}; i>=0; --i){
				int is_below_right{comparison[i].second};
				if(is_below_right>0){
					int is_below_left{comparison[i].first};
					// seg is below the right part of segs[i]
					if(is_below_left>=0){
						// seg is completely below segs[i]
						i = update_segs_bb(seg, i, comparison);
						continue;
					}
					// seg is under the right part of segs[i] but over the left part of it
					i = update_segs_ob(seg, i, comparison);
					continue;
				}
				if(is_below_right<0){
					// seg is over the right part of segs[i]
					if(comparison[i].first>0){
						// seg is below the left part of segs[i] but over the right part of it
						i = update_segs_bo(seg, i, comparison);
						continue;
					}
					// seg is completely over segs[i]
					// do nothing
					continue;
				}
				// seg intersect with segs[i] at xs[i+1]
				if(comparison[i].first>=0){
					// seg is below segs[i] completely (might overlap)
					i = update_segs_bb(seg, i, comparison);
					continue;
				}
				// seg is over (might overlap) segs[i] completely
				// do nothing
				continue;
			}
			// resize  scaled_ws
			scaled_ws.resize(segs.size());
		}
		void insert_point(const pair<double, double> & point, const int i){
			int size = segs.size();
			if(i>=0 && i<size){
				const pair<double, double> point1{segs[i].point1()};
				const pair<double, double> point2{segs[i].point2()};
				segs[i] = dclong::segment<false>(point, point2);
				segs.insert(segs.begin()+i, dclong::segment<false>(point1, point));
			}
			if(i==size){
				segs.push_back(dclong::segment<false>(segs[segs.size()-1].point2(), point));
			}
			segs.insert(segs.begin(), dclong::segment<false>(point, segs[0].point1()));
		}
	protected:
		void swap(piecewise_segment<false> & other){
			std::swap(ddist, other.ddist);
			std::swap(segs, other.segs);
			std::swap(scaled_ws, other.scaled_ws);
		}
	public:
		piecewise_segment(const piecewise_segment<false> & other) :
		ddist{other.ddist}, segs{other.segs},
		scaled_ws{other.scaled_ws} {
		}

		piecewise_segment(piecewise_segment<false> && other) :
		ddist{move(other.ddist)}, segs{move(other.segs)},
		scaled_ws{move(other.scaled_ws)} {
		}

		piecewise_segment<false> & operator=(const piecewise_segment<false> & other){
			piecewise_segment<false>(other).swap(*this);
			return *this;
		}

		piecewise_segment<false> & operator=(piecewise_segment<false> && other){
			piecewise_segment<false>(other).swap(*this);
			return *this;
		}
		/**
		 * A constructor that takes the x and y coordiantes as arguments.
		 * @param xs a vector of x coordinates.
		 * @param ys a vector of y coordinates.
		 */
		piecewise_segment(const vector<double> & xs,
				const vector<double> & ys, size_t capacity=20) :
		segs(std::max(xs.size() -1, capacity)), scaled_ws(segs.size()) {
			const int size = xs.size() - 1;
			resize(size);
			for(int i=0; i<size; ++i){
				segs[i] = dclong::segment<false>(make_pair(xs[i], ys[i]), make_pair(xs[i+1], ys[i+1]));
			}
		}
		/**
		 * A constructor that takes a bound of points as the argument.
		 * @param points a vector of points,
		 * where a point is a pair of x and y coordinates.
		 */
		piecewise_segment(const vector<pair<double, double>> & points, int capacity=20) :
		segs(points.size() - 1), scaled_ws(segs.size()) {
			const int size = points.size() - 1;
			resize(size);
			for(int i=0; i<size; ++i){
				segs[i] = dclong::segment<false>(points[i], points[i+1]);
			}
		}
		/**
		 * A constructor that takes x coordinates and y coordinates as arguments.
		 * @param xs a vector of x coordinates.
		 * @param ys a vector of pairs of y coordinates.
		 * The pair of y coordinates is the y value of the left and right end point
		 * of the corresponding seg defined on the intervals specified by the x coordinates.
		 * The size of ys is 1 less than the size of xs.
		 *
		 * This allows non continuous density functions.
		 */
		piecewise_segment(const vector<double> & xs,
				const vector<pair<double, double>> & ys, size_t capacity=20) :
		segs(std::max(ys.size(), capacity)), scaled_ws(segs.size()) {
			// calculate seg functions
			const int size = ys.size() - 1;
			resize(size);
			for(int i=0; i<size; ++i){
				segs[i] = dclong::segment<false>(make_pair(xs[i], ys[i].first),
						make_pair(xs[i+1], ys[i].second));
			}
		}
		/**
		 * A constructor that takes a seg functions
		 * and the domain of density as arguments.
		 * @param segs a vector of segs,
		 * where a seg is a pair of slope (a) and intercept (b).
		 * @param minx the minimum value of x.
		 * @param maxx the maximum value of x.
		 */
		piecewise_segment(const vector<dclong::line> & lines,
				double minx, double maxx, size_t capacity=20) :
		segs(std::max(capacity, lines.size())), scaled_ws(segs.size()) {
			const int size = lines.size();
			resize(size);
			for(int i=0; i<size; ++i){
				segs[i] = dclong::segment<false>(lines[i]);
			}
			segment_domain(minx, maxx);
		}
		/**
		 * A constructor that takes slopes, intercepts and the
		 * domain of density as arguments.
		 * @param as a vector of slope of segs.
		 * @param bs a vector of intercept of segs.
		 * @param minx the minimum value of x.
		 * @param maxx the maximum value of x.
		 */
		piecewise_segment(const vector<double> & as, const vector<double> & bs,
				double minx, double maxx, size_t capacity=20) :
		segs(std::max(capacity, as.size())), scaled_ws(segs.size()) {
			const int size = as.size();
			resize(size);
			for(int i=0; i<size; ++i){
				segs[i] = dclong::segment<false>(dclong::line(as[i], bs[i]));
			}
			segment_domain(minx, maxx);
		}
		/**
		 * A constructor that takes a bound of points and the
		 * domain of density as arguments.
		 * @param segs a vector of segs.
		 * Each seg is represented by (x, y, a),
		 * where (x,y) is a point on the seg and a is the slope of the seg.
		 * @param minx
		 * @param maxx
		 */
		piecewise_segment(const vector<pair<double, double>> & points, const vector<double> & as,
				double minx, double maxx, size_t capacity=20) :
		segs(std::max(capacity, as.size())), scaled_ws(segs.size()) {
			const int size = as.size();
			resize(size);
			for(int i=0; i<size; ++i){
				segs[i] = dclong::segment<false>(dclong::line(points[i], as[i]));
			}
			segment_domain(minx, maxx);
		}
		/**
		 * Set seg i.
		 * This is needed when the domain is infinite,
		 * i.e., x[0] is -inf or x[x.size() -1] is inf.
		 * @param a the slope of seg i.
		 * @param b the intercept of seg i.
		 * @param i the index of the seg to be set.
		 * You must call the function update_weights after you finished
		 * setting segs in order to get weights updated.
		 */
		inline void set_seg(dclong::line line, int i){
			segs[i].slope(line.slope());
			segs[i].intercept(line.intercept());
		}

		int segs_number(){
			return segs.size();
		}
		pair<double, double> domain(){
			int size = segs.size();
			return make_pair(segs[0].x1(), segs[size-1].x2());
		}
		void update_segs(const dclong::line line){
			update_segs(dclong::segment<false>(line, domain()));
		}
		/**
		 * Add a point.
		 * This is for updating lower bound.
		 * inner intercept segs
		 */
	private:
		int which_seg(double x){
			const int i = upper_bound(segs.begin(), segs.end(), x, [](double v1, const segment<false> & seg){
				return v1 < seg.x2();
			}) - segs.begin();
			if(i>0){
				return i;
			}
			// i==0
			if(x>=segs[0].x1()){
				return 0;
			}
			return -1;
		}
	public:
		void insert_point(const pair<double, double> & point){
			insert_point(point, which_seg(point.first));
		}
//		/**
//		 * Update the upper bound segs.
//		 *
//		 * This method is very dangeous,
//		 * because it make too many assumptions,
//		 * but these assumptions might not hold in real situation.
//		 * Only use this function when all segs are tangent segs of the log density function.
//		 *
//		 * Add a seg and a cut-off point.
//		 * This is for the purpose of adaptive rejection sampling.
//		 * This method assumes that the slope of the added seg is between
//		 * the slope of its two adjacent segs,
//		 * and it only updates domain (xs) of these two segs.
//		 * If the original bound is the tangent seg of a log density function,
//		 * and the added seg is also a tangent seg,
//		 * then this method works well.
//		 * This is usually the case if the derivative of the density function is known.
//		 * @param a the slope of the new seg.
//		 * @param b the intercept of the new seg.
//		 * @param x a x coordinate value for the purpose of finding
//		 * the right position in xs and segs.
//		 * Note that x is not necessarily the cut-off point to be added
//		 * to xs. Also other elements of xs might be updated if necessary.
//		 */
//		void add_seg(const dclong::segment<false> & seg, double x){
//			const vector<double>::iterator it = upper_bound(xs.begin(), xs.end(), x);
//			if(it>xs.begin() && it<xs.end()){
//				// corresponding index in segs
//				int index = it - xs.begin() - 1;
//				double xupper{*it};
//				int is_below_at_xupper{seg.is_lower(xupper, segs[index])};
//				if(is_below_at_xupper>0){
//					++ index;
//				}
//				segs.insert(segs.begin() + index, seg);
//				// calculate the x coordinate to be added and update other x coordinates if necessary
//				double x1{segs[index].intersection(segs[index-1])};
//				// need to check that x1 is greater than xs[index-1], o.w. have to update more
//				xs[index] = x1;
//				double x2{segs[index].intersection(segs[index+1])};
//				// have to check that x2 is smaller than the element on its right, o.w. have to update more
//				xs.insert(xs.begin() + index + 1, x2);
//				return;
//			}
//			// in adaptive rejection sampling, the following two cases
//			// usually won't happen
//			// but it is supported in case it is used in other cases
//			if(it==xs.begin()){
//				// insert a seg to the begin of segs
//				segs.insert(segs.begin(), seg);
//				xs.insert(it, x);
//				// calculate the x coordinate to be added
//				double xnew{segs[0].intersection(segs[1])};
//				if(xnew <= x){
//					// wrong seg function
//					cerr << "The seg is not suitable to add in the specified range.\n";
//					exit(1);
//				}
//				xs[1] = xnew;
//				return;
//			}
//			// insert a seg to the end of segs
//			const int size = segs.size();
//			segs.insert(segs.end(), seg);
//			xs.insert(it, x);
//			// calculate the x coordinate to be added
//			double xnew{segs[size-1].intersection(segs[size])};
//			if(xnew>=x){
//				// wrong seg function
//				cerr << "The seg is not suitable to add in the specified range.\n";
//				exit(1);
//			}
//			xs[size] = xnew;
//		}
//		/**
//		 * Add a new and a cut-off point.
//		 * This is for the purpose of adaptive rejection sampling.
//		 * @param point a point on the seg to be added.
//		 * For adaptive rejection sampling purpose,
//		 * a point on the density function is usual used.
//		 * The point use the (x, y) convention.
//		 * @param a the slope of the seg to be added.
//		 * In adaptive rejection sampling,
//		 * if the point is on the density function,
//		 * this slope is derivative (if exists) of the density function
//		 * at the point.
//		 * Note that the after the seg is added,
//		 * new cut-off (not necessarily point.first) will be add to xs,
//		 * and other elements of xs might be updated if necessary.
//		 */
//		void add_seg(const pair<double, double> & point, double a){
//			add_seg(dclong::segment<false>(point, a), point.first);
//		}
//		/**
//		 * Update weights of specified segs.
//		 * Only needed when one or more segs are changed.
//		 * @param index a vector of indices of segs to be updated.
//		 */
//		void update_weights(const vector<int> & index){
//			int size = index.size();
//			for(int i=0; i<size; ++i){
//				weight(i);
//			}
//			scale_weights();
//		}
//		/**
//		 * Update weights of all segs.
//		 * Only needed when one or more segs are changed.
//		 */
//		void update_weights(){
//			weights();
//		}
		/**
		 * Generate a random number from the specified distribution.
		 */
		template<class RandomEngine> double operator()(RandomEngine & rng){
			return segs[ddist(rng)](rng);
		}
		template<class InputIt, class RandomEngine> void operator()(InputIt first, InputIt last, RandomEngine & rng){
            for(InputIt it{first}; it!=last; ++it){
                *it = operator()(rng);
            }
		}

		template<bool Log=false> double density(double x);

		template<bool Log=false> double value(double x);
		/**
		 * Must be called before you do useful things.
		 * No need to be called again if no change.
		 */
		void update_state(){
			int size = segs.size();
			for(int i=0; i<size; ++i){
				segs[i].update_state();
			}
			scale_weights();
		}
    private:
        void scale_weights(){
            transform(segs.begin(), segs.end(), scaled_ws.begin(), [](const dclong::segment<false> & seg){
                return seg.integral<false>();
            });
            ddist.param(discrete_distribution<int>::param_type(scaled_ws.begin(), scaled_ws.end()));
        }
};
/**
 * the order here matters ... !!
 */
template<> double piecewise_segment<false>::value<false>(double x) {
	int i = which_seg(x);
	if(i>=0 && i<segs.size()){
		return segs[i].value(x);
	}
	return 0;
}
template<> double piecewise_segment<false>::value<true>(double x) {
	int i = which_seg(x);
	if(i>=0 && i<segs.size()){
		return std::log(segs[i].value(x));
	}
	return -numeric_limits<double>::infinity();
}
template<> double piecewise_segment<false>::density<false>(double x){
	return value(x) / accumulate(segs.begin(), segs.end(), 0.0, [](double init, const dclong::segment<false> & seg){
		return init + seg.integral<false>();
	});
}
template<> double piecewise_segment<false>::density<true>(double x){
	return std::log(value(x) / accumulate(segs.begin(), segs.end(), 0.0, [](double init, const dclong::segment<false> & seg){
		return init + seg.integral<false>();
	}));
}

}


#endif /* PIECEWISE_SEGMENT_DISTRIBUTION_HPP_ */
