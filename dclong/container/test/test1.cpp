

#include "dclong/string/string.hpp"
#include "dclong/container/container.hpp"
#include <vector>
#include <iostream>
using namespace std;

void test1(){
	auto x = dclong::vector(3,10,1);
	cout << dclong::to_string(x.begin(), x.end()) << endl;
    cout << x.size() << " " << x.capacity() << endl;
}

int main(){
    test1();
}
