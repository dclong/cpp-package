/*
 * container.hpp
 *
 *  Created on: Oct 21, 2012
 *      Author: dclong
 */

#ifndef CONTAINER_HPP_
#define CONTAINER_HPP_

#include <vector>

namespace dclong {
using namespace std;

//enum class container : signed char {
//	vector, list, set, map
//};


template<class T> vector<T> vector(int size, int capacity, T value = T()){
	std::vector<T> x(capacity, value);
	x.resize(size);
	return x;
}

//template<class T> vector<T> vector(int size, int capacity){
//	std::vector<T> x(capacity, T());
//	x.resize(size);
//	return x;
//}

}


#endif /* CONTAINER_HPP_ */
