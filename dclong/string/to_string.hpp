/*
 * string.hpp
 *
 *  Created on: Oct 6, 2012
 *      Author: dclong
 */

#ifndef DCLONG_TO_STRING_HPP_
#define DCLONG_TO_STRING_HPP_
#include <string>
#include <iterator>
#include <tuple>
namespace dclong {
using namespace std;
using std::to_string;
// it is better to use template than use function pointer here ...
// mainly becuase lambda function cannot be used as function pointer currently
// in future this might be supported
// another way is to use std::function,
// a most convenient way is to use tempalate ..., the cons is slow to compile ...
//typedef template<class InputIt> string (*ToStringFun)(iterator_traits<InputIt>::value_type);
string to_string(const string & s){
	return s;
}


template<class T, class ToStringFun> string to_string(const pair<T, T> & p, ToStringFun tsf,
		const string & prefix, const string & sufix){
	return prefix + tsf(p.first) + ", " + tsf(p.second) + sufix;
}

template<class T> string to_string(const pair<T, T> & p, const string & prefix, const string & sufix){
	return to_string(p, [](const T & x){
		return to_string(x);
	}, prefix, sufix);
}

template<class T, class ToStringFun> string to_string(const pair<T, T> & p, ToStringFun tsf){
	return to_string(p, tsf, "(", ")");
}

template<class T> string to_string(const pair<T, T> & p){
	return to_string(p, "(", ")");
}

template<class T, class ToStringFun> string to_string(const tuple<T, T, T> & t, ToStringFun tsf,
		const string & prefix = "(", const string & sufix = ")"){
	return prefix + tsf(get<0>(t)) + ", " + tsf(get<1>(t)) + ", " + tsf(get<2>(t)) + sufix;
}

//template<class T> string to_string(const tuple<T, T, T> & t, const string & prefix = "(", const string & sufix = ")"){
//	return to_string(t, [](const T & x){
//		return to_string(x);
//	}, prefix, sufix);
//}

//template<class T, class ToStringFun> string to_string(T t, ToStringFun tsf, const string & sep){
//	return dclong::to_string(t.begin(), t.end(), tsf, sep);
//}
//
//template<class T, class ToStringFun> string to_string(T t, ToStringFun tsf){
//	return dclong::to_string(t.begin(), t.end(), tsf);
//}
//
//template<class T> string to_string(T t, const string & sep){
//	return dclong::to_string(t.begin(), t.end(), sep);
//}
//
//template<class T> string to_string(T t){
//	return dclong::to_string(t.begin(), t.end());
//}

template<class InputIt, class ToStringFun> string to_string(InputIt first, InputIt last,
		ToStringFun tsf, const string & sep){
	string s(tsf(*first));
	for(InputIt it(++first); it!=last; ++it){
		s += sep + tsf(*it);
	}
	return s;
}

template<class InputIt, class ToStringFun> string to_string(InputIt first, InputIt last, ToStringFun tsf){
	return to_string(first, last, tsf, " ");
}

template<class InputIt> string to_string(InputIt first, InputIt last, const string & sep){
	return to_string(first, last, [](const typename iterator_traits<InputIt>::value_type & x){
		return to_string(x);
	}, sep);
}

template<class InputIt> string to_string(InputIt first, InputIt last){
	return to_string(first, last, [](const typename iterator_traits<InputIt>::value_type & x){
		return to_string(x);
	}, " ");
}
// ---------------------- another version of implementation ----------------------------------
//template<class InputIt> string to_string(InputIt first, InputIt last,
//		string (*tsf)(const typename iterator_traits<InputIt>::value_type &), const string & sep){
//	string s(tsf(*first));
//	for(InputIt it(++first); it!=last; ++it){
//		s += sep + tsf(*it);
//	}
//	return s;
//}
//
//template<class InputIt> string to_string(InputIt first, InputIt last,
//		string (*tsf)(const typename iterator_traits<InputIt>::value_type &)){
//	return to_string(first, last, tsf, " ");
//}
//
//template<class InputIt> string to_string(InputIt first, InputIt last, string sep){
//	return to_string(first, last, [](typename iterator_traits<InputIt>::value_type x){
//		return to_string(x);
//	}, sep);
//}
//
//template<class InputIt> string to_string(InputIt first, InputIt last){
//	return to_string(first, last, [](typename iterator_traits<InputIt>::value_type x){
//		return to_string(x);
//	}, " ");
//}

}
#endif /* STRING_HPP_ */
