/*
 * test-string.cpp
 *
 *  Created on: Oct 6, 2012
 *      Author: dclong
 */
#include "dclong/string/string.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <utility>
using namespace std;
void test1(){
//	string s = "abc";
//	std::cout << dclong::to_string(s) << std::endl;
	vector<string> vs{"abc", "def", "ghi"};
	//cout << dclong::to_string(vs.begin(), vs.end(), [](const string & s){return s;}, " ") << endl;
	cout << dclong::to_string(vs.begin(), vs.end()) << endl;
	vector<int> xs{0, 1, 2, 3};
	cout << dclong::to_string(xs.begin(), xs.end()) << endl;
//	cout << dclong::to_string(xs.begin(), xs.end(), [](double x){return to_string(x);}, " ");
}

/**
void test2(){
	pair<double, double> p(1,2);
	//cout << dclong::to_string(p, [](double x){return to_string(x);}, "(",")") << endl;
	cout << dclong::to_string(p, "(", ")") << endl;
}
**/
int main(){
    test1();
}
