/*
 * test1.cpp
 *
 *  Created on: Nov 8, 2012
 *      Author: dclong
 */


#include "dclong/armadillo/armadillo.hpp"
#include <armadillo>

void test1(){
	arma::mat m1 = arma::zeros<arma::mat>(3,2);
	arma::mat m2 = arma::ones(3,3);
	arma::mat m3 = arma::zeros<arma::mat>(3,4);
	arma::mat m = arma::join_rows(m1, m2, m3);
	m.print();
}

int main(){
	test1();
}
