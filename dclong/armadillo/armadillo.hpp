/*
 * armadillo.hpp
 *
 *  Created on: Nov 8, 2012
 *      Author: dclong
 */

#ifndef ARMADILLO_HPP_
#define ARMADILLO_HPP_

#include "dclong/algorithm.hpp"
#include <armadillo>
namespace arma {

	template<typename T, typename... Args>
	void join_rows_helper(arma::Mat<T> & des_mat, int first, const arma::Mat<T> & m){
		int last = first + m.n_cols;
		des_mat.cols(first, last - 1) = m;
	}

	template<typename T, typename... Args>
	void join_rows_helper(arma::Mat<T> & des_mat,
					int first, const arma::Mat<T> & m, Args... args){
		int last = first + m.n_cols;
		des_mat.cols(first, last - 1) = m;
		join_rows_helper(des_mat, last, args...);
	}

	template<typename T, typename... Args> arma::Mat<T> join_rows(const arma::Mat<T> & m, Args... args){
		long n_cols = dclong::accumulate_f([](long x, const arma::Mat<T> & m){return x + m.n_cols;}, 0L, m, args...);
		arma::Mat<T> des_mat(m.n_rows, n_cols);
		join_rows_helper(des_mat, 0, m, args...);
		return des_mat;
	}
}



#endif /* ARMADILLO_HPP_ */
