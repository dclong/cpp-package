/*
 * util.h
 *
 *  Created on: Aug 20, 2012
 *      Author: dclong
 */

#ifndef DCLONG_TIMER_H_
#define DCLONG_TIMER_H_
#include<chrono>
#include<string>
#include<iostream>
namespace dclong {
class timer {
private:
	std::chrono::time_point<std::chrono::system_clock> starting_time;
	std::chrono::time_point<std::chrono::system_clock> ending_time;
	long elapsed_time;
	void swap(timer other){// to be finished and add move constructor and so on ...

	}
public:
	timer(){
		start();
	}
	void start() {
		starting_time = std::chrono::system_clock::now();
	}
	void end() {
		ending_time = std::chrono::system_clock::now();
		elapsed_time = (ending_time - starting_time).count();
	}
	void print_milliseconds(std::string job) {
		std::cout<< "The time used for "<<job<<" is "<<milliseconds()<<" milliseconds."<<std::endl;
	}
	void print_seconds(std::string job) {
		std::cout << "The time used for "<<job<<" is "<<seconds()<<" seconds."<<std::endl;
	}
	void print_minutes(std::string job) {
		std::cout<< "The time used for "<<job<<" is "<<minutes()<<" minutes."<<std::endl;
	}
	void print_hours(std::string job) {
		std::cout << "The time used for "<<job<<" is "<<hours()<<" hours."<<std::endl;
	}
	void print_days(std::string job) {
		std::cout << "The time used for "<<job<<" is "<<days()<<" days."<<std::endl;
	}

	void begin() {
		start();
	}
	void stop() {
		end();
	}
	void reset(){
		start();
	}
	double nanoseconds(){
		return elapsed_time;
	}
	double microseconds(){
		return nanoseconds() / 1000.0;
	}
	double milliseconds() {
		return microseconds() /1000.0;
	}
	double seconds() {
		return milliseconds() / 1000.0;
	}
	double minutes() {
		return seconds() / 60;
	}
	double hours() {
		return minutes() / 60;
	}
	double days() {
		return hours() / 24;
	}
};
}

#endif /* UTIL_H_ */
