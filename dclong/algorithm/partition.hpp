/*
 * partition.h
 *
 *  Created on: Aug 25, 2012
 *      Author: dclong
 */

#ifndef DCLONG_PARTITION_H_
#define DCLONG_PARTITION_H_

#include <unordered_set>
#include <algorithm>
#include <cstddef>

namespace dclong{
	/**
	 * Returns partition of iterators.
	 */
	template<class InputIt, class ClassValue> std::unordered_set<std::unordered_set<InputIt>> 
    partition(const InputIt & first, const InputIt & last, ClassValue cv){
        //---------------------------------------------------------------------
		typedef decltype(cv(*first)) value_type;
		std::unordered_set<value_type> exist;
		std::unordered_set<std::unordered_set<InputIt>> p;
		for(InputIt i=first; i<last; ++i){
			value_type c = cv(*i);
			auto loc = std::find(std::begin(exist), std::end(exist), c);
			if(loc < std::end(exist)){
                p[loc - std::begin(exist)].push_back(i);
				continue;
			}
            // no existing class, create one for it
            p.push_back({i});
            exist.push_back(c);
		}
		return p;
	}
	template<class InputIt> std::unordered_set<std::unordered_set<InputIt>>
	partition(const InputIt & first, const InputIt & last){
        typedef typename InputIt::value_type value_type;
        std::unordered_set<value_type> exist;
        std::unordered_set<std::unordered_set<InputIt>> p;
        for(InputIt i=first; i<last; ++i){
            auto loc = std::find(std::begin(exist), std::end(exist),*i);
            if(loc < std::end(exist)){
                p[loc - std::begin(exist)].push_back(i);
                continue;
            }
            // no existing class, create one for it
            p.push_back({i});
            exist.push_back(*i);
        }
        return p;
	}
	/**
	 * Return partition of indexes.
	 */
	template<class InputIt, class ClassValue> std::unordered_set<std::unordered_set<std::ptrdiff_t>>
    partition_i(const InputIt & first, const InputIt & last, ClassValue cv){
        //----------------------------------------------------------------------
        typedef decltype(cv(*first)) value_type;
        std::unordered_set<value_type> exist;
        std::unordered_set<std::unordered_set<std::ptrdiff_t>> p;
        for(InputIt i=first; i<last; ++i){
            value_type c = cv(*i);
            auto loc = std::find(std::begin(exist), std::end(exist),c);
            if(loc < std::end(exist)){
                p[loc - std::begin(exist)].push_back(i - first);
                continue;
            }
            // no existing class, create one for it
            p.push_back({i - first});
            exist.push_back(c);
        }
        return p;
    }

//	template<class InputIt> std::unordered_set<std::unordered_set<std::ptrdiff_t>>
//	partition_i(const InputIt & first, const InputIt & last){
//        typedef typename InputIt::value_type value_type;
//        std::unordered_set<value_type> exist;
//        std::unordered_set<std::unordered_set<std::ptrdiff_t>> p;
//        for(InputIt i=first; i<last; ++i){
//            auto loc = std::find(std::begin(exist), std::end(exist),*i);
//            if(loc < std::end(exist)){
//                p[loc - std::begin(exist)].push_back(i - first);
//                continue;
//            }
//            // no existing class, create one for it
//            p.push_back({i - first});
//            exist.push_back(*i);
//        }
//        return p;
//    }

	template<class InputIt> std::unordered_map<typename InputIt::value_type, std::unordered_set<std::ptrdiff_t>>
	partition_i(const InputIt & first, const InputIt & last){
	        typedef typename InputIt::value_type value_type;
	        std::unordered_map<value_type, std::unordered_set<std::ptrdiff_t>> p;
	        for(InputIt i(first); i<last; ++i){
	        	p[*i].insert(std::distance(first, i));
	        }
	        return p;
	}
}


#endif /* PARTITION_H_ */
