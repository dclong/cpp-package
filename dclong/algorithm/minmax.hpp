/*
 * max.hpp
 *
 *  Created on: Nov 11, 2012
 *      Author: dclong
 */

#ifndef DCLONG_MAX_HPP_
#define DCLONG_MAX_HPP_

#include <functional>

namespace dclong {
template<typename Compare, typename T>
T max_f(Compare comp, const T & x1, const T & x2) {
	return comp(x1, x2)?x2:x1;
}
// the benefit of using an extra initial value is to control the
// type of return value
template<typename Compare, typename T, typename ... Args>
T max_f(Compare comp, const T & x1, const T & x2, Args ... args) {
	return max_f(comp, comp(x1, x2)?x2:x1, args...);
}
template<typename T, typename ... Args>
T max(T x1, T x2, Args ... args) {
	return max_f(std::less<T>(), x1, x2, args...);
}

template<typename Compare, typename T>
T min_f(Compare comp, const T & x1, const T & x2) {
	return comp(x1, x2)?x1:x2;
}
// the benefit of using an extra initial value is to control the
// type of return value
template<typename Compare, typename T, typename ... Args>
T min_f(Compare comp, const T & x1, const T & x2, Args ... args) {
	return min_f(comp, comp(x1, x2)?x1:x2, args...);
}
template<typename T, typename ... Args>
T min(T x1, T x2, Args ... args) {
	return min_f(std::less<T>(), x1, x2, args...);
}
}


#endif /* DCLONG_MAX_HPP_ */
