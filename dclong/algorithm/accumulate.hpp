/*
 * accumulate.hpp
 *
 *  Created on: Nov 8, 2012
 *      Author: dclong
 */

#ifndef DCLONG_ACCUMULATE_HPP_
#define DCLONG_ACCUMULATE_HPP_

namespace dclong {
template<typename BinaryOperation, typename InitType, typename T>
InitType accumulate_f(BinaryOperation bop, InitType init, const T & x) {
	return bop(init, x);
}
// the benefit of using an extra initial value is to control the
// type of return value
template<typename BinaryOperation, typename InitType, typename T, typename ... Args>
InitType accumulate_f(BinaryOperation bop, InitType init, const T & x, Args ... args) {
	return accumulate_f(bop, bop(init, x), args...);
}
template<typename InitType, typename T, typename ... Args>
InitType accumulate(InitType init, const T & x, Args ... args) {
	return accumulate_f([](InitType x1, InitType x2) {return x1 + x2;}, init, x, args...);
}
}

#endif /* ACCUMULATE_HPP_ */
