#ifndef ALGORITHM_HPP_
#define ALGORITHM_HPP_

#include "dclong/container/container.hpp"
#include <algorithm>
#include <iterator>
#include <vector>
#include <set>
namespace dclong {
using namespace std;

template<class InputIt1, class InputIt2, template<class> class Container = set>
				Container<typename InputIt1::value_type> set_union(InputIt1 first1, InputIt1 last1,
				InputIt2 first2, InputIt2 last2) {
	Container<typename InputIt1::value_type> c;
	std::set_union(first1, last1, first2, last2, std::inserter(c, c.end()));
	return c;
}
/**
template<class InputIt1, class InputIt2, template<class> class Container = set, class Compare>
Container<typename InputIt1::value_type> set_union(InputIt1 first1, InputIt1 last1,
				InputIt2 first2, InputIt2 last2, Compare comp) {
	Container<typename InputIt1::value_type> c;
	std::set_union(first1, last1, first2, last2, std::inserter(c, c.end()), comp);
	return c;
}
template<template<class> class Container = set, class T> Container<T>
set_union(Container<T> s1, Container<T> s2) {
	return dclong::set_union<typename set<T>::iterator, typename set<T>::iterator,
					Container>(s1.begin(), s1.end(), s2.begin(), s2.end());
}
template<template<class> class Container = set, class T, class Compare> Container<T>
set_union(Container<T> s1, Container<T> s2, Compare comp) {
	return dclong::set_union<typename set<T>::iterator, typename set<T>::iterator, Container>(s1.begin(), s1.end(), s2.begin(), s2.end(), comp);
}
template<class InputIt1, class InputIt2, template<class> class Container = set>
				Container<typename InputIt1::value_type> set_intersection(InputIt1 first1, InputIt1 last1,
				InputIt2 first2, InputIt2 last2) {
	Container<typename InputIt1::value_type> c;
	std::set_intersection(first1, last1, first2, last2, std::inserter(c, c.end()));
	return c;
}

template<class InputIt1, class InputIt2, template<class> class Container = set, class Compare>
				Container<typename InputIt1::value_type> set_intersection(InputIt1 first1, InputIt1 last1,
				InputIt2 first2, InputIt2 last2, Compare comp) {
	Container<typename InputIt1::value_type> c;
	std::set_intersection(first1, last1, first2, last2, std::inserter(c, c.end()), comp);
	return c;
}
template<template<class> class Container = set, class T> Container<T> set_intersection(Container<T> s1, Container<T> s2) {
	return dclong::set_intersection<typename set<T>::iterator, typename set<T>::iterator, Container>(s1.begin(), s1.end(), s2.begin(), s2.end());
}
template<template<class> class Container = set, class T, class Compare> Container<T> set_intersection(set<T> s1, set<T> s2, Compare comp) {
	return dclong::set_intersection<typename set<T>::iterator, typename set<T>::iterator, Container>(s1.begin(), s1.end(), s2.begin(), s2.end(), comp);
}
template<class InputIt1, class InputIt2, template<class> class Container = set>
				Container<typename InputIt1::value_type> set_difference(InputIt1 first1, InputIt1 last1,
				InputIt2 first2, InputIt2 last2) {
	Container<typename InputIt1::value_type> c;
	std::set_difference(first1, last1, first2, last2, std::inserter(c, c.end()));
	return c;
}

template<class InputIt1, class InputIt2, template<class> class Container = set, class Compare>
				Container<typename InputIt1::value_type> set_difference(InputIt1 first1, InputIt1 last1,
				InputIt2 first2, InputIt2 last2, Compare comp) {
	Container<typename InputIt1::value_type> c;
	std::set_difference(first1, last1, first2, last2, std::inserter(c, c.end()), comp);
	return c;
}
template<template<class> class Container = set, class T> Container<T> set_difference(set<T> s1, set<T> s2) {
	return dclong::set_difference<typename set<T>::iterator, typename set<T>::iterator, Container>(s1.begin(), s1.end(), s2.begin(), s2.end());
}
template<template<class> class Container = set, class T, class Compare> Container<T> set_difference(set<T> s1, set<T> s2, Compare comp) {
	return dclong::set_difference<typename set<T>::iterator, typename set<T>::iterator, Container>(s1.begin(), s1.end(), s2.begin(), s2.end(), comp);
}

template<class InputIt1, class InputIt2, template<class> class Container = set>
				Container<typename InputIt1::value_type> set_symmetric_difference(InputIt1 first1, InputIt1 last1,
				InputIt2 first2, InputIt2 last2) {
	Container<typename InputIt1::value_type> c;
	std::set_symmetric_difference(first1, last1, first2, last2, std::inserter(c, c.end()));
	return c;
}

template<class InputIt1, class InputIt2, template<class> class Container = set, class Compare>
				Container<typename InputIt1::value_type> set_symmetric_difference(InputIt1 first1, InputIt1 last1,
				InputIt2 first2, InputIt2 last2, Compare comp) {
	Container<typename InputIt1::value_type> c;
	std::set_symmetric_difference(first1, last1, first2, last2, std::inserter(c, c.end()), comp);
	return c;
}
template<template<class> class Container = set, class T> Container<T> set_symmetric_difference(set<T> s1, set<T> s2) {
	return dclong::set_symmetric_difference<typename set<T>::iterator, typename set<T>::iterator, Container>(s1.begin(), s1.end(), s2.begin(), s2.end());
}
template<template<class> class Container = set, class T, class Compare> Container<T> set_symmetric_difference(set<T> s1, set<T> s2, Compare comp) {
	return dclong::set_symmetric_difference<typename set<T>::iterator, typename set<T>::iterator, Container>(s1.begin(), s1.end(), s2.begin(), s2.end(), comp);
}

template<template<class> class Container, class T> bool includes(Container<T> s1, Container<T> s2){
	return std::includes(s1.begin(), s1.end(), s2.begin(), s2.end());
}

template<template<class> class Container, class T, class Compare> bool includes(Container<T> s1, Container<T> s2, Compare comp){
	return std::includes(s1.begin(), s1.end(), s2.begin(), s2.end(), comp);
}

**/

//template<class InputIt1, class InputIt2, class Container=Container.set> Container set_union(InputIt1 first1, InputIt1 last1,
//				InputIt2 first2, InputIt2 last2) {
//	Container c;
//	std::set_union(first1, last1, first2, last2, std::inserter(c, c.end()));
//	return c;
//}
//template<class InputIt1, class InputIt2, class Container, class Compare> Container set_union(InputIt1 first1,
//				InputIt1 last1, InputIt2 first2, InputIt2 last2, Compare comp) {
//	Container c;
//	std::set_union(first1, last1, first2, last2, std::inserter(c, c.end()), comp);
//	return c;
//
//}
//template<class InputIt1, class InputIt2, class Container> Container set_intersection(InputIt1 first1, InputIt1 last1,
//				InputIt2 first2, InputIt2 last2) {
//	Container c;
//	std::set_intersection(first1, last1, first2, last2, std::inserter(c, c.end()));
//	return c;
//
//}
//template<class InputIt1, class InputIt2, class Container, class Compare> Container set_intersection(InputIt1 first1,
//				InputIt1 last1, InputIt2 first2, InputIt2 last2, Compare comp) {
//	Container c;
//	std::set_intersection(first1, last1, first2, last2, std::inserter(c, c.end()), comp);
//	return c;
//
//}
//template<class InputIt1, class InputIt2, class Container> Container set_difference(InputIt1 first1, InputIt1 last1,
//				InputIt2 first2, InputIt2 last2) {
//	Container c;
//	std::set_difference(first1, last1, first2, last2, std::inserter(c, c.end()));
//	return c;
//
//}
//template<class InputIt1, class InputIt2, class Container, class Compare> Container set_difference(InputIt1 first1,
//				InputIt1 last1, InputIt2 first2, InputIt2 last2, Compare comp) {
//	Container c;
//	std::set_difference(first1, last1, first2, last2, std::inserter(c, c.end()), comp);
//	return c;
//
//}
//template<class InputIt1, class InputIt2, class Container> Container set_symmetric_difference(InputIt1 first1,
//				InputIt1 last1, InputIt2 first2, InputIt2 last2) {
//	Container c;
//	std::set_symmetric_difference(first1, last1, first2, last2, std::inserter(c, c.end()));
//	return c;
//
//}
//template<class InputIt1, class InputIt2, class Container, class Compare> Container set_symmetric_difference(
//				InputIt1 first1, InputIt1 last1, InputIt2 first2, InputIt2 last2, Compare comp) {
//	Container c;
//	std::set_symmetric_difference(first1, last1, first2, last2, std::inserter(c, c.end()), comp);
//	return c;
//
//}

}

#endif /* ALGORITHM_HPP_ */
