/*
 * count.hpp
 *
 *  Created on: Nov 11, 2012
 *      Author: dclong
 */

#ifndef DCLONG_COUNT_HPP_
#define DCLONG_COUNT_HPP_


namespace dclong {
template<typename UnaryPred, typename T>
bool count_f(UnaryPred uop, const T & x) {
	return uop(x);
}

template<typename UnaryPred, typename T, typename ... Args>
int count_f(UnaryPred uop, const T & x, Args ... args) {
	return uop(x) + count_f(uop, args...);
}
template<typename T, typename ... Args>
int count(const T & value, const T & x, Args ... args) {
	return count_f([value](const T & x) {return value==x;}, x, args...);
}
}


#endif /* DCLONG_COUNT_HPP_ */
