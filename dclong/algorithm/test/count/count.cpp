/*
 * count.cpp
 *
 *  Created on: Nov 11, 2012
 *      Author: dclong
 */




#include "../count.hpp"
#include <iostream>
using namespace std;
void test1(){
	int x {dclong::count(1,1,2,3,1,2,1)};
	cout << x << endl;
}
void test2(){
	int x {dclong::count_f([](int x){return x%2==0;},1,2,3,1,2,1)};
	cout << x << endl;
}
int main(){
	test1();
	test2();
}
