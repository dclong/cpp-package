/*
 * minmax.cpp
 *
 *  Created on: Nov 11, 2012
 *      Author: dclong
 */



#include "../../minmax.hpp"
#include <iostream>
#include <cmath>
using namespace std;
void test1(){
	auto x = dclong::min(1,2,3,4,-1);
	cout << x << endl;
}

void test2(){
	auto x = dclong::min_f([](int x1, int x2){return std::abs(x1)<std::abs(x2);}, -10, -2, 3, -4,5);
	cout << x << endl;
}


void test3(){
	auto x = dclong::max(1,2,3,4,-1);
	cout << x << endl;
}

void test4(){
	auto x = dclong::max_f([](int x1, int x2){return std::abs(x1)<std::abs(x2);}, -10, -2, 3, -4,5);
	cout << x << endl;
}

int main(){
	test1();
	test2();
	test3();
	test4();
}
