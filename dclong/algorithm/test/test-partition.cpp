/*
 * test-partition.cpp
 *
 *  Created on: Aug 25, 2012
 *      Author: dclong
 */

#include "/home/dclong/Dropbox/code/cpp/cackage/io/io.h"
#include "/home/dclong/Dropbox/code/cpp/cackage/util/util.h"

#include <vector>
using namespace std;
int main(){
	vector<int> x{2,3,50,2,1,1,1,50,3,3};
    dclong::print(begin(x), end(x));
	auto p = dclong::partition_i(begin(x), end(x), [](int & e){return e % 2;});
    dclong::print(begin(p), end(p), [](vector<size_t> & i){
            dclong::print(begin(i), end(i));
    });
}
