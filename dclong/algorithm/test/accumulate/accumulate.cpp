/*
 * test_accumulate.cpp
 *
 *  Created on: Nov 8, 2012
 *      Author: dclong
 */


void test1(){
	double s {accumulate_f([](double x1, double x2){return x1*x2;}, 1, 1, 2, 3.5, 4)};
	cout << s << endl;
}

void test2(){
	double s {accumulate(0.0, 1.0, 2.5, 3, 4)};
	cout << s << endl;
}


int main(){
	test1();
	test2();
}


