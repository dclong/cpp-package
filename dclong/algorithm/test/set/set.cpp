/*
 * test-algorithm.cpp
 *
 *  Created on: Oct 21, 2012
 *      Author: dclong
 */


#include "dclong/algorithm/algorithm.hpp"
#include "dclong/string/string.hpp"
#include <set>
#include <iostream>
#include <vector>
using namespace std;
void test1(){
	set<int> x{3, 1, 2};
	cout << dclong::to_string(x.begin(), x.end()) << endl;
	set<int> y{8,7,9};
    cout << dclong::to_string(y.begin(), y.end()) << endl;
	auto z = dclong::set_union<set<int>::iterator, set<int>::iterator, set>(x.begin(), x.end(), y.begin(), y.end());
	cout << dclong::to_string(z.begin(), z.end()) << endl;
	//vector<int> w = dclong::set_union<set<int>::iterator, set<int>::iterator, vector>(x.begin(), x.end(), y.begin(), y.end());
}
/**
void test2(){
	set<int> x{3, 1, 2};
	cout << dclong::to_string(x.begin(), x.end()) << endl;
	set<int> y{8, 7, 9};
    cout << dclong::to_string(y.begin(), y.end()) << endl;
	auto z = dclong::set_union(x, y);
    cout << z.count(1) << endl;
    cout << dclong::to_string(z.begin(), z.end()) << endl;
    cout << dclong::includes(x,y) << endl;
}
**/
int main(){
	test1();
}
